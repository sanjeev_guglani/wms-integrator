package com.delhivery.godam.db.utils;

import com.delhivery.godam.db.service.DbServicesBridge;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * Created by Sanjeev Guglani on 15/5/19.
 */
public class ClientDetailsUtils {

    /**
     * Get client uuid string.
     *
     * @param masterChannel the master channel
     * @return the string
     */
    public static String getClientUuid(GodamUtils.MasterChannel masterChannel){
        return DbServicesBridge.getDbServices().clientDetailService().getClientUuid(masterChannel);
    }
}
