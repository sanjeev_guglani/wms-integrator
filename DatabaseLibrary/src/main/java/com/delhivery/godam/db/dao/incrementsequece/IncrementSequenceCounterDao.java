package com.delhivery.godam.db.dao.incrementsequece;


import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;

@Service
public class IncrementSequenceCounterDao {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * method to get increment counter which will be unique across all
     * containers running
     * it uses sequence's of db to get value
     *
     * @return increment counter
     */
    public BigInteger getIncrementCounterNextValue() {

        String nativeQuery = "SELECT nextval('public.increment_id_seq')";
        return (BigInteger) entityManager.createNativeQuery(nativeQuery).getSingleResult();

    }

    public BigInteger getShipmentSequenceNextValue() {

        String nativeQuery = "SELECT nextval('public.order_shipment_seq')";
        return  (BigInteger) entityManager.createNativeQuery(nativeQuery).getSingleResult();

    }

    /**
     * method to get increment counter which will be unique in case
     * of transmission and messages control numbers
     * @return increment counter
     */
    public BigInteger getAmazonMessageSequenceCounterNextValue(){

        String nativeQuery = "SELECT nextval('public.amazon_message_seq')";
        return (BigInteger) entityManager.createNativeQuery(nativeQuery).getSingleResult();
    }

}
