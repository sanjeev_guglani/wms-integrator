package com.delhivery.godam.db.dao.storedetails;

import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * The interface Stock order store details dao.
 */
public interface IStockOrderStoreDetailsDao {

    /**
     * Store details by store id b 2 b store.
     *
     * @param channel the channel
     * @param storeId the store id
     * @return the b 2 b store
     */
    B2BStore storeDetailsByStoreId(GodamUtils.MasterChannel channel, String storeId);

}
