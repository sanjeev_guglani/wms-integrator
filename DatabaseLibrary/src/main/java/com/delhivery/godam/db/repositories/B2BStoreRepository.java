package com.delhivery.godam.db.repositories;

import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface B 2 b store repository.
 */
@Repository
public interface B2BStoreRepository extends JpaRepository<B2BStore, Long> {

    /**
     * Find by master channel channel and store id b 2 b store.
     *
     * @param channel the channel
     * @param storeId the store id
     * @return the b 2 b store
     */
    B2BStore findByMasterChannel_ChannelAndStoreId(GodamUtils.MasterChannel channel, String storeId);
}
