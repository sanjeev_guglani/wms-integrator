package com.delhivery.godam.db.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *  Define a generic interface to implement Object <-> String Json conversion.
 *  Because interfaces cannot contain properties, therefore two methods getInstance()
 *  and getObjectMapper() are provided for the conversion logic access to object instances
 *  that it requires at run time. Converter classes will need to provide implementations
 *  for these methods.
 *
 * Created by Vaibhav on 10/10/18.
 */
public interface GenericDbColumnJsonConverter<T> extends javax.persistence.AttributeConverter<T, String> {

    Logger logger = LoggerFactory.getLogger(GenericDbColumnJsonConverter.class);
    T getInstance();
    ObjectMapper getObjectMapper();

    @Override
    default String convertToDatabaseColumn(T meta) {
        try {
            return getObjectMapper().writeValueAsString(meta);
        } catch (JsonProcessingException ex) {
            logger.error("Unexpected IOEx decoding json from database: " + meta);
            return null;
        }
    }

    @Override
    default T convertToEntityAttribute(String dbData) {
        try {
            return getObjectMapper().readValue(dbData, (Class<T>)getInstance().getClass());
        } catch (IOException ex) {
            logger.error("Unexpected IOEx decoding json from database: " + dbData);
            return null;
        }
    }

}