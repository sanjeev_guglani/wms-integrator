package com.delhivery.godam.db.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * created by sachin bhogal
 * <p>
 * jpa entity class to store seller details data
 */
@Entity(name = "item_bucket_mappings")
@Data
@Table(indexes = {@Index(name = "primary_ref_index",columnList = "primary_ref_number")})
public class ItemBucketMappings implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "primary_ref_number", nullable = false )
    private String primaryRefNumber;

    @Column(name = "secondary_ref_number")
    private String secondaryRefNumber;

    @Column(name ="product_number")
    private String productNumber;

    @Column(name ="bucket")
    private String bucket;

    @Column(name ="previous_bucket")
    private String previousBucket;

    @Column(name ="count")
    private int count;

    @Column(name = "return_id")
    private String retrunId;

    @Column(name="fulfillment_center")
    private String fulfillmentCenter;

    @Column(name ="updated_at")
    private long updatedAt;
}
