package com.delhivery.godam.db.entity;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


/**
 * created by sachin bhogal
 * <p>
 * jpa entity class to store master channel data
 */
@Entity(name = "master_channel")
@Data
public class MasterChannel implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Convert(converter = MasterChannelConverter.class)
    @Column(name = "channel", nullable = false, unique = true)
    private GodamUtils.MasterChannel channel;

    @Embedded
    private AuthenticationDetails authDetails;

}
