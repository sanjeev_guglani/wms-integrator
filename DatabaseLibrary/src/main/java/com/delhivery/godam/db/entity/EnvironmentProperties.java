package com.delhivery.godam.db.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/*
* @author indreshkhandelwal
* jpa entity class to store the environment base
* properties
* */
@Data
@Entity(name="environment_properties")
public class EnvironmentProperties implements Serializable {

    public EnvironmentProperties(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public EnvironmentProperties(){

    }

    public EnvironmentProperties(int id, String key, String value){
        this.id = id;
        this.key = key;
        this.value = value;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="key", columnDefinition = "text")
    private String key;

    @Column(name="value", columnDefinition = "text")
    private String value;

}
