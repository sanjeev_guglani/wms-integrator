package com.delhivery.godam.db.utils;


import com.delhivery.godam.integrator.contracts.exceptions.*;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

/**
 * created by sachin bhogal
 * enums contains messages to be logged for specific exceptions
 */
public enum CustomExceptionHandlers {

    ORDER_DATA_EXCEPTION {
        @Override
        public Function<Exception, Map<String, Object>> getErrorLog() {

            return (exc) -> {
                OrderDataException exception = ((OrderDataException) exc);
                Map<String, Object> errors = new HashMap<>();
                errors.put(ERROR_CODE, getErrorCode(exception));
                errors.put(MESSAGE, exception.getMessage());
                errors.put(ORDER_DATA,exception.getRawData());
                return errors;
            };
        }

        @Override
        public String getExceptionName() {
            return OrderDataException.class.getSimpleName();
        }
    },

    NOTIFICATION_DATA_EXCEPTION {
        @Override
        public Function<Exception, Map<String, Object>> getErrorLog() {

            return (exc) -> {
                NotificationsDataException exception = ((NotificationsDataException) exc);
                Map<String, Object> errors = new HashMap<>();
                errors.put(ERROR_CODE, getErrorCode(exception));
                errors.put(MESSAGE, exception.getMessage());
                errors.put(ORDER_NUMBER, exception.getRawData());
                return errors;
            };
        }

        @Override
        public String getExceptionName() {
            return NotificationsDataException.class.getSimpleName();
        }
    },

    ASN_DATA_EXCEPTION {
        @Override
        public Function<Exception, Map<String, Object>> getErrorLog() {
            return (exc) -> {
                ASNDataException exception = ((ASNDataException) exc);
                Map<String, Object> errors = new HashMap<>();
                errors.put(ERROR_CODE, getErrorCode(exception));
                errors.put(MESSAGE, exception.getMessage());
                errors.put(ASN_DATA,exception.getRawData());
                return errors;
            };
        }

        @Override
        public String getExceptionName() {
            return ASNDataException.class.getSimpleName();
        }
    },

    INVALID_UNIT_OF_MEASUREMENT {
        @Override
        public Function<Exception, Map<String, Object>> getErrorLog() {
            return (exc) -> {
                InvalidUnitOfMeasurementException exception =
                        ((InvalidUnitOfMeasurementException) exc);
                Map<String, Object> errors = new HashMap<>();
                errors.put(MESSAGE, exception.getMessage());
                errors.put(PRODUCT_IDS, exception.getProductId());
                errors.put(UNIT_OF_MEASUREMENT, exception.getUom());
                errors.put(REFERENCE_NO, exception.getReferenceNumber());
                errors.put(WAREHOUSE, exception.getWarehouse());
                errors.put(PRODUCT_QTY, exception.getQuantity());
                return errors;
            };
        }

        @Override
        public String getExceptionName() {
            return InvalidUnitOfMeasurementException.class.getSimpleName();
        }
    };

    public abstract Function<Exception, Map<String, Object>> getErrorLog();

    public abstract String getExceptionName();

    public String getErrorCode(DataException ex) {
        ErrorMessage errorMessage = ex.getErrorMessage();
        return errorMessage != null ? errorMessage.getErrorCode() : NOT_DEFINED;
    }
}
