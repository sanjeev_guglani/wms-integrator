package com.delhivery.godam.db.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The type Authentication details.
 */
@Embeddable
@Data
public class AuthenticationDetails implements Serializable {


    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "endpoint")
    private String endpoint;

    //TODO is this contain unique key constraint??
    @Column(name = "client_key")
    private String clientKey;

    @Column(name = "client_user_token")
    private String clientUserToken;

    @Column(name = "token")
    private String token;

    @Column(name = "auth_code")
    private String authCode;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "refresh_token")
    private String refreshToken;

    @Column(name = "access_key")
    private String accessKey;

    @Column(name = "fbd_user_token")
    private String fbdUserToken;

    // to be used when single supplier linked to seller
    @Column(name = "supplier_key")
    private String supplierKey;

    @Column(name = "master_user_token")
    private String masterUserToken;


}
