package com.delhivery.godam.db.utils;

import com.delhivery.godam.db.entity.MasterChannel;
import com.delhivery.godam.db.service.DbServicesBridge;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * @author Dewbrat Kumar on 27/8/18
 */
public class MasterChannelUtils {

    /**
     * method to get master channel detail from slug name
     * @param masterChannel channel name
     * @return master channel details
     */
    public static MasterChannel getMasterChannelDetails(GodamUtils.MasterChannel masterChannel){
        return DbServicesBridge.getDbServices().masterChannelService().getByMasterChannel(masterChannel);
    }

    /**
     * method to get client user token based on master channel
     * @param channel master channel
     * @param tokenPrefixRequired is token required or not
     * @return client user token
     */
    public static String getClientUserTokenMasterChannel(String channel, boolean tokenPrefixRequired){
        GodamUtils.MasterChannel masterChannel = GodamUtils.MasterChannel.getMasterChannel(channel);
        String clientUserToken = getMasterChannelDetails(masterChannel).getAuthDetails().getClientUserToken();
        return tokenPrefixRequired?"Token "+ clientUserToken : clientUserToken;
    }
}
