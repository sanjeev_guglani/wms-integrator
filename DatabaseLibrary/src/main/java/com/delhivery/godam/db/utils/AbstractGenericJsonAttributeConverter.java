package com.delhivery.godam.db.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *  Define a generic interface to implement Object <-> String Json conversion.
 *  Because interfaces cannot contain properties, therefore two methods getInstance()
 *  and getObjectMapper() are provided for the conversion logic access to object instances
 *  that it requires at run time. Converter classes will need to provide implementations
 *  for these methods.
 *
 * Created by vaibhav on 10/10/18.
 */
public abstract class AbstractGenericJsonAttributeConverter<T> implements GenericDbColumnJsonConverter<T>  {

    private static final ObjectMapper objectmapper = new ObjectMapper();

    @Override
    public ObjectMapper getObjectMapper() {
        return AbstractGenericJsonAttributeConverter.objectmapper;
    }
}
