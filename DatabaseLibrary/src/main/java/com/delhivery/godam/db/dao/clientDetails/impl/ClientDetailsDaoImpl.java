package com.delhivery.godam.db.dao.clientDetails.impl;

import com.delhivery.godam.db.dao.clientDetails.ClientDetailsDao;
import com.delhivery.godam.db.repositories.ClientDetailsRepository;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Sanjeev Guglani on 15/5/19.
 *  This dao layer is responsible for interacting with clientDetails table
 */
@Service
public class ClientDetailsDaoImpl implements ClientDetailsDao {

    @Autowired
    private ClientDetailsRepository clientDetailsRepository;


    @Override
    public String getClientUuid(GodamUtils.MasterChannel masterChannel) {
        return clientDetailsRepository.findClientUuidByMasterChannel(masterChannel);
    }
}
