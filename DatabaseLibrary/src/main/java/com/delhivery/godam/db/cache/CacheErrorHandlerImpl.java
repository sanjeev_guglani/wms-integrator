package com.delhivery.godam.db.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;

/**
 * created by sachin bhogal
 * <p>
 * as spring cache used aop framework so we don;t have direct control to interact with redis in case of any exception
 * so spring cache provides errorHandler which will be invoked if any exception occurs while interacting with redis
 */


public class CacheErrorHandlerImpl implements CacheErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheErrorHandlerImpl.class);

    @Override
    public void handleCacheGetError(RuntimeException e, Cache cache, Object o) {

        LOGGER.error(String.format("Unable to get data from " +
                "redis cache %s key %s error %s", cache.getName(), o, e.getMessage()));
    }

    @Override
    public void handleCachePutError(RuntimeException e, Cache cache, Object o, Object o1) {

        LOGGER.error(String.format("Unable to put data to " +
                "redis cache %s key %s value %s error %s ", cache.getName(), o, o1, e.getMessage()));
    }

    @Override
    public void handleCacheEvictError(RuntimeException e, Cache cache, Object o) {
        LOGGER.error(String.format("Unable to delete key from " +
                "redis cache %s key %s error %s ", cache.getName(), o, e.getMessage()));
    }

    @Override
    public void handleCacheClearError(RuntimeException e, Cache cache) {
        LOGGER.error(String.format("Unable to clear cache from " +
                "redis cache %s error %s ", cache.getName(), e.getMessage()));
    }

}
