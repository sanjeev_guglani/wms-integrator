package com.delhivery.godam.db.cache;

import com.delhivery.godam.db.utils.RedisCache;
import org.springframework.cache.annotation.Cacheable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * created by sachin bhogal
 * <p>
 * annotation will be place on method and data return by method will be cached to underlying cache in our case is redis
 * annotation also used @Cacheable which is spring framework annotation uses aop framework behind the scenes
 * so that we don't have to write code to get data from redis it will do it for us during method invocation
 * <p>
 * in any cache values is stored as key value pair
 * so that any retrieval of data is done by unique key and key generation logic will be same across app
 * for that we defined key generator defined in RedisConfig class
 * <p>
 * Note:
 * <p>
 * we are defining our cache with combination of table_name:channel ie. warehouse_details:paytm will be
 * one cache which will stored warehouse details for paytm channel similarly there will be another cache for another channel
 * <p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Cacheable(cacheResolver = "redisCacheResolver")
public @interface CacheService {

    /**
     * this will be position of channel name passed in method argument as we need to get cache name at runtime
     * so if we call method like obj.findByKey(key , channel) then cache suffix index will be 1
     * <p>
     * if we dont pass channel in any method argument then method will not put data in redis as it will not able to get
     * cache name
     *
     * @return the int
     */
    int cacheSuffixIndex();

    /**
     * this is used to define the uniqueness of keys
     * for example; mehtod call like obj.findByKey1AndKey2(channel ,key1 , key2) then uniqueKeyIndexes will be {1,2}
     * and data stored in redis like:
     * table_name:channel (this will be cache name)
     * findByKey1AndKey2:key1:key2 (this will be key)
     *
     * @return the int [ ]
     */
    int[] uniqueKeyIndexes();

    /**
     * this is used to get table name
     *
     * @return the redis cache
     */
    RedisCache cachePrefix();
}

