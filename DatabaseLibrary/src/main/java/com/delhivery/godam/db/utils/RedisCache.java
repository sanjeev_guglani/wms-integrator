package com.delhivery.godam.db.utils;


import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.db.entity.WarehouseDetails;

import javax.persistence.Entity;
import java.util.Arrays;
import java.util.List;

import static com.delhivery.godam.db.utils.CacheConf.HILTI_STORE_DETAILS;

/**
 * created by sachin bhogal
 * <p>
 * enum is used to define mapping between cache and channels
 */
public enum RedisCache {


    /**
     * Warehouse details redis cache.
     */
    WAREHOUSE_DETAILS(WarehouseDetails.class.getAnnotation(Entity.class).name(),
            Arrays.asList(CacheConf.PAYTM_WAREHOUSE, CacheConf.AMAZON_WAREHOUSE)),

    /**
     * Store details redis cache.
     */
    STORE_DETAILS(B2BStore.class.getAnnotation(Entity.class).name(),
            Arrays.asList(HILTI_STORE_DETAILS)),


    /**
     * Auth token redis cache.
     */
    AUTH_TOKEN("auth_token_cache", Arrays.asList(CacheConf.PAYTM_AUTH_TOKEN_CACHE)),

    /**
     * Product details redis cache.
     */
    PRODUCT_DETAILS("product_details", Arrays.asList(CacheConf.TI_PRODUCT_DETAILS_CACHE)),

    /**
     * Master channel details redis cache.
     */
    MASTER_CHANNEL_DETAILS("master_channel_details", Arrays.asList(CacheConf.TI_MASTER_CHANNEL_DETAILS_CACHE));

    private String cacheName;
    private List<CacheConf> cacheConf;

    RedisCache(String cacheName, List<CacheConf> cacheConf) {
        this.cacheConf = cacheConf;
        this.cacheName = cacheName;
    }

    /**
     * Gets cache name.
     *
     * @return the cache name
     */
    public String getCacheName() {
        return cacheName;
    }

    /**
     * Gets cache conf.
     *
     * @return the cache conf
     */
    public List<CacheConf> getCacheConf() {
        return cacheConf;
    }
}

