package com.delhivery.godam.db.entity;

import com.delhivery.godam.integrator.contracts.utils.Utility;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type B 2 b store.
 */
@Entity(name = "store_details")
@Data
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"store_id", "channel"})
})
public class B2BStore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "store_id",nullable = false)
    private String storeId;

    @Column(name = "name" , nullable = false)
    private String name;

    @Column(name = "address_1")
    private String address1;

    @Column(name = "address_2")
    private String address2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "pincode")
    private Long pincode;

    @Column(name = "phone_1")
    private String phone1;

    @Column(name = "phone_2")
    private String phone2;

    @Column(name = "email")
    private String email;

    @Column(name = "gst_in")
    private String gstin;

    @JoinColumn(name = "channel", referencedColumnName = "channel",nullable = false)
    @ManyToOne
    private MasterChannel masterChannel;

    /**
     * Sets phone 1.
     *
     * @param phone1 the phone 1
     */
    public void setPhone1(String phone1) {
        this.phone1 = Utility.extractMobileNumber(phone1);
    }

    @Column(name = "updated_at")
    private long updatedAt;
}
