package com.delhivery.godam.db.cache;

import com.delhivery.godam.db.utils.CacheConf;
import com.delhivery.godam.db.utils.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * created by sachin bhogal
 * <p>
 * redis cache configuration class
 * class contains configuration required to use redis instance
 */
@Configuration
@Slf4j
public class RedisCacheConfig extends CachingConfigurerSupport {


    @Value("${redis.host}")
    private String redisHost;

    @Value("${redis.port}")
    private Integer redisPort;

    @Value("${redis.cache.default.ttl}")
    private Long cacheDefaultTTL;

    @Value("${redis.db.index}")
    private Integer dbIndex;

    @Value("${redis.connection.time.out}")
    private Integer connectionTimeOutInMs;

    @Value("${redis.connection.pool.max.size}")
    private Integer maxConnection;

    @Value("${redis.connection.pool.min.idle}")
    private Integer minimumIdle;

    @Value("${redis.connection.pool.max.idle}")
    private Integer maxIdle;

    @Value("${redis.connection.pool.max.wait.ms}")
    private Integer maxWaitMillis;



    /**
     * lettuce connection factory
     * for details about connection pool configuration see
     * https://gist.github.com/JonCole/925630df72be1351b21440625ff2671f#file-redis-bestpractices-java-jedis-md
     *
     * @return the lettuce connection factory
     */
    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration redisConf = new RedisStandaloneConfiguration();
        redisConf.setHostName(redisHost);
        redisConf.setPort(redisPort);
        redisConf.setDatabase(dbIndex);
        return new LettuceConnectionFactory(redisConf);
    }


    /**
     * Cache manager cache manager.
     *
     * @param connectionFactory redis connection factory
     * @return the cache manager
     */
    @Bean(name = "redisCacheManager")
    public CacheManager cacheManager(LettuceConnectionFactory connectionFactory) {
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .disableCachingNullValues()
                .entryTtl(Duration.ofMinutes(cacheDefaultTTL))
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.string()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.java()));

        return RedisCacheManager.RedisCacheManagerBuilder.fromConnectionFactory(connectionFactory)
                .withInitialCacheConfigurations(getConfigurationForCacheName())
                .cacheDefaults(redisCacheConfiguration).build();
    }



    /**
     * method will set ttl for all available caches at startup
     *
     * @return map contains key as cacheNames and values as their expire time
     */
    private Map<String, RedisCacheConfiguration> getConfigurationForCacheName() {

        Map<String, RedisCacheConfiguration> cacheNameConfiguration = new HashMap<>();

        for (RedisCache x : RedisCache.values()) {
            for (CacheConf y : x.getCacheConf()) {
                RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                        .entryTtl(Duration.ofMinutes(y.getExpireTime()));
                cacheNameConfiguration.put(String.format("%s:%s", x.getCacheName(),
                        y.getMasterChannel().getSlugName()), redisCacheConfiguration);
            }
        }
        return cacheNameConfiguration;
    }




    /**
     * it is provided by spring cache to resolve cache names at runtime
     *
     * @return cache resolver
     */
    @Bean(name = "redisCacheResolver")
    @Override
    public CacheResolver cacheResolver() {
        return new CacheResolver(cacheManager(redisConnectionFactory()));
    }

    /**
     * Evict cache resolver evict cache resolver.
     *
     * @return the evict cache resolver
     */
    @Bean
    public EvictCacheResolver evictCacheResolver() {
        return new EvictCacheResolver(cacheManager(redisConnectionFactory()));
    }

    /**
     * key generator used for key generation in redis
     * key will be generated as:
     * obj.findByKey1AndKey2(channel , key1 , key2)
     * then key is "findByKey1AndKey2:key1:key2"
     *
     * @return
     */
    @Bean(name = "redisKeyGenerator")
    @Override
    public KeyGenerator keyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object o, Method method, Object... objects) {

                try {
                    Method targetMethod = o.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
                    int[] keyIndexes =
                            targetMethod.getAnnotation(CacheService.class).uniqueKeyIndexes();
                    return String.format("%s:%s", method.getName(), getKey(keyIndexes, objects));
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                //TODO
                return null;
            }

            private String getKey(int[] keyIndexes, Object[] args) {

                StringBuilder sb = new StringBuilder();
                for (int index : keyIndexes) {
                    sb.append(args[index]);
                    sb.append(":");
                }
                String str = sb.toString();
                return str.substring(0, str.length() - 1);
            }
        };
    }


    /**
     * as spring cache used aop framework so we don;t have direct control to interact with redis in case of any exception
     * so spring cache provides errorHandler which will be invoked if any exception occurs while interacting with redis
     *
     * @return errorHandler
     */
    @Bean(name = "redisErrorHandler")
    @Override
    public CacheErrorHandler errorHandler() {

        return new CacheErrorHandlerImpl();
    }

}
