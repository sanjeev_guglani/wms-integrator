package com.delhivery.godam.db.utils;

import com.delhivery.godam.db.service.DbServicesBridge;

import java.math.BigInteger;

/**
 * this class contains utility method that
 * returns the sequence number of the message counter
 *
 * @author Kumar Vaibhav on 23/08/18
 */
public class SequenceUtils {

    /**
     * gets Message Counter
     *
     * @return the seller details
     */
    public static BigInteger fetchNextIncrementalSequenceNumber() {

        return DbServicesBridge.getDbServices().incrementSequenceDao().getIncrementCounterNextValue();
    }


    /**
     * gets Shipment Counter
     *
     * @return the seller details
     */
    public static BigInteger fetchNextIncrementalShipmentNumber() {

        return DbServicesBridge.getDbServices().incrementSequenceDao().getShipmentSequenceNextValue();
    }

    /**
     * method to fetch next incremental transmission sequence number
     * @return the next sequence number
     */
    public static BigInteger fetchMessageSequenceNumber(){
        return DbServicesBridge.getDbServices().incrementSequenceDao().getAmazonMessageSequenceCounterNextValue();
    }
}
