package com.delhivery.godam.db.cache;

import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.AbstractCacheResolver;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;

import java.util.Collection;
import java.util.Collections;

/**
 * The type Evict cache resolver.
 */
public class EvictCacheResolver extends AbstractCacheResolver  {

    /**
     * Instantiates a new Evict cache resolver.
     *
     * @param cacheManager the cache manager
     */
    public EvictCacheResolver(CacheManager cacheManager) {
        super(cacheManager);
    }

    @Override
    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        /**/
        DeleteCache annotation = null;
        //TODO
        try {
            annotation = context.getTarget().getClass()
                    .getDeclaredMethod(context.getMethod().getName(), context.getMethod().getParameterTypes()).getAnnotation(DeleteCache.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        //TODO need to improve
        RedisCache cachePrefix = annotation.cache();
        GodamUtils.MasterChannel channel = annotation.channel();
        return Collections.singleton(String.format("%s:%s", cachePrefix.getCacheName(),channel.getSlugName()));
    }
}
