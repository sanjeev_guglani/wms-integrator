package com.delhivery.godam.db.config;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Objects;

/**
 * The type Http client.
 */
public class HttpClient {

    private static HttpClient _instanse = null;

    /**
     * The Rest template.
     */
    public RestTemplate restTemplate;

    private HttpClient() {
        restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(Collections.singletonList(new MappingJackson2HttpMessageConverter()));
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static HttpClient getInstance() {
        if (Objects.isNull(_instanse)){
            _instanse = new HttpClient();
        }
        return _instanse;
    }
}
