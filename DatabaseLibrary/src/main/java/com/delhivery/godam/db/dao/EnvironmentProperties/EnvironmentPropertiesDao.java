package com.delhivery.godam.db.dao.EnvironmentProperties;

import com.delhivery.godam.db.entity.EnvironmentProperties;

import java.util.List;

/*
* @author indreshkhandelwal
* @since 1/8/2019
* */

public interface EnvironmentPropertiesDao {

    /*
    * method to get all the environment
    * stored in database
    * @return list of all the properties
    * */
    List<EnvironmentProperties> getAllProperties();
}
