package com.delhivery.godam.db.dao.warehousedetails.impl;

import com.delhivery.godam.db.cache.CacheService;
import com.delhivery.godam.db.entity.WarehouseDetails;
import com.delhivery.godam.db.repositories.WarehouseDetailRepository;
import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * The type Warehouse detail dao.
 */
@Service
public class WarehouseDetailDaoImpl implements IWareHouseDetailDao {


    private final WarehouseDetailRepository warehouseDetailsRepository;

    /**
     * Instantiates a new Warehouse detail dao.
     *
     * @param warehouseDetailsRepository the warehouse details repository
     */
    @Autowired
    public WarehouseDetailDaoImpl(WarehouseDetailRepository warehouseDetailsRepository) {

        this.warehouseDetailsRepository = warehouseDetailsRepository;
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public String getFulfillmentCenterNameByWareHouseId(GodamUtils.MasterChannel channel, String warehouseId) {
        return Optional.ofNullable(warehouseDetailsRepository.getFcNameByWarehouseId(channel, warehouseId))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), warehouseId));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public String getFulfillmentCenterUuidByWareHouseId(GodamUtils.MasterChannel channel, String warehouseId) {
        return Optional.ofNullable(warehouseDetailsRepository.getFcUuidByWarehouseId(channel, warehouseId))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), warehouseId));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public WarehouseDetails getWarehouseDetailsByFCName(GodamUtils.MasterChannel channel, String fcName) throws WarehouseMappingNotFoundException {
        return Optional.ofNullable(warehouseDetailsRepository.findByMasterChannelAndFulfillmentCenterName(channel, fcName))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), fcName));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public String getFcSkFromWarehouse(GodamUtils.MasterChannel channel, String warehouse) throws WarehouseMappingNotFoundException {
        return Optional.ofNullable(warehouseDetailsRepository.getFcSkFromWarehouse(channel, warehouse))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), warehouse));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public String getWarehouseFromFcSk(GodamUtils.MasterChannel channel, String fcSk) throws WarehouseMappingNotFoundException {
        return Optional.ofNullable(warehouseDetailsRepository.getWarehouseFromFcSk(channel, fcSk))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), fcSk));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public String getWarehouseFromFcName(GodamUtils.MasterChannel channel, String fcName) throws WarehouseMappingNotFoundException {
        return Optional.ofNullable(warehouseDetailsRepository.getWarehouseFromFCName(channel, fcName))
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), fcName));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    public Map<String, String> getAdditionalParamByWarehouseId(GodamUtils.MasterChannel channel, String warehouseId) throws WarehouseMappingNotFoundException {
        return Optional.of(warehouseDetailsRepository.getWarehouseDetailsByWarehouse(channel, warehouseId).getAdditionalParams())
                .orElseThrow(() -> new WarehouseMappingNotFoundException(channel.getSlugName(), warehouseId));
    }

}
