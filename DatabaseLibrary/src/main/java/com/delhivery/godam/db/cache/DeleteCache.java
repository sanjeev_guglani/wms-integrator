package com.delhivery.godam.db.cache;

import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.cache.annotation.CacheEvict;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * created by sachin bhogal
 * <p>
 * annotation is used to delete all keys from cache
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@CacheEvict(cacheResolver = "evictCacheResolver", allEntries = true)
public @interface DeleteCache {

    /**
     * Cache redis cache.
     *
     * @return the redis cache
     */
    RedisCache cache();

    /**
     * Channel godam utils . master channel.
     *
     * @return the godam utils . master channel
     */
    GodamUtils.MasterChannel channel();
}
