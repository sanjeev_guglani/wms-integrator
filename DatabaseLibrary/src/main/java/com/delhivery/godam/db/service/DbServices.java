package com.delhivery.godam.db.service;


import com.delhivery.godam.db.dao.clientDetails.ClientDetailsDao;
import com.delhivery.godam.db.dao.incrementsequece.IncrementSequenceCounterDao;
import com.delhivery.godam.db.dao.masterchannel.IMasterChannelDao;
import com.delhivery.godam.db.dao.storedetails.IStockOrderStoreDetailsDao;
import com.delhivery.godam.db.dao.warehousedetails.impl.IWareHouseDetailDao;

/**
 * The interface Db services.
 */
public interface DbServices {

    /**
     * Warehouse service ware house detail dao.
     *
     * @return the ware house detail dao
     */
    IWareHouseDetailDao warehouseService();


    /**
     * Store details service stock order store details dao.
     *
     * @return the stock order store details dao
     */
    IStockOrderStoreDetailsDao storeDetailsService();


    /**
     * incrementSequence counter service
     *
     * @return incrementSequenceDao service
     */
    IncrementSequenceCounterDao incrementSequenceDao();

    /**
     * Master channel service master channel dao.
     *
     * @return the master channel dao
     */
    IMasterChannelDao masterChannelService();


    /**
     * Client detail service client details dao.
     *
     * @return the client details dao
     */
    ClientDetailsDao clientDetailService();



}
