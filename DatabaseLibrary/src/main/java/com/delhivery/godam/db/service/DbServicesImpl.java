package com.delhivery.godam.db.service;

import com.delhivery.godam.db.dao.clientDetails.ClientDetailsDao;
import com.delhivery.godam.db.dao.incrementsequece.IncrementSequenceCounterDao;
import com.delhivery.godam.db.dao.masterchannel.IMasterChannelDao;
import com.delhivery.godam.db.dao.storedetails.IStockOrderStoreDetailsDao;
import com.delhivery.godam.db.dao.warehousedetails.impl.IWareHouseDetailDao;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The type Db services.
 */
public class DbServicesImpl implements DbServices {


    @Autowired
    private IWareHouseDetailDao wareHouseDetailDao;

    @Autowired
    private IStockOrderStoreDetailsDao stockOrderStoreDetailsDao;


    @Autowired
    private IncrementSequenceCounterDao incrementSequenceCounterDao;

    @Autowired
    private IMasterChannelDao masterChannelDao;

    @Autowired
    private ClientDetailsDao clientDetailsDao;

    @Override
    public IWareHouseDetailDao warehouseService() {
        return wareHouseDetailDao;
    }


    @Override
    public IStockOrderStoreDetailsDao storeDetailsService() {
        return stockOrderStoreDetailsDao;
    }

    @Override
    public IncrementSequenceCounterDao incrementSequenceDao() {
        return incrementSequenceCounterDao;
    }

    @Override
    public IMasterChannelDao masterChannelService() {
        return masterChannelDao;
    }

    @Override
    public ClientDetailsDao clientDetailService() {
        return clientDetailsDao;
    }
}
