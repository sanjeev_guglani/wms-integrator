package com.delhivery.godam.db.utils;

import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.db.entity.WarehouseDetails;
import com.delhivery.godam.db.service.DbServicesBridge;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

import java.util.Map;

/**
 * The type Warehouse utils.
 */
public class WarehouseUtils {

    /**
     * Gets fulfillment center name.
     *
     * @param masterChannel the master channel
     * @param warehouse     the warehouse
     * @return the fulfillment center name
     */
    public static String getFulfillmentCenterName(GodamUtils.MasterChannel masterChannel, String warehouse) {

        return DbServicesBridge.getDbServices().warehouseService()
                .getFulfillmentCenterNameByWareHouseId(masterChannel, warehouse);
    }


    /**
     * Gets fulfillment center Uuid.
     *
     * @param masterChannel the master channel
     * @param warehouse     the warehouse
     * @return the fulfillment center Uuid
     */
    public static String getFulfillmentCenterUuid(GodamUtils.MasterChannel masterChannel, String warehouse) {

        return DbServicesBridge.getDbServices().warehouseService()
                .getFulfillmentCenterUuidByWareHouseId(masterChannel, warehouse);
    }

    /**
     * Gets fulfillment center sk.
     *
     * @param channel   the channel
     * @param warehouse the warehouse
     * @return the fulfillment center sk
     */
    public static String getFulfillmentCenterSK(GodamUtils.MasterChannel channel, String warehouse) {

        return DbServicesBridge.getDbServices().warehouseService()
                .getFcSkFromWarehouse(channel, warehouse);
    }

    /**
     * Gets warehouse name.
     *
     * @param channel             the channel
     * @param fulfillmentCenterSK the fulfillment center sk
     * @return the warehouse name
     */
    public static String getWarehouseName(GodamUtils.MasterChannel channel, String fulfillmentCenterSK) {
        return DbServicesBridge.getDbServices().warehouseService().getWarehouseFromFcSk(channel, fulfillmentCenterSK);
    }

    /**
     * Gets warehouse name by fc name.
     *
     * @param channel the channel
     * @param fcName  the fc name
     * @return the warehouse name by fc name
     */
    public static String getWarehouseNameByFCName(GodamUtils.MasterChannel channel, String fcName) {
        return DbServicesBridge.getDbServices().warehouseService().getWarehouseFromFcName(channel, fcName);
    }

    /**
     * Gets warehouse details.
     *
     * @param masterChannel         the master channel
     * @param fulfillementCenterNum the fulfillement center num
     * @return the warehouse details
     */
    public static WarehouseDetails getWarehouseDetails(GodamUtils.MasterChannel masterChannel, String fulfillementCenterNum) {

        return DbServicesBridge.getDbServices().warehouseService()
                .getWarehouseDetailsByFCName(masterChannel, fulfillementCenterNum);
    }

    /**
     * Gets warehouse additional params.
     *
     * @param channel     the channel
     * @param wareHouseId the ware house id
     * @return the warehouse additional params
     */
    public static Map<String, String> getWarehouseAdditionalParams(GodamUtils.MasterChannel channel, String wareHouseId) {

        return DbServicesBridge.getDbServices().warehouseService()
                .getAdditionalParamByWarehouseId(channel, wareHouseId);
    }

    /**
     * Gets b 2 b store details.
     *
     * @param channel the channel
     * @param storeId the store id
     * @return the b 2 b store details
     */
    public static B2BStore getB2BStoreDetails(GodamUtils.MasterChannel channel, String storeId) {

        return DbServicesBridge.getDbServices().storeDetailsService()
                .storeDetailsByStoreId(channel, storeId);
    }

}
