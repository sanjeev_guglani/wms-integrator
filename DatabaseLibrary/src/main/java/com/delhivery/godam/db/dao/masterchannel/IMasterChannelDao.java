package com.delhivery.godam.db.dao.masterchannel;

import com.delhivery.godam.db.entity.MasterChannel;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * The interface Master channel dao.
 */
public interface IMasterChannelDao {

    /**
     * Gets by slug name.
     *
     * @param slugName the slug name
     * @return the by slug name
     */
    MasterChannel getBySlugName(String slugName);


    /**
     * Gets by master channel.
     *
     * @param masterChannel the master channel
     * @return the by master channel
     */
    MasterChannel getByMasterChannel(GodamUtils.MasterChannel masterChannel);
}
