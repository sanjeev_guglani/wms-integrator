package com.delhivery.godam.db.dao.paytm.impl;

import com.delhivery.godam.db.dao.warehousedetails.impl.IWareHouseDetailDao;
import com.delhivery.godam.db.utils.WarehouseUtils;
import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import com.delhivery.godam.db.dao.paytm.PaytmWarehouseMapDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * This is the class to provide methods to access paytm warehouse map data
 */
@Service
@Qualifier("paytmWarehouseMapConfigStore")
public class PaytmWarehouseMapConfigStore implements PaytmWarehouseMapDao {

    private static final Logger logger = LoggerFactory.getLogger(PaytmWarehouseMapConfigStore.class);

    @Autowired
    private IWareHouseDetailDao wareHouseDetailDao;

    @Override
    public String getWarehouseId(String fcId) throws WarehouseMappingNotFoundException {
        return WarehouseUtils.getWarehouseName(GodamUtils.MasterChannel.PAYTM, fcId);
    }

    @Override
    public String getFulfillmentCenterId(String whId, String requestId) throws WarehouseMappingNotFoundException{
        return WarehouseUtils.getFulfillmentCenterSK(GodamUtils.MasterChannel.PAYTM, whId);
    }

    @Override
    public String getFulfillmentCenterNum(String whId, String requestId) throws WarehouseMappingNotFoundException{
        return WarehouseUtils.getFulfillmentCenterName(GodamUtils.MasterChannel.PAYTM, whId);
    }
}
