package com.delhivery.godam.db.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Objects;

/**
 * The type Db services bridge.
 */
public class DbServicesBridge implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    private static DbServices dbServices;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        DbServicesBridge.applicationContext = applicationContext;
    }

    /**
     * Gets db services.
     *
     * @return the db services
     */
    public static DbServices getDbServices() {
        if (Objects.isNull(dbServices)) {
            dbServices = applicationContext.getBean(DbServices.class);
        }
        return dbServices;
    }
}
