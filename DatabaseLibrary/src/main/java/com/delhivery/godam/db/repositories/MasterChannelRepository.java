package com.delhivery.godam.db.repositories;

import com.delhivery.godam.db.entity.MasterChannel;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Master channel repository.
 */
public interface MasterChannelRepository extends JpaRepository<MasterChannel, Long> {

    /**
     * Find by channel master channel.
     *
     * @param channel the channel
     * @return the master channel
     */
    MasterChannel findByChannel(GodamUtils.MasterChannel channel);
}
