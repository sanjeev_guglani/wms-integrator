package com.delhivery.godam.db.dao.EnvironmentProperties.Impl;

import com.delhivery.godam.db.dao.EnvironmentProperties.EnvironmentPropertiesDao;
import com.delhivery.godam.db.entity.EnvironmentProperties;
import com.delhivery.godam.db.repositories.EnvironmentPropertiesRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnvironmentPropertiesDaoImpl implements EnvironmentPropertiesDao {

    @Autowired
    private EnvironmentPropertiesRepositories environmentPropertiesRepositories;


    /*
     * method to get all the environment
     * stored in database
     * @return list of all the properties
     * */
    @Override
    public List<EnvironmentProperties> getAllProperties() {
        return environmentPropertiesRepositories.findAll();
    }
}
