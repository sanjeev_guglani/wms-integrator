package com.delhivery.godam.db.dao.storedetails;

import com.delhivery.godam.db.cache.CacheService;
import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.db.repositories.B2BStoreRepository;
import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.stereotype.Service;

/**
 * The type Stock order store details dao.
 */
@Service
public class StockOrderStoreDetailsDao implements IStockOrderStoreDetailsDao {


    private final B2BStoreRepository stockOrderStoreDetailsRepository;

    /**
     * Instantiates a new Stock order store details dao.
     *
     * @param stockOrderStoreDetailsRepository the stock order store details repository
     */
    public StockOrderStoreDetailsDao(B2BStoreRepository stockOrderStoreDetailsRepository) {

        this.stockOrderStoreDetailsRepository = stockOrderStoreDetailsRepository;
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.STORE_DETAILS)
    public B2BStore storeDetailsByStoreId(GodamUtils.MasterChannel channel, String storeId) {
        return stockOrderStoreDetailsRepository.findByMasterChannel_ChannelAndStoreId(channel, storeId);
    }
}
