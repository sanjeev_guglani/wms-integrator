package com.delhivery.godam.db.repositories;

import com.delhivery.godam.db.entity.EnvironmentProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * repositroy class for environmentProperties
 * Created by indreshkhandelwal on 8/1/19.
 */
@Repository
public interface EnvironmentPropertiesRepositories extends JpaRepository<EnvironmentProperties, Integer> {


    /*
     * method to get all the environment
     * stored in database
     * @return list of all the properties
     * */
    List<EnvironmentProperties> findAll();

    /*
     * method to save any custom environment property
     * to stored it in database
     * @return list of all the properties
     * */
    EnvironmentProperties save(EnvironmentProperties properties);

    /*
     * method to get any custom environment property
     * @return property from the environment
     * */
    EnvironmentProperties getByKey(String key);
}
