package com.delhivery.godam.db.cache;

import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.AbstractCacheResolver;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;

import java.util.Collection;
import java.util.Collections;

/**
 * created by sachin bhogal
 * <p>
 * class is used to get cache names at runtime while using spring cache
 */
public class CacheResolver extends AbstractCacheResolver {


    /**
     * Instantiates a new Cache resolver.
     *
     * @param cacheManager the cache manager
     */
    public CacheResolver(CacheManager cacheManager) {
        super(cacheManager);
    }

    @Override
    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        /**/
        CacheService annotation = null;
        //TODO
        try {
            annotation = context.getTarget().getClass()
                    .getDeclaredMethod(context.getMethod().getName(), context.getMethod().getParameterTypes()).getAnnotation(CacheService.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        //TODO need to improve
        RedisCache cachePrefix = annotation.cachePrefix();
        int cacheSuffixIndex = annotation.cacheSuffixIndex();
        return Collections.singleton(String.format("%s:%s", cachePrefix.getCacheName(),
                ((GodamUtils.MasterChannel) context.getArgs()[cacheSuffixIndex]).getSlugName()));
    }
}

