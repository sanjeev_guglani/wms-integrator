package com.delhivery.godam.db.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;

/**
 * created by sachin bhogal
 * <p>
 * jpa entity class to store wareHouse details data
 */
@Data
@Entity(name = "warehouse_details")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"channel", "name"})
})
public class WarehouseDetails implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Embedded
    private AuthenticationDetails authentication;

    @JoinColumn(name = "channel", referencedColumnName = "channel", nullable = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private MasterChannel masterChannel;

    @Column(name = "fulfillment_center_name",nullable = false)
    private String fulfillmentCenterName;

    @Column(name = "fulfillment_center_uuid")
    private String fulfillmentCenterUuid;

    @Column(name = "fulfillment_center_sk",nullable = false)
    private String fulfillmentCenterSK;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="name")
    @Column(name="value" , nullable = false)
    @CollectionTable(name="warehouse_details_extra_info", joinColumns=@JoinColumn(name="id"))
    private Map<String, String> additionalParams;


}
