package com.delhivery.godam.db.config;

import com.delhivery.godam.db.cache.RedisCacheConfig;
import com.delhivery.godam.db.service.DbServices;
import com.delhivery.godam.db.service.DbServicesBridge;
import com.delhivery.godam.db.service.DbServicesImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * The type Db configuration.
 */
@Configuration
@Import(RedisCacheConfig.class)
@EnableCaching
@ComponentScan(basePackages = {"com.delhivery.godam.db.dao","com.delhivery.godam.db.service"})
@EnableJpaRepositories(basePackages = "com.delhivery.godam.db.repositories")
public class DbConfiguration {

    @Value("${integrator.db.url}")
    private String dbUrl;

    @Value("${integrator.db.username}")
    private String userName;

    @Value("${integrator.db.password}")
    private String password;

    @Value("${integrator.db.driver.class.name}")
    private String jdbcDriverClass;

    @Value("${integrator.db.connection.time.out}")
    private Long connectionTimeOut;

    @Value("${integrator.db.idle.time.out}")
    private Long idleTimeOut;

    @Value("${integrator.db.max.pool.size}")
    private Integer maxPoolSize;

    @Value("${integrator.db.orm.dialect}")
    private String hibernateDialect;

    @Value("${integrator.db.orm.ddl-auto}")
    private String schemaDDL;

    @Value("${integrator.db.min.idle}")
    private Integer minimumIdle;

    @Value("${integrator.db.max.life.time.ms}")
    private Long maxLifeTime;

    /**
     * Entity manager factory local container entity manager factory bean.
     *
     * @return the local container entity manager factory bean
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.delhivery.godam.db.entity");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    /**
     * Data source data source.
     *
     * @return the data source
     */
    @Bean
    public DataSource dataSource() {

        HikariConfig config = new HikariConfig();
        config.setPoolName("SpringBootJPAHikariCP");
        config.setJdbcUrl(dbUrl);
        config.setUsername(userName);
        config.setPassword(password);
        config.setConnectionTimeout(connectionTimeOut);
        config.setDriverClassName(jdbcDriverClass);
        config.setIdleTimeout(idleTimeOut);
        config.setMaximumPoolSize(maxPoolSize);
        config.setMinimumIdle(minimumIdle);
        config.setMaxLifetime(maxLifeTime);
        //config.setM

        return new HikariDataSource(config);

    }

    /**
     * Transaction manager platform transaction manager.
     *
     * @param emf the emf
     * @return the platform transaction manager
     */
    @Bean
    public PlatformTransactionManager transactionManager(
            EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    /**
     * Exception translation persistence exception translation post processor.
     *
     * @return the persistence exception translation post processor
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Additional properties properties.
     *
     * @return the properties
     */
    Properties additionalProperties() {
        Properties properties = new Properties();

        properties.setProperty("hibernate.hbm2ddl.auto", schemaDDL);
        properties.setProperty(
                "hibernate.dialect", hibernateDialect);
        properties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");

        return properties;
    }

    /**
     * Db services db services.
     *
     * @return the db services
     */
    @Bean
    public DbServices dbServices() {
        return new DbServicesImpl();
    }

    /**
     * Db services bridge db services bridge.
     *
     * @return the db services bridge
     */
    @Bean
    public DbServicesBridge dbServicesBridge() {

        return new DbServicesBridge();
    }
}
