package com.delhivery.godam.db.dao.masterchannel.impl;

import com.delhivery.godam.db.cache.CacheService;
import com.delhivery.godam.db.dao.masterchannel.IMasterChannelDao;
import com.delhivery.godam.db.entity.MasterChannel;
import com.delhivery.godam.db.repositories.MasterChannelRepository;
import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Master channel dao.
 */
@Service
public class MasterChannelDaoImpl implements IMasterChannelDao {

    private final MasterChannelRepository masterChannelRepository;


    /**
     * Instantiates a new Master channel dao.
     *
     * @param masterChannelRepository the master channel repository
     */
    @Autowired
    public MasterChannelDaoImpl(MasterChannelRepository masterChannelRepository) {

        this.masterChannelRepository = masterChannelRepository;
    }

    @Override
    public MasterChannel getBySlugName(String slugName) {
        return masterChannelRepository
                .findByChannel(GodamUtils.MasterChannel.getMasterChannel(slugName));
    }

    @Override
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 0, cachePrefix = RedisCache.MASTER_CHANNEL_DETAILS)
    public MasterChannel getByMasterChannel(GodamUtils.MasterChannel masterChannel) {
        return masterChannelRepository.findByChannel(masterChannel);
    }
}
