package com.delhivery.godam.db.dao.paytm;


import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;


/**
 * This is the DAO contract for interacting with Paytm warehouse map collection
 */
public interface PaytmWarehouseMapDao {

   // String getAuthTokenForWarehouse(GodamUtils.MasterChannel channel ,  String requestId);
    /**
     * Returns the warehouse ID
     * @param fcId fcId
     * @return warehouse id
     */
    String getWarehouseId(String fcId) throws WarehouseMappingNotFoundException;

    /**
     * Return the FulfillmentCenterId
     * @param whId whId
     * @param requestId requestId
     * @return FulfillmentCenterId
     */
    String getFulfillmentCenterId(String whId, String requestId) throws WarehouseMappingNotFoundException;

    /**
     * Return the FulfillmentCenterNum
     * @param whId whId
     * @param requestId requestId
     * @return FulfillmentCenterNum
     * */
    String getFulfillmentCenterNum(String whId, String requestId) throws WarehouseMappingNotFoundException;

}
