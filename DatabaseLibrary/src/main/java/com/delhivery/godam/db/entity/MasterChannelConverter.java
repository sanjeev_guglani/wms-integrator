package com.delhivery.godam.db.entity;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * The type Master channel converter.
 */
@Converter
public class MasterChannelConverter implements AttributeConverter<GodamUtils.MasterChannel, String> {

    @Override
    public String convertToDatabaseColumn(GodamUtils.MasterChannel attribute) {
        return attribute.getSlugName();
    }

    @Override
    public GodamUtils.MasterChannel convertToEntityAttribute(String dbData) {

        return GodamUtils.MasterChannel.getMasterChannel(dbData);
    }
}