package com.delhivery.godam.db.utils;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * created by sachain bhogal
 * <p>
 * enum is used to manage ttl for channel specific caches
 */
public enum CacheConf {

    /**
     * Paytm warehouse cache conf.
     */
    PAYTM_WAREHOUSE(GodamUtils.MasterChannel.PAYTM, 259200L),
    /**
     * Flipkart seller details cache conf.
     */
    FLIPKART_SELLER_DETAILS(GodamUtils.MasterChannel.FLIPKART, 86400L),
    /**
     * Amazon warehouse cache conf.
     */
    AMAZON_WAREHOUSE(GodamUtils.MasterChannel.AMAZON, 259200L),
    /**
     * Hilti store details cache conf.
     */
    HILTI_STORE_DETAILS(GodamUtils.MasterChannel.HILTI,259200L),
    /**
     * Paytm auth token cache cache conf.
     */
    PAYTM_AUTH_TOKEN_CACHE(GodamUtils.MasterChannel.PAYTM,7200L),
    /**
     * Ti product details cache cache conf.
     */
    TI_PRODUCT_DETAILS_CACHE(GodamUtils.MasterChannel.TI_CYCLE,86400L),
    /**
     * Master channel details cache cache conf.
     */
    TI_MASTER_CHANNEL_DETAILS_CACHE(GodamUtils.MasterChannel.TI_CYCLE, 259200L);

    private GodamUtils.MasterChannel masterChannel;
    private Long expireTime;

    CacheConf(GodamUtils.MasterChannel masterChannel, Long expireTime) {
        this.masterChannel = masterChannel;
        this.expireTime = expireTime;
    }

    /**
     * Gets master channel.
     *
     * @return the master channel
     */
    public GodamUtils.MasterChannel getMasterChannel() {
        return masterChannel;
    }

    /**
     * Gets expire time.
     *
     * @return the expire time
     */
    public Long getExpireTime() {
        return expireTime;
    }
}
