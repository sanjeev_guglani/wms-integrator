package com.delhivery.godam.db.dao.warehousedetails.impl;

import com.delhivery.godam.db.cache.CacheService;
import com.delhivery.godam.db.entity.WarehouseDetails;
import com.delhivery.godam.db.utils.RedisCache;
import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

import java.util.Map;

/**
 * created by sachin bhogal
 * <p>
 * contract to get data from warehouse details entity
 */
public interface IWareHouseDetailDao {

    /**
     * Gets fulfillment center name by ware house id.
     *
     * @param channel     the channel
     * @param warehouseId the warehouse id
     * @return the fulfillment center name by ware house id
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    String getFulfillmentCenterNameByWareHouseId(GodamUtils.MasterChannel channel, String warehouseId) throws WarehouseMappingNotFoundException;

    /**
     * Gets fulfillment center uuid by ware house id.
     *
     * @param channel     the channel
     * @param warehouseId the warehouse id
     * @return the fulfillment center uuid by ware house id
     */
    @CacheService(cacheSuffixIndex = 0, uniqueKeyIndexes = 1, cachePrefix = RedisCache.WAREHOUSE_DETAILS)
    String getFulfillmentCenterUuidByWareHouseId(GodamUtils.MasterChannel channel, String warehouseId);

    /**
     * Gets warehouse details by fc name.
     *
     * @param channel the channel
     * @param fcName  the fc name
     * @return the warehouse details by fc name
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    WarehouseDetails getWarehouseDetailsByFCName(GodamUtils.MasterChannel channel, String fcName) throws WarehouseMappingNotFoundException;

    /**
     * Gets fc sk from warehouse.
     *
     * @param channel   the channel
     * @param warehouse the warehouse
     * @return the fc sk from warehouse
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    String getFcSkFromWarehouse(GodamUtils.MasterChannel channel, String warehouse) throws WarehouseMappingNotFoundException;

    /**
     * Gets warehouse from fc sk.
     *
     * @param channel the channel
     * @param fcSk    the fc sk
     * @return the warehouse from fc sk
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    String getWarehouseFromFcSk(GodamUtils.MasterChannel channel, String fcSk) throws WarehouseMappingNotFoundException;

    /**
     * Gets warehouse from fc name.
     *
     * @param channel the channel
     * @param fcName  the fc name
     * @return the warehouse from fc name
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    String getWarehouseFromFcName(GodamUtils.MasterChannel channel, String fcName) throws WarehouseMappingNotFoundException;

    /**
     * Gets additional param by warehouse id.
     *
     * @param channel     the channel
     * @param warehouseId the warehouse id
     * @return the additional param by warehouse id
     * @throws WarehouseMappingNotFoundException the warehouse mapping not found exception
     */
    Map<String, String> getAdditionalParamByWarehouseId(GodamUtils.MasterChannel channel, String warehouseId) throws WarehouseMappingNotFoundException;


}
