package com.delhivery.godam.db.repositories;

import com.delhivery.godam.db.entity.ClientDetails;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * The interface Client details repository.
 */
@Repository("clientDetailsRepository")
public interface ClientDetailsRepository extends JpaRepository<ClientDetails,Long> {

    /**
     * Find client uuid by master channel string.
     *
     * @param channel the channel
     * @return the string
     */
    @Query("select c.clientUuid from client_details c where c.masterChannel.channel=?1")
    String findClientUuidByMasterChannel(GodamUtils.MasterChannel channel);
}
