package com.delhivery.godam.db.repositories;


import com.delhivery.godam.db.entity.WarehouseDetails;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * created by sachin bhogal
 * <p>
 * interface provides crud operation for warehouse details table
 */
@Repository("warehouseDetailRepository")
public interface WarehouseDetailRepository extends JpaRepository<WarehouseDetails, Long> {

    /**
     * Gets fc name by warehouse id.
     *
     * @param channel     the channel
     * @param warehouseId the warehouse id
     * @return the fc name by warehouse id
     */
    @Query(value = "select w.fulfillmentCenterName from warehouse_details w where w.masterChannel.channel=?1 AND w.name=?2")
    String getFcNameByWarehouseId(GodamUtils.MasterChannel channel, String warehouseId);


    /**
     * Gets fc Uuid by warehouse id.
     *
     * @param channel     the channel
     * @param warehouseId the warehouse id
     * @return the fc Uuid by warehouse id
     */
    @Query(value = "select w.fulfillmentCenterUuid from warehouse_details w where w.masterChannel.channel=?1 AND w.name=?2")
    String getFcUuidByWarehouseId(GodamUtils.MasterChannel channel, String warehouseId);

    /**
     * Find by master channel and fulfillment center name warehouse details.
     *
     * @param masterChannel the master channel
     * @param fcName        the fc name
     * @return the warehouse details
     */
    @Query("select w from warehouse_details w where w.masterChannel.channel=?1 And w.fulfillmentCenterName=?2")
    WarehouseDetails findByMasterChannelAndFulfillmentCenterName(GodamUtils.MasterChannel masterChannel, String fcName);

    /**
     * Gets fc sk from warehouse.
     *
     * @param masterChannel the master channel
     * @param warehouse     the warehouse
     * @return the fc sk from warehouse
     */
    @Query("select w.fulfillmentCenterSK from warehouse_details w where w.masterChannel.channel=?1 AND w.name=?2")
    String getFcSkFromWarehouse(GodamUtils.MasterChannel masterChannel, String warehouse);

    /**
     * Gets warehouse from fc name.
     *
     * @param masterChannel the master channel
     * @param fcName        the fc name
     * @return the warehouse from fc name
     */
    @Query("select w.name from warehouse_details w where w.masterChannel.channel=?1 AND w.fulfillmentCenterName=?2")
    String getWarehouseFromFCName(GodamUtils.MasterChannel masterChannel, String fcName);

    /**
     * Gets warehouse from fc sk.
     *
     * @param masterChannel the master channel
     * @param fcSk          the fc sk
     * @return the warehouse from fc sk
     */
    @Query("select w.name from warehouse_details w where w.masterChannel.channel=?1 AND w.fulfillmentCenterSK=?2")
    String getWarehouseFromFcSk(GodamUtils.MasterChannel masterChannel, String fcSk);

    /**
     * Gets warehouse details by warehouse.
     *
     * @param channel   the channel
     * @param warehouse the warehouse
     * @return the warehouse details by warehouse
     */
    @Query("select w from warehouse_details as w where w.masterChannel.channel = ?1 AND w.name =?2")
    WarehouseDetails getWarehouseDetailsByWarehouse(GodamUtils.MasterChannel channel, String warehouse);
}
