package com.delhivery.godam.db.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Sanjeev Guglani on 15/5/19.
 */
@Entity(name = "client_details")
@Data
public class ClientDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_uuid", nullable = false, unique = true)
    private String clientUuid;

    @JoinColumn(name = "channel_id", nullable = false, referencedColumnName = "channel")
    @OneToOne
    private MasterChannel masterChannel;
}
