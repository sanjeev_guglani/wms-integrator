package com.delhivery.godam.db.dao.clientDetails;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * Created by Sanjeev Guglani on 15/5/19.
 * This dao layer is responsible for interacting with clientDetails table
 */
public interface ClientDetailsDao {


    /**
     * Gets client uuid.
     *
     * @param masterChannel the master channel
     * @return the client uuid
     */
    String getClientUuid(GodamUtils.MasterChannel masterChannel);
}
