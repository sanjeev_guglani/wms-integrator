package com.delhivery.godam.integrator.contracts.utils;

import com.delhivery.godam.integrator.contracts.commons.annotation.Currency;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Objects;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.PRICE_SCALE;

/**
 * @author Dewbrat Kumar on 19/2/18
 * Currency serializer class used to set default values and scalling
 * for currency fields
 */
public class BigDecimalJsonSerializer extends JsonSerializer<Object> implements ContextualSerializer {

    private int precision = PRICE_SCALE;

    public BigDecimalJsonSerializer(){
    }

    public BigDecimalJsonSerializer(int precision){
        this.precision = precision;
    }

    @Override
    public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if(Objects.nonNull(value)){
            jsonGenerator.writeNumber(((BigDecimal)value).setScale(precision, BigDecimal.ROUND_HALF_UP));
        }else{
            jsonGenerator.writeNumber(BigDecimal.ZERO.setScale(precision, BigDecimal.ROUND_HALF_UP));
        }
    }

    @Override
    public Class<Object> handledType() {
        return Object.class;
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty property) throws JsonMappingException {
        int precision = PRICE_SCALE;
        Currency currency = null;
        if (property != null) {
            currency = property.getAnnotation(Currency.class);
        }
        if (currency != null) {
            precision = currency.precision();
        }
        return new BigDecimalJsonSerializer(precision);
    }
}
