package com.delhivery.godam.integrator.contracts.commons.constants;

/**
 * Class to containing keys and constants used in application
 * Created by sanjeev guglani on 20/4/16.
 */
public class IntegratorConstants {

    // Callback notifications
    public static final String ASN_CREATE_CALLBACK = "AGNCreate";
    public static final String ASN_RETURN_CALLBACK = "AsnReturn";
    public static final String PRODUCT_CREATE_CALLBACK = "ProductCallback";
    public static final String CHANNEL_SHOPCLUES_BASIC = "shopclues-basic";
    public static final String FFID_PUSH_CALLBACK = "FfidCallback";

    public static final String  AUTHORIZATION_TOKEN = "Authorization-Token";
    public static final String SELLER_AUTH_TOKEN = "SellerAuthToken";
    public static final String APPLICATION_JSON = "application/json";
    public static final String ENDPOINT = "endpoint";
    public static final String ROUTE_RETURNED = "routeReturned";
    public static final String STATUS = "status";
    public static final String STATE = "state";
    public static final String STATUS_CODE = "statusCode";
    public static final String CRTS_PAYLOAD = "crts_payload";
    public static final String EXCEPTION_CAUSE = "cause";
    public static final String REQUEST = "request";
    public static final String COMPONENT_UPDATE_REQUEST = "component_update_request";
    public static final String COMPONENT_COUNT = "component_count";
    public static final String PRODUCT_COMPONENT_SEPARATOR = "_";
    public static final String ASN_SEPARATOR = "_";
    public static final String ASN_EXTRAS = "asn_extras";
    public static final String PRODUCT_SKU = "product_sku";
    public static final String PRODUCT_NUM = "product_num";
    public static final String ASN_NUM = "asn_num";
    public static final String TRACKING_ID = "tracking_id";
    public static final String ASN_TYPE = "asn_type";
    public static final String RECALL_DETAILS = "recall_details";
    public static final String CBN = "CBN";
    public static final String BUCKET = "bucket";
    public static final String CLIENT_REQUEST = "client_request";
    public static final String PRODUCT_CHANNEL_MAPPING_TYPE = "F";
    public static final String PRODUCT_CHANNEL_LISTING_ID = "1";
    public static final int ALLOCATION_NUMBER = 1;

    public static final String NOT_FOUND = "not found";

    public static final String RESP_MESSAGE = "RESP_MESSAGE";
    public static final String REDIRECTION_LOCATION = "redirectionLocation";
    public static final String REQUEST_URI = "uri";
    public static final String STATUS_TEXT = "statusText";
    public static final String RESPONSE_HEADERS = "responseHeaders";
    public static final String RESPONSE_BODY = "responseBody";
    public static final String EMAIL_SUBJECT="EmailSubject";
    public static final String GODAM_SUCCESS_STATUS = "SUCCESS";
    public static final String GODAM_POST_SUCCESS_STATUS = "POST_SUCCESS";
    public static final String UPDATE_CLIENT_DETAILS = "update-client";
    public static final String FETCH_CLIENT_DETAILS = "fetch-client";

    public static final String RESPONSE = "response";
    public static final String GODAM_RESPONSE = "godam_response";
    public static final String TRANSFORMED_ORDERV2_LIST = "transformedOrders";
    public static final String TRANSFORMED_ASNV2_LIST = "transformedAsns";
    public static final String TRANSFORMED_PRODUCTV2_LIST = "transformedProducts";
    public static final String TRANSFORMED_ASNV2 = "transformedAsn";

    public static final String INVALID_ORDER_LIST = "invalidOrdersList";
    public static final String INVALID_ASN_LIST = "invalidAsnList";
    public static final String INVALID_PRODUCT_LIST = "invalidProductsList";
    public static final String INVALID_PRODUCT_CHANNEL_MAP_LIST = "invalidProductChannelMapList";

    public static final String PARSED_ORDERS = "parsedOrders";
    public static final String PARSED_ASN = "parsedAsn";
    public static final String PARSED_PRODUCTS = "parsedProducts";
    public static final String BATCH_PRODUCTS = "batchProducts";
    public static final String SUCCESS_RETURN_ASNS = "successAsns";
    public static final String CANCEL_REQUEST_LOAD = "cancelRequestLoad";
    public static final String ORDERLINES_MAP = "orderLinesMap";
    public static final String ERROR = "error";
    public static final String RAW_DATA = "raw-data";
    public static final String NOTIFICATION_NAME = "notificationName";
    public static final String ENTITY = "entity";
    public static final String MODULE = "module";
    public static final String SUCCESS_MESSAGE = "Operation Successful. Please Check status of request id.";
    public static final String FAIL_MESSAGE = "Operation Failed. Please Check status of request id for information.";
    public static final String IMPLEMENTATION_NOT_FOUND = "Implementation not found.";
    public static final String TRUCK_ID = "truckId";
    public static final String SEAL_ID = "sealId";
    public static final Integer NORMAL_PRODUCTS = 0;
    public static final Integer MASTER_PRODUCTS = 1;
    public static final Integer CHILD_PRODUCTS = 2;
    public static final String PARSED_PRODUCT_CHANNEL_MAP = "parsedProductChannelMap";

    // Channels
    public static final String AMAZON = "amazon"; // to use 'amazon', use tolower()
    public static final String AMAZON_CHANNEL = "amazon";
    public static final String CSV_FILES = "default-csv";
    public static final String SNAPDEAL = "Snapdeal";
    public static final String DEFAULT_CHANNEL = "Default";
    public static final String DEFAULT_CHANNEL_NAME= "default";
    public static final String TICYCLE_CHANNEL_NAME = "ticycle";
    public static final String AMAZON_RVO = "amazon-rvo";
    public static final String AMAZON_TRANSFER_SHIPMENT = "amazon-to";

    // headers required
    public static final String INTEGRATOR_TOKEN = "IntegratorToken";
    public static final String APPLICATION_JSON_CONTENT_TYPE = "Content-Type=application/json";

    // cancel check statuses
    public static final String ORDER_CANCELLED = "cancelled";
    public static final String ORDER_PENDING = "pending";
    public static final String ACTIVITY = "activity";
    public static final String ACTION = "action";
    public static final String BODY ="body";

    // keys used in project
    public static final String REQUEST_ID = "request_id";
    public static final String USER_ID = "User-Uuid";
    public static final String PACK_REQUEST_LOAD = "packRequestLoad";
    public static final String MANIFEST_REQUEST_LOAD = "manifestRequestLoad";
    public static final String PICK_REQUEST_LOAD = "pickRequestLoad";
    public static final String PACK_RESPONSE_LOAD = "packResponseLoad";
    public static final String PACKING_SLIP_LINK ="packingSlipLink";
    public static final String MANIFEST_SLIP_LINK ="manifestSlipLink";
    public static final String PACK_SLIP = "packSlip";
    public static final String INVOICE = "invoice";
    public static final String REQUEST_ID_V2 = "Request-Id";
    public static final String MESSAGE = "message";
    public static final String CHANNEL = "channel";
    public static final String CACHE_PREFIX = "cache_prefix";
    public static final String CLIENT_ORDERS = "clientOrders";
    public static final String CLIENT_RECALL_REQUEST = "clientRecallRequest";
    public static final String CLIENT_TRANS_SHIP_REQUEST = "clientTranshipRequest";
    public static final String CANCEL_ORDER_REQUEST = "cancelOrderRequest";
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String SUPPLIER_ID = "supplier_id";
    public static final String API = "api";
    public static final String FULFILLMENT_MODE_BS = "BS";
    public static final String FULFILLMENT_CENTER_NUM = "fulfillment_center_num";
    public static final String CLIENT_ACCESS_KEY = "client_access_key";
    public static final String S3_BUCKET = "s3_bucket";
    public static final String MESSAGE_ID_TYPE = "MessageIdType";
    public static final String SERVICE_TYPE = "ServiceType";
    public static final String MODEL_PACKAGE = "ModelPackage";
    public static final double THRESHOLD_WEIGHT = 25.0;
    public static final String AUTH_DETAILS = "auth_details";
    public static final String SELLER_AUTH_DETAILS = "seller_auth_details";
    public static final String SELLER_ID = "seller_id";

    public static final String FEED_FILE_NAME = "FeedFileName";
    public static final String WAREHOUSE = "Warehouse";
    public static final String ERROR_IDENTIFIER_ID = "ErrorIdentifier";
    public static final String WAREHOUSE_ID = "warehouse";
    public static final String REFERENCE_NUM = "reference_num";

    public static final String NOTIFICATION_TYPE = "NotificationType";
    public static final String TARGET_DRN = "DSNSTargetDRN";
    public static final String COUNTRY_INDIA = "India";

    public static final String REFERENCE_NO = "reference_no";
    public static final String UNIT_OF_MEASUREMENT = "uom";
    public static final String PRODUCT_QTY = "product_quantity";
    public static final String ASN_DATA = "asn_data";
    public static final String ORDER_DATA = "order_data";
    public static final String ORDER_NUMBER = "order_number";
    public static final String PRODUCT_IDS = "product_ids";
    public static final String PRODUCT_DETAILS_REQUEST = "product_details_request";
    public static final String PRODUCT_DETAILS = "product_details";
    public static final String ASN_PRODUCT_DETAILS = "asn_product_details";
    public static final String USER_NAME = "user_name";
    public static final int DEFAULT_WORKFLOW=1;

    public static final String ERROR_CODE = "error_code";
    public static final String NOT_DEFINED = "Not_Defined";
    public static final String AUTH_TOKEN = "authToken";
    // http methods
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";

    // route names
    public static final String ORDER_PICK_ROUTE_ID = "OrderPickRoute";

    public static final String ORDER_CREATE_V2_ROUTE = "direct:godamOrderCreateV2";
    public static final String ORDER_UPDATE_V2_ROUTE = "direct:godamOrderUpdateV2";
    public static final String ORDER_REST_CALL_ROUTE_WITH_RETRY = "direct:godamOrderCreateWithRetry";
    public static final String ORDER_CREATE_V2_WITH_RETRY_ROUTE_ID = "OrderCreateV2WithRetryRoute";
    public static final String ORDER_CREATE_ROUTE_ID = "OrderCreateRoute";
    public static final String ORDER_CREATE_V2_ROUTE_ID = "OrderCreateRouteV2";
    public static final String GODAM_ORDER_CREATE_V2_ROUTE_ID = "GodamOrderCreateRouteV2";
    public static final String GODAM_ORDER_UPDATE_V2_ROUTE_ID = "GodamOrderUpdateRouteV2";
    public static final String VALIDATE_AND_TRANSFORM_ROUTE = "direct:validateAndTransformRoute";
    public static final String VALIDATE_AND_TRANSFORM_ROUTE_ID = "ValidateAndTransformRoute";
    public static final String VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE = "direct:validateAndTransformGodamOrderCreateRoute";
    public static final String VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE_ID = "ValidateAndTransformGodamOrderCreateRoute";
    public static final String ORDER_UPDATE_V2_ROUTE_ID = "OrderUpdateRouteV2";
    public static final String VALIDATE_AND_TRANSFORM_UPDATE_ROUTE = "direct:validateAndTransformUpdateRoute";
    public static final String VALIDATE_AND_TRANSFORM_UPDATE_ROUTE_ID = "ValidateAndTransformUpdateRoute";
    public static final String GODAM_ORDER_UPDATE_ROUTE = "direct:godamOrderUpdateRoute";
    public static final String GODAM_ORDER_UPDATE_ROUTE_ID = "godamUpdateRoute";
    public static final String ORDER_CANCEL_V2_ROUTE_ID = "OrderCancelRouteV2";
    public static final String GODAM_ORDER_CANCEL_V2_ROUTE_ID = "GodamOrderCancelRouteV2";


    public static final String PREPARE_DSNS_NOTIFICATION_ROUTE = "direct:prepareDsnsNotificationRoute";
    public static final String PREPARE_DSNS_NOTIFICATION_ROUTE_ID = "PrepareDsnsNotificationRoute";
    public static final String VALIDATE_AND_TRANSFORM_NOTIFICATION_ROUTE = "direct:validateAndTransformNotificationRoute";
    public static final String VALIDATE_AND_TRANSFORM_NOTIFICATION_ROUTE_ID = "ValidateAndTransformNotificationRoute";

    public static final String S3_FILE_DOWNLOAD_ROUTE_ID = "S3FileDownloadRoute";
    public static final String ORDER_CREATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "OrderCreateCSVFileTypeRoute";
    public static final String ORDER_UPDATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "OrderUpdateCSVFileTypeRoute";
        public static final String ASN_CREATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "AsnCreateCSVFileTypeRoute";
    public static final String ASN_UPDATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "AsnUpdateCSVFileTypeRoute";
    public static final String PRODUCT_CREATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "ProductCreateCSVFileTypeRoute";
    public static final String PRODUCT_CHANNEL_MAPPING_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "ProductChannelMappingCSVFileTypeRoute";
    public static final String PRODUCT_CHANNEL_MAPPING_BULK_FILE_TYPE_PROCESS_ROUTE_ID = "ProductChannelMappingBulkFileTypeRoute";
    public static final String PRODUCT_UPDATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "ProductUpdateCSVFileTypeRoute";
    public static final String WAYBILL_UPDATE_CSV_FILE_TYPE_PROCESS_ROUTE_ID = "WaybillUpdateCSVFileTypeRoute";
    public static final String WAYBILL_STATUS_FILE_TYPE_PROCESS_ROUTE_ID = "WaybillUpdateFileTypeRoute";

    public static final String LOCAL_FILE_READING_ROUTE = "direct:localFileReadingRoute";
    public static final String LOCAL_FILE_READING_ROUTE_ID = "LocalFileReadingRoute";
    public static final String LOCAL_FILE_PROCESS_ROUTE = "direct:localFileProcessingRoute";
    public static final String LOCAL_FILE_PROCESS_ROUTE_ID = "LocalFileProcessingRoute";

    public static final String ASN_CREATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVAsnCreateProcessingRoute";
    public static final String ASN_UPDATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVAsnUpdateProcessingRoute";
    public static final String VALIDATED_GODAM_FORMAT_ASN_ROUTE = "BulkAsnCreateRouteValidFileRecords";
    public static final String VALIDATED_GODAM_FORMAT_ASN_UPDATE_ROUTE = "BulkAsnUpdateRouteValidFileRecords";
    public static final String INVALID_GODAM_FORMAT_ASN_ROUTE = "ErrorProcessInvalidAsnFileRecordsRoute";
    public static final String GODAM_RESPONSE_ASN_ROUTE = "GodamResponseRouteValidAsnFileRecords";
    public static final String VALIDATED_GODAM_FORMAT_PRODUCT_CHANNEL_ROUTE = "BulkProductChannelMapCreateRouteValidFileRecords";
    public static final String INVALID_GODAM_FORMAT_PRODUCT_CHANNEL_ROUTE = "BulkProductInvalidChannelMapCreateRouteValidFileRecords";
    public static final String VALIDATED_GODAM_FORMAT_WAYBILL_STATUS_ROUTE = "WaybillStatusRouteValidFileRecords";
    public static final String INVALID_GODAM_FORMAT_WAYBILL_STATUS_ROUTE = "WaybillStatusRouteInValidFileRecords";

    public static final String ORDER_CREATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVOrderCreateProcessingRoute";
    public static final String ORDER_UPDATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVOrderUpdateProcessingRoute";

    public static final String ORDER_FILE_UPDATE_ROUTE_ID = "DefaultCSVOrderUpdateRoute";
    public static final String ASN_FILE_UPDATE_ROUTE_ID = "DefaultCSVAsnUpdateRoute";
    public static final String VALIDATED_GODAM_FORMAT_ORDER_ROUTE = "BulkOrderCreateRouteValidFileRecords";

    public static final String PRODUCT_CREATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVProductCreateProcessingRoute";
    public static final String PRODUCT_UPDATE_FILE_CONSUMING_ROUTE_ID = "DefaultCSVProductUpdateProcessingRoute";
    public static final String VALIDATED_GODAM_FORMAT_PRODUCT_CREATE_ROUTE = "BulkProductCreateRouteValidFileRecords";
    public static final String VALIDATED_GODAM_FORMAT_PRODUCT_UPDATE_ROUTE = "BulkProductUpdateRouteValidFileRecords";
    public static final String INVALID_GODAM_FORMAT_PRODUCT_ROUTE = "ErrorProcessInvalidProductFileRecordsRoute";
    public static final String GODAM_RESPONSE_PRODUCT_ROUTE = "GodamResponseRouteValidProductFileRecords";
    public static final String GODAM_RESPONSE_PRODUCT_CHANNEL_MAPPING_ROUTE = "GodamResponseRouteValidProductChannelMapFileRecords";
    public static final String PRODUCT_FILE_UPDATE_ROUTE_ID = "DefaultCSVProductUpdateRoute";
    public static final String GODAM_RESPONSE_WAYBILL_STATUS_ROUTE = "GodamResponseRouteValidWaybillStatusFileRecords";

    public static final String WAYBILL_STATUS_UPDATE_ROUTE = "direct:updateGodamWaybillStatus";
    public static final String WAYBILL_STATUS_UPDATE_ROUTE_ID = "UpdateGodamWaybillStatus";

    public static final String VALIDATED_GODAM_FORMAT_ORDER_UPDATE_ROUTE = "BulkOrderUpdateRouteValidFileRecords";
    public static final String INVALID_GODAM_FORMAT_ORDER_ROUTE = "ErrorProcessInvalidOrderFileRecordsRoute";
    public static final String GODAM_RESPONSE_ORDER_ROUTE = "GodamResponseRouteValidOrderFileRecords";

    public static final String RECALL_TRANSFORM_ROUTE = "direct:recallTransformRoute";
    public static final String RECALL_TRANSFORM_ROUTE_ID = "RecallTransformRoute";
    public static final String RECALL_VALIDATE_ROUTE = "direct:recallValidateRoute";
    public static final String RECALL_VALIDATE_ROUTE_ID = "RecallValidateRoute";
    public static final String RECALL_CREATE_ROUTE = "direct:recallRoute";
    public static final String RECALL_CREATE_ROUTE_ID = "RecallRoute";
    public static final String TRANSHIP_CREATE_ROUTE = "direct:transShipRoute";
    public static final String TRANSHIP_CREATE_ROUTE_ID = "TransShipRoute";
    public static final String TRANS_SHIP_VALIDATE_AND_TRANSFORM_ROUTE = "direct:transhipValidateAndTransformRoute";
    public static final String TRANS_SHIP_VALIDATE_AND_TRANSFORM_ROUTE_ID = "TranshipValidateAndTransformRoute";
    public static final String FC_DETAIL_ROUTE = "direct:FCdetailsRoute";
    public static final String FC_DETAIL_ROUTE_ID = "FCDetailRoute";
    public static final String ORDER_CANCEL_VALIDATE_AND_TRANSFORM_ROUTE = "direct:orderCancelValidateAndTransformRoute";
    public static final String ORDER_CANCEL_VALIDATE_AND_TRANSFORM_ROUTE_ID = "OrderCancelValidateAndTransformRoute";
    public static final String LEVIS_ORDER_CANCEL_VALIDATE_AND_TRANSFORM_ROUTE = "direct:orderCancelValidateAndTransformV2Route";
    public static final String LEVIS_ORDER_CANCEL_VALIDATE_AND_TRANSFORM_ROUTE_ID = "OrderCancelValidateAndTransformV2Route";
    public static final String TRACK_REQUEST_ROUTE = "direct:push-tracker-log";
    public static final String TRACK_REQUEST_LIST_ROUTE = "direct:push-tracker-log-list";
    public static final String TRACK_REQUEST_ROUTE_ID = "trackerLogRouteId";
    public static final String TRACK_REQUEST_LIST_ROUTE_ID = "trackerLogListRouteId";
    public static final String LEVIS_GODAM_ORDER_CANCEL_ROUTE = "direct:orderCancelGodamRoute";
    public static final String LEVIS_GODAM_ORDER_CANCEL_ROUTE_ID = "LevisOrderCancelGodamRoute";
    public static final String LEVIS_ORDER_CANCEL_NOTIFICATION_ROUTE = "direct:levisOrderCancelNotificationRoute";
    public static final String LEVIS_ORDER_CANCEL_NOTIFICATION_ROUTE_ID="LevisOrderCancelNotificationRouteId";

    public static final String AMAZON_ORDER_CREATE_ROUTE_ID = "AmazonOrderCreateRoute";
    public static final String AMAZON_ORDER_CREATEV2_ROUTE_ID = "AmazonOrderCreateV2Route";
    public static final String AMAZON_ORDER_TRANSFORM_ROUTE_ID = "AmazonOrderTransformROute";
    public static final String AMAZON_ORDER_CREATE_NOTIFICATION_ROUTE = "direct:amazonOrderCreateNotificationRoute";
    public static final String AMAZON_ORDER_CREATE_NOTIFICATION_ROUTE_ID = "AmazonOrderCreateNotificationRoute";
    public static final String AMAZON_ORDER_CREATE_GODAM_ROUTE_ID = "AmazonOrderCreateGodamRoute";
    public static final String AMAZON_OFR_CREATE_ROUTE_ID = "AmazonOFRCreateRoute";
    public static final String AMAZON_ORDER_CREATE_ROUTE = "direct://amazonOrderCreateRoute";
    public static final String AMAZON_OFR_CREATE_ROUTE = "direct://OFRCreateRoute";
    public static final String AMAZON_PRR_CREATE_ROUTE = "direct://PRRCreateRoute";
    public static final String AMAZON_TRANS_SHIP_CREATE_ROUTE = "direct:transShipCreateRoute";
    public static final String AMAZON_FETCH_WAREHOUSE_DETAIL_ROUTE = "direct:FetchAmazonWarehouseDetailsRoute";
    public static final String AMAZON_FETCH_WAREHOUSE_DETAIL_ROUTE_ID = "FetchAmazonWarehouseDetailsRouteId";
    public static final String AMAZON_TRANS_SHIP_CREATE_ROUTE_ID = "TransShipCreateRoute";
    public static final String AMAZON_PRR_CREATE_ROUTE_ID = "PRRCreateRoute";
    public static final String AMAZON_NOTIFICATION_OLD_ROUTE = "direct:amazonNotificationOldRoute";
    public static final String AMAZON_NOTIFICATION_ROUTE = "direct:amazonNotificationRoute";
    public static final String AMAZON_ON_DEMAND_PCF_ROUTE = "direct:amazonProductOnDemandRoute";
    public static final String PARSE_AND_UPDATE_COMPONENTS_ROUTE = "direct:amazonProductUpdateRoute";
    public static final String UPDATE_PRODUCT_DEFINITION_ROUTE = "direct:amazonUpdateProductDefinitionRoute";
    public static final String AMAZON_MULTI_COMPONENT_PRODUCT_ROUTE = "direct:amazonMultiComponentProductCreateRoute";
    public static final String AMAZON_INBOUND_MULTI_COMPONENT_ROUTE = "direct:amazonAsnMultiComponentRoute";
    public static final String AMAZON_MULTI_COMPONENT_PRODUCT_ASN_CREATE_ROUTE = "direct:amazonAsnMultiComponentWhenInvExistRoute";
    public static final String AMAZON_INBOUND_MULTI_COMPONENT_WHEN_INV_EXIST_ROUTE_ID = "AmazonAsnMultiComponentWhenInvExistRouteId";
    public static final String AMAZON_MULTI_COMPONENT_ASN_CREATE_ROUTE = "direct:amazonMultiComponentAsnCreateRoute";
    public static final String AMAZON_ON_DEMAND_PCF_ROUTE_ID = "AmazonOnDemandProductRouteId";
    public static final String AMAZON_NOTIFICATION_FETCHER_ROUTE = "direct:amazonNotificationFetcherRoute";
    public static final String ORDER_PICK_NOTIFICATION_FETCHER_ROUTE = "direct:orderPickNotificationFetcherRoute";
    public static final String ORDER_PICK_NOTIFICATION_FETCHER_ROUTE_ID = "OrderPickNotificaitonFetcherRoute";
    public static final String AMAZON_OFR_NOTIFICATION_ROUTE = "direct:amazonOFRNotificationRoute";
    public static final String AMAZON_CREATE_ORDER_CANCEL_ROUTE = "direct:amazonCreateOrderCancelRoute";
    public static final String AMAZON_ORDER_CANCEL_NOTIFICATION_ROUTE = "direct:amazonOrderCancelNotificationRoute";
    public static final String AMAZON_ORDER_CANCEL_NOTIFICATION_ROUTE_ID = "AmazonOrderCancelNotificationRoute";
    public static final String AMAZON_CREATE_ORDER_CANCEL_ROUTE_ID = "AmazonCreateOrderCancelRoute";
    public static final String SHOPCLUES_ORDER_CREATE_ROUTE_ID = "ShopcluesOrderCreateRoute";
    public static final String ORDER_PACK_ROUTE_ID = "OrderPackRoute";
    public static final String ORDER_MANIFEST_ROUTE_ID = "OrderManifestRoute";
    public static final String AMAZON_ORDER_PACK_MLTS_ROUTE_ID = "AmazonMLTSPackRoute";
    public static final String AMAZON_ORDER_MANIFEST_MLTS_ROUTE_ID = "AmazonMLTSManifestRoute";
    public static final String AMAZON_NOTIFICATION_TRANSFORM_AND_VALIDATE_ID = "AmazonNotificationTransformAndValidateRoute";
    public static final String AMAZON_NOTIFICATION_TRANSFORM_AND_VALIDATE = "direct:amazonNotificationTransformAndValidateRoute";


    public static final String AMAZON_INBOUND_MULTI_COMPONENT_ROUTE_ID = "AmazonInboundMultiComponentRoute";
    public static final String UPDATE_PRODUCT_DEFINITION_ROUTE_ID = "UpdateProductDefinitionRoute";
    public static final String AMAZON_PRODUCT_CREATE_ROUTE_ID = "AmazonProductCreateRoute";
    public static final String AMAZON_PRODUCT_CREATE_ROUTE_ID_V2 = "AmazonProductCreateRouteV2";
    public static final String PARSE_AND_UPDATE_COMPONENTS_ROUTE_ID = "AmazonProductUpdateRoute";
    public static final String SNAPDEAL_ORDER_CREATE_ROUTE_ID = "SnadealOrderCreateRoute";
    public static final String SNAPDEAL_DROP_SHIP_ORDER_CREATE_ROUTE_ID = "SnadealDropShipOrderCreateRoute";
    public static final String SNAPDEAL_ONE_SHIP_ORDER_CREATE_ROUTE_ID = "SnadealOneShipOrderCreateRoute";
    public static final String SNAPDEAL_ORDER_MANIFEST_ROUTE_ID = "SnapdealOrderManifestRoute";
    public static final String SNAPDEAL_DROP_SHIP_ORDER_MANIFEST_ROUTE_ID = "SnapdealDropShipOrderManifestRoute";
    public static final String SNAPDEAL_ONE_SHIP_ORDER_MANIFEST_ROUTE_ID = "SnapdealOneShipOrderManifestRoute";
    public static final String SNAPDEAL_ORDER_PACK_ROUTE_ID = "SnapdealOrderPackRoute";
    public static final String SNAPDEAL_ORDER_DETAIL_ROUTE_ID = "SnapdealOrderDetailRoute";
    public static final String SNAPDEAL_ORDER_REPACK_ROUTE_ID = "SnapdealOrderRePackRoute";
    public static final String SNAPDEAL_DROP_SHIP_ORDER_PACK_ROUTE_ID = "SnapdealDropShipOrderPackRoute";
    public static final String SNAPDEAL_ONE_SHIP_ORDER_PACK_ROUTE_ID = "SnapdealOneShipOrderPackRoute";
    public static final String SNAPDEAL_NOTIFICATION_ROUTE_ID = "SnapdealNotificationRoute";
    public static final String AMAZON_ASN_SCHEDULER_ROUTE_ID = "AmazonASNSchedulerRoute";
    public static final String AMAZON_ASN_QUEUE_CONSUME_ROUTE_ID = "AmazonASNQueueConsumeRoute";
    public static final String AMAZON_TRANSFER_ORDER_ASN_QUEUE_ROUTE_ID = "AmazonTransferOrderAsnRoute";
    public static final String AMAZON_ASN_CREATE_ROUTE_ID = "AmazonASNCreateRoute";
    public static final String AMAZON_ASN_TRANSFORM_ROUTE_ID = "AmazonASNTransformRoute";
    public static final String AMAZON_ASN_UPDATE_ROUTE_ID = "AmazonASNUpdateRoute";
    public static final String AMAZON_PRODUCT_RECALL_ROUTE = "direct://AmazonProductRecallRoute";
    public static final String AMAZON_PRODUCT_RECALL_ROUTE_ID = "AmazonProductRecallRouteId";
    public static final String AMAZON_TRANS_SHIP_ORDER_ROUTE_ID = "AmazonTranshipOrderRoute";
    public static final String AMAZON_ORDER_CANCEL_ROUTE_ID = "AmazonOrderCancelRoute";
    public static final String AMAZON_CUSTOMER_RECALL_ROUTE_ID = "AmazonCustomerRecallRoute";
    public static final String AMAZON_OFR_NOTIFICATION_ROUTE_ID = "AmazonOFRNotificationRoute";
    public static final String AMAZON_NOTIFICATION_ROUTE_ID = "AmazonNotificationRoute";
    public static final String AMAZON_NEW_NOTIFICATION_ROUTE_ID = "AmazonNewNotificationRoute";
    public static final String AMAZON_NOTIFICATION_FETCHER_ROUTE_ID = "AmazonNotificationFetcherRoute";
    public static final String AMAZON_ORDER_PICK_ROUTE_ID = "AmazonOrderPickRoute";
    public static final String GODAM_PRODUCT_DETAIL_ROUTE_ID = "GodamProductDetailRouute";
    public static final String GODAM_PRODUCT_CHANNEL_MAPPINT_LIST_ROUTE_ID = "GodamProductChannelMappingListRouute";

    public static final String GODAM_PACKING_SLIP_UPLOAD_ROUTE = "direct://GodamPackingSlipUploadRoute";

    public static final String LEVIS_NOTIFICATION_ROUTE = "direct:levisNotificationRoute";


    public static final String GODAM_INVOICE_SLIP_UPLOAD_ROUTE = "direct://GodamInvoiceSlipUploadRoute";

    public static final String GODAM_PACKING_SLIP_UPLOAD_ROUTE_ID = "GodamPackingSlipUploadRoute";
    public static final String GODAM_INVOICE_SLIP_UPLOAD_ROUTE_ID = "GodamInvoiceSlipUploadRoute";
    public static final String SNAPDEAL_ORDER_CANCEL_CHECK_ROUTE = "direct://SnapdealOrderCancelCheckRoute";
    public static final String SNAPDEAL_ORDER_CANCEL_CHECK_ROUTE_ID = "SnapdealOrderCancelCheckRoute";
    public static final String SNAPDEAL_DROPSHIP_ORDER_CANCEL_CHECK_ROUTE_ID = "SnapdealDropshipOrderCancelCheckRoute";
    public static final String SNAPDEAL_ONESHIP_ORDER_CANCEL_CHECK_ROUTE_ID = "SnapdealOneshipOrderCancelCheckRoute";
    public static final String DEFAULT_ORDER_CANCEL_CHECK_ROUTE_ID = "DefaultOrderCancelCheckRoute";
    public static final String VOONIK_ORDER_CANCEL_CHECK_ROUTE = "direct://voonikOrderCancelCheckRoute";
    public static final String VOONIK_ORDER_CANCEL_CHECK_ROUTE_ID = "VoonikOrderCancelCheckRoute";
    public static final String VOONIK_VENDOR_FULFILLED_ORDER_CANCEL_CHECK_ROUTE = "direct://voonikvendor_fulfilledOrderCancelCheckRoute";
    public static final String VOONIK_VENDOR_FULFILLED_ORDER_CANCEL_CHECK_ROUTE_ID = "VoonikVendorFulfilledOrderCancelCheckRoute";
    public static final String VOONIK_FULFILLED_ORDER_CANCEL_CHECK_ROUTE = "direct://voonikvoonik_fulfilledOrderCancelCheckRoute";
    public static final String VOONIK_FULFILLED_ORDER_CANCEL_CHECK_ROUTE_ID = "VoonikFulfilledOrderCancelCheckRoute";
    public static final String VOONIK_ORDER_GENERATE_MANIFEST_ROUTE_ID = "VoonikOrderGenerateManifestRoute";
    public static final String VOONIK_ORDER_MANIFEST_ROUTE = "direct://voonikOrderManifest";
    public static final String VOONIK_MANIFEST_DOWNLOAD_ROUTE = "direct://voonikDownloadManifest";
    public static final String VOONIK_MANIFEST_DOWNLOAD_ROUTE_ID = "VoonikDownloadManifest";
    public static final String VOONIK_ORDER_MANIFEST_ROUTE_ID = "VoonikOrderManifestRoute";
    public static final String VOONIK_VENDOR_FULFILLED_ORDER_MANIFEST_ROUTE_ID = "VoonikVendorFulfilledOrderManifestRoute";
    public static final String VOONIK_FULFILLED_ORDER_MANIFEST_ROUTE_ID = "VoonikFulfilledOrderManifestRoute";
    public static final String FILE_FROM_LINK_DOWNLOAD_ROUTE_ID = "FileFromLinkdownloadRoute";
    public static final String PRODUCT_DETAILS_ROUTE = "direct:productDetailsRoute";
    public static final String ASN_DETAILS_ROUTE = "direct:ASNDetailsRoute";
    public static final String PRODUCT_DETAILS_ROUTE_ID = "ProductDetailsRoute";
    public static final String ASN_DETAILS_ROUTE_ID = "ASNDetailsRoute";
    public static final String INTERNAL_RETRY_ROUTE = "direct:internalRetryRoute";
    public static final String INTERNAL_RETRY_ROUTE_ID = "InternalRetryRoute";
    public static final String FLIPKART_CLIENT_UPDATE_ROUTE = "direct:flipkart-fbd-update-client-route";
    public static final String FLIPKART_CLIENT_UPDATE_ROUTE_ID = "flipkartClientUpdateRouteId";
    public static final String FLIPKART_FETCH_CLIENT_DETAILS_ROUTE = "direct:flipkart-fbd-fetch-client-route";
    public static final String FLIPKART_FETCH_CLIENT_DETAILS_ROUTE_ID = "flipkartFetchClientDetailsRoute";
    public static final String REJECT_BUCKET_ROUTE_ID = "RejectBucketRoute";
    public static final String REJECT_BUCKET_ROUTE = "direct:RejectBucketRoute";
    public static final String RETURN_PICKUP_ROUTE = "direct:ReturnPickupRoute";
    public static final String RETURN_PICKUP_ROUTE_ID = "ReturnPickupRoute";


    // Shopclues Route IDs
    public static final String PRODUCT_DETAIL_FETCHER_FOR_SHOPCLUES_ROUTE_ID = "ProductDetailFetcherForShopclues";
    public static final String SHOPCLUES_PACK_AT_CREATE_ROUTE_ID = "ShopcluesPackAtCreateRoute";
    public static final String SHOPCLUES_ORDER_CREATE_SCHEDULER_ROUTE_ID = "ShopcluesOrderCreateScheduler";
    public static final String SHOPCLUES_ORDER_PACK_ROUTE_ID = "ShopclueOrderPackRoute";
    public static final String SHOPCLUES_ORDER_MANIFEST_ROUTE_ID = "ShopclueOrderManifestRoute";
    public static final String SHOPCLUES_NOTIFICATION_ROUTE_ID = "ShopcluesNotificationRoute";
    public static final String EMAIL_ROUTE_ID = "InternalEmailRoute";
    public static final String EMAIL_DSNS_ROUTE_ID = "ExceptionEmailRoute";
    public static final String SHOPCLUES_ORDER_CANCEL_CHECK_ROUTE_ID = "ShopcluesCancelCheckRoute";

    //Amazon-MWS Route Ids
    public static final String AMAZON_MWS_NOTIFICATION_ROUTE_ID = "AmazonMWSNotificationRoute";


    public static final String AMAZON_MWS_ORDER_CANCEL_CHECK_ROUTE_ID = "AmazonMWSOrderCancelCheckRoute";

    //Xiaomi Route Ids
    public static final String XIAOMI_NOTIFICATION_ROUTE_ID = "XiaomiNotificationRoute";
    public static final String XIAOMI_ORDER_SHIP_NOTIFICATION_ROUTE_ID = "XiaomiOrderNotificationRoute";
    public static final String XIAOMI_ORDER_SHIP_NOTIFICATION_ROUTE = "direct:xiaomiOrderNotification";
    public static final String XIAOMI_DEFAULT_NOTIFICATION_ROUTE = "direct:xiaomiInventoryNotification";
    public static final String XIAOMI_DEFAULT_NOTIFICATION_ROUTE_ID = "XiaomiInventoryNotificationRoute";

    //voonik route ids
    public static final String VOONIK_FULFILLED_NOTIFICATION_ROUTE_ID = "VoonikFulfilledNotificationRoute";
    public static final String VOONIK_VENDOR_FULFILLED_NOTIFICATION_ROUTE_ID = "VoonikVendorFulfilledNotificationRoute";
    public static final String VOONIK_DEFAULT_NOTIFICATION_ROUTE = "direct:voonikDefaultNotificationRoute";
    public static final String VOONIK_DEFAULT_NOTIFICATION_ROUTE_ID = "VoonikDefaultNotificationRoute";
    public static final String VOONIK_FULFILLED_ORDER_PACK_ROUTE_ID = "VoonikFulfilledOrderPackRoute";

    public static final String ONEPLUS_NOTIFICATION_ROUTE = "direct:oneplusNotificationRoute";
    public static final String ONEPLUS_NOTIFICATION_ROUTE_ID = "OneplusNotificationRoute";

    public static final String EXCEPTION_HANDLER_ROUTE_ID = "ExceptionHandler";

    public static final String HILTI_ASN_CREATE_ROUTE_ID = "HiltiAsnCreateRoute";
    public static final String HILTI_ORDER_CREATE_V2_ROUTE_ID = "HiltiOrderCreateRoute";
    public static final String HILTI_WMS_V1_ORDER_CREATE_ROUTE = "direct:HiltiWMSV1OrderCreateRoute";
    public static final String HILTI_WMS_V1_ORDER_CREATE_ROUTE_ID = "HiltiWMSV1OrderCreateRouteID";
    public static final String HILTI_ORDER_CREATE_ROUTE_ID = "HiltiOrderCreateRouteID";

    // Queue constants
    public static final int QUEUE_THROTTLE_RATE = 10;
    public static final int QUEUE_READ_TIME_PERIOD = 1000;

    // RoadForm constants
    public static final String ROADFORM_CREATE_ROUTE_ID = "roadFormCreateRoute";
    public static final String ROADFORM_FETCH_ROUTE_ID = "roadFormFetchRoute";

    public static final String ORDER_DETAIL = "orderDetail";

    //Controller constants
    public static final String LOAD = "load";

    public static final String GODAM_AUTH_TOKEN_PREFIX = "Token ";
    public static final String JUNK_REGEX_CONSTANTS = "[^a-zA-Z0-9-_]";

    public static final Character COMMA_SEPARATOR = ',';

    public static final String PCF_SEPARATOR = "|";

    //Order Processing History Constants
    public static final String ORDER_PROCESSING_ROUTE_ID = "orderProcessingHistoryRoute";
    public static final String ORDER_PROCESSING_ROUTE = "direct:order-processing-history";
    public static final String ORDR_ID = "order_id";
    public static final String WAYBILL_NUMBER = "waybill_number";
    public static final String DATE_FROM = "date_from";
    public static final String DATE_TO = "date_to";


    // File Constants
    public static final Character CSV_SEPARATOR = ',';
    public static final Character CSV_QUOTE = '"';

    public static  final String LOGGING_LEVEL = "logging-level";
    public static final String FROM_ROUTE_ID = "fromRouteId";
    public static final String LOGGING_MESSAGE = "logging_message";

    public static final String FAILURE = "failure";
    public static final String PRODUCT_IMAGE = "product_image";
    public static final String IMEI = "IMEI";
    public static final String MASTER_CHANNEL = "MC";
    public static final int PRICE_SCALE = 2;

    public static final String APPOINTMENT= "appointment";

    public interface NotificationTypes {
        String PO_INVENTORY_ADJUSTMENT = "ims-inventory-adjustment_po";
        String ITEM_INVENTORY_ADJUSTMENT = "ims-inventory-adjustment";
        String ORDER_FAIL = "oms-order-fail";
        String ORDER_SHIP = "oms-order-ship";
        String DISPATCH_COMPLETE = "oms-dispatch-complete";
        String RETURNS_DISPATCH_COMPLETE = "returns-dispatch-complete";
        String RETURNS_PRODUCT_RECALL = "returns-product-recall";
        String ORDER_INVENTORY_SHORT = "oms-order-inventory_short";
        String ORDER_CANCEL = "oms-order-cancel";
        String INTEGRATOR_ORDER_CANCEL = "oms-in_order-cancel";
        String IMS_INVENTORY_ARRIVE = "ims-inventory-receipt";
        String IMS_SERIAL_SCAN = "ims-serial-scan";
        String IMS_INVENTORY_COUNTED = "ims-inventory-counted";
        String IMS_INVENTORY_UPDATE = "ims-inventory-update";
        String IMS_INVENTORY_QC_COMPLETE = "ims-inventory-adjustment_po";
        String INBOUND_ASN_CREATECALLBACK = "inbound-asn-createcallback";
        String PCM_PRODUCT_CREATECALLBACK = "pcm-product-createcallback";
        String ORDER_RETURNED = "oms-order-return";
        String ORDER_DELIVERED = "oms-order-deliver";
        String ORDER_RTS = "oms-order-ship";
        String ORDER_PACK = "oms-order-pack";
        String ORDER_PICK = "oms-order-pick";
        String IMS_INVENTORY_LEVEL = "ims-inventory-level";
        String INBOUND_PO_COMPLETE = "inbound-po-complete";
        String INBOUND_FTG_COMPLETE = "inbound-ftg-complete";
        String INBOUND_ASN_RETURN_CALLBACK = "inbound-asn-return_callback";
        String ORDER_VERIFY = "oms-order-verify";
        String ORDER_CREATE = "oms-order-create";
        String RETURN_RECALL_REQUEST="returns-recall-request";
        String RECALL_SHIP_REQUEST="returns-product-recall";
        String INBOUND_ASN_COMPLETE = "inbound-asn-complete";
        String TO_INBOUND_COMPLETE = "to-inbound-complete";
        String IMS_SERIAL_RECEIPT = "ims-serial-receipt";
        String INBOUND_ASN_CANCEL = "inbound-asn-cancel";

        String ORDER_ALLOCATION_STATE = "oms-order-allocate";
        String PICK_WAVE_COMPLETE = "oms-pickwave-complete";
        String TO_PICK_WAVE_COMPLETE = "to-pickwave-complete";
        String ORDER_RTS_STATE = "oms-order-rts";
        String ORDER_CANCEL_STATE = "oms-order-cancel";
        String ORDER_RETURN_PICKUP = "oms-return-pickup";
    }

    public interface ErrorMsgs {
        String PACK_RESPONSE_VALIDATION_FAILED = "Pack Response Load Failed Validation";
        String INVALID_PACK_RESPONSE = "Unable to parse received pack response. Invalid Json received";
        String INVALID_PICK_RESPONSE = "Unable to parse received pick response. Invalid Json received";
        String NOTIFICATION_TYPE_NOT_SUPPORTED = "Notification type not supported for this client.";
        String INVALID_NOTIFICATION_PAYLOAD = "Unable to parse incoming notification request data. Please provide correct data format.";
        String INVALID_ORDER_DATA_PAYLOAD = "Unable to parse client order data format. Please check request data format.";
        String INVALID_ASN_PAYLOAD = "Unable to parse incoming request load. Please provide correct data format.";
        String RELATIONSHIP_NOT_FOUND = "Relationship not found for order create";
        String PACK_BOX_SUGGESTION_VALIDATION = "Items weight can't be greater than 25.";
        String INVALID_MANIFEST_REQUEST = "Unable to parse received manifest request. Invalid csv received";
        String MANIFEST_REQUEST_VALIDATION_FAILED = "Manifest Request Load Failed Validation";
        String INVALID_RECALL_DATA_PAYLOAD = "Unable to parse incoming recall request data. Please provide correct data format.";
        String INVALID_WEIGHT_PAYLOAD = "Weight should be less than 25kg";
        String INVALID_APPOINTMENT_PAYLOAD = "Appointment Request Validation Failed";
        String UNSUCCESSFUL_ASN_RESPONSE = "Asn creation failed in godam";
        String INVALID_APPOINTMENT_REQUEST = "Unable to parse incoming request load. Please provide correct data format.";

        String ORDERID_OR_WAYBILL_MISSING = "order_id or waybill_number is missing";
        String DATE_MISSING = "One of the date is missing. Provide both the dates";
        String START_DATE_GREATER = "Start date %s should not be greater than end date %s";
        String UNABLE_TO_PARSE = "unable to parse order history request data";
    };

    public enum NotificationModel {
        OMS, IMS, PO, IRN, IAN, ASN, PRODUCT_CALLBACK, INBOUND, RETURNS
    }

    public enum LoggingLevel{
        INFO , WARN, ERROR
    }

    public enum ORDER_STATUS{

        PENDING("pending"),
        CANCELLED("cancelled");

        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        ORDER_STATUS(String value){
            setValue(value);
        }
    }

}
