package com.delhivery.godam.integrator.contracts.utils;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

import java.math.BigDecimal;

/**
 * @author Dewbrat Kumar on 20/2/18
 * class to create custom serializer provider
 * for jacson, it used to set default value and scaling
 * of decimal values
 */
public class CustomSerializerProvider extends DefaultSerializerProvider {


    public CustomSerializerProvider() { super(); }
    public CustomSerializerProvider(CustomSerializerProvider provider, SerializationConfig config,
                                    SerializerFactory jsf) {
        super(provider, config, jsf);
    }
    @Override
    public CustomSerializerProvider createInstance(SerializationConfig config,
                                                   SerializerFactory jsf) {
        return new CustomSerializerProvider(this, config, jsf);
    }

    @Override
    public JsonSerializer<Object> findNullValueSerializer(BeanProperty property) throws JsonMappingException {
        if (property.getType().getRawClass().equals(BigDecimal.class)) {
            return new BigDecimalJsonSerializer();
        } else {
            return super.findNullValueSerializer(property);
        }
    }
}
