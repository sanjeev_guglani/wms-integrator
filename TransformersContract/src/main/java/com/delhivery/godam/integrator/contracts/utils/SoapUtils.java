package com.delhivery.godam.integrator.contracts.utils;


import org.w3c.dom.Document;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;


public class SoapUtils {

    /**
     * method will convert incoming soapEnvelope into SoapMessage
     *
     * @param rawContent soapEnvelope in string form
     * @return parsed soapMessage
     * @throws SOAPException if invalid data
     * @throws IOException   if invalid data
     */

    public static SOAPMessage stringToSoapMessage(String rawContent) throws SOAPException, IOException {
        return MessageFactory.newInstance().createMessage(null,
                new ByteArrayInputStream(rawContent.getBytes()));
    }

    /**
     * method will covert incoming soapMessage body into string
     *
     * @param soapMessage soapMEssage
     * @return soapBody in string
     * @throws SOAPException        if invalid soapMessage
     * @throws TransformerException if invalid soapMessage
     */
    public static String soapBodyToString(SOAPMessage soapMessage) throws SOAPException, TransformerException {

        Document doc = soapMessage.getSOAPBody().extractContentAsDocument();
        StringWriter sw = new StringWriter();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.transform(new DOMSource(doc), new StreamResult(sw));
        return sw.toString();
    }
}
