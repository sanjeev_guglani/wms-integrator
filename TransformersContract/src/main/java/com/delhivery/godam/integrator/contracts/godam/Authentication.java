package com.delhivery.godam.integrator.contracts.godam;

import com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages;
import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by delhivery on 15/4/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Authentication {

    @JsonProperty("username")
    @NotNull(message = ValidationMessages.NOT_NULL_MESSAGE)
    @Size(max = 255, message = ValidationMessages.CHAR_LIMIT_MESSAGE)
    private String username;

    @JsonProperty("token")
    @NotNull(message = ValidationMessages.NOT_NULL_MESSAGE)
    @Size(max = 255, message = ValidationMessages.CHAR_LIMIT_MESSAGE)
    private String token;

    @JsonProperty("fc_sk")
    private String fcSk;

    @JsonProperty("fulfillment_center_num")
    private String fcNum;

    @JsonProperty("client_access_key")
    private String clientAccessKey;

    @JsonProperty("supplier_num")
    private String supplierNum;

    @JsonProperty("end_point")
    @NotNull(message = ValidationMessages.NOT_NULL_MESSAGE)
    @Size(max = 255, message = ValidationMessages.CHAR_LIMIT_MESSAGE)
    private String endPoint;

    @JsonProperty("password")
    @NotNull(message = ValidationMessages.NOT_NULL_MESSAGE)
    @Size(max = 255, message = ValidationMessages.CHAR_LIMIT_MESSAGE)
    private String password;

    @JsonProperty("client_sk")
    private String clientSk;

    @JsonProperty("client_store_sk")
    @NotNull(message = ValidationMessages.NOT_NULL_MESSAGE)
    @NotEmpty(message = ValidationMessages.NOT_NULL_MESSAGE)
    @Size(max = 255, message = ValidationMessages.CHAR_LIMIT_MESSAGE)
    private String clientStoreSk;

    /**
     * Gets client access key.
     *
     * @return the client access key
     */
    public String getClientAccessKey() {
        return clientAccessKey;
    }

    /**
     * Sets client access key.
     *
     * @param clientAccessKey the client access key
     */
    public void setClientAccessKey(String clientAccessKey) {
        this.clientAccessKey = clientAccessKey;
    }

    /**
     * Gets fc num.
     *
     * @return the fc num
     */
    public String getFcNum() {
        return fcNum;
    }

    /**
     * Sets fc num.
     *
     * @param fcNum the fc num
     */
    public void setFcNum(String fcNum) {
        this.fcNum = fcNum;
    }

    /**
     * Gets fc sk.
     *
     * @return the fc sk
     */
    public String getFcSk() {
        return fcSk;
    }

    /**
     * Sets fc sk.
     *
     * @param fcSk the fc sk
     */
    public void setFcSk(String fcSk) {
        this.fcSk = fcSk;
    }

    /**
     * Gets supplier num.
     *
     * @return the supplier num
     */
    public String getSupplierNum() {
        return supplierNum;
    }

    /**
     * Sets supplier num.
     *
     * @param supplierNum the supplier num
     */
    public void setSupplierNum(String supplierNum) {
        this.supplierNum = supplierNum;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets end point.
     *
     * @return the end point
     */
    public String getEndPoint() {
        return endPoint;
    }

    /**
     * Sets end point.
     *
     * @param endPoint the end point
     */
    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    /**
     * Gets client sk.
     *
     * @return the client sk
     */
    public String getClientSk() {
        return clientSk;
    }

    /**
     * Sets client sk.
     *
     * @param clientSk the client sk
     */
    public void setClientSk(String clientSk) {
        this.clientSk = clientSk;
    }

    /**
     * Gets default channel headers.
     *
     * @return the default channel headers
     */
    @JsonIgnore
    public HttpHeaders getDefaultChannelHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", String.format("Token %s",this.getToken()));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * Gets client store sk.
     *
     * @return the client store sk
     */
    public String getClientStoreSk() {
        return clientStoreSk;
    }

    /**
     * Sets client store sk.
     *
     * @param clientStoreSk the client store sk
     */
    public void setClientStoreSk(String clientStoreSk) {
        this.clientStoreSk = clientStoreSk;
    }

    /**
     * copy constructor to make a copy of the authentication object
     *
     * @param authentication the authentication
     */
    public Authentication(Authentication authentication){
        setClientStoreSk(authentication.getClientStoreSk());
        setEndPoint(authentication.getEndPoint());
        setPassword(authentication.getPassword());
        setToken(authentication.getToken());
        setUsername(authentication.getUsername());
    }

    /**
     * default constructor
     */
    public Authentication(){

    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }
}
