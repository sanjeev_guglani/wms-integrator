package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Consignee {

    @JsonProperty("name")
    private String name;

    @JsonProperty("address_line1")
    private String addressLine1;

    @JsonProperty("address_line2")
    private String addressLine2;

    @JsonProperty("pin_code")
    private Long pinCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("country")
    private String country;

    @JsonProperty("primary_phone_number")
    private String primaryPhoneNumber;

    @JsonProperty("secondary_phone_number")
    private String secondaryPhoneNumber;

    @JsonProperty("email")
    private String email;
}
