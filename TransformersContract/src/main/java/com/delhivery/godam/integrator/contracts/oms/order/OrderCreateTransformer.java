package com.delhivery.godam.integrator.contracts.oms.order;

import com.delhivery.godam.integrator.contracts.TransformerHelper;
import com.delhivery.godam.integrator.contracts.exceptions.OrderDataException;
import com.delhivery.godam.integrator.contracts.oms.model.OrderContract;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanjeev Guglani on 28/3/19.
 *
 * @param <C> the type parameter represent Client ASN Type
 * @param <G> the type parameter represent Godam ASN Type
 */
public interface OrderCreateTransformer<C,G extends OrderContract> {

    /**
     * The constant logger.
     */
    Logger logger = LoggerFactory.getLogger(OrderCreateTransformer.class);


    /**
     * Gets client orders.
     *
     * @param orderRawData the order raw data
     * @return the client orders
     * @throws OrderDataException the order data exception
     */
    List<C> getClientOrders(String orderRawData) throws OrderDataException;

   
    /**
     * method to validate list of orders
     * @param orders consume orders
     */
    default List<TransformerHelper<C>> validate(List<C> orders) {
        List<TransformerHelper<C>> helpers = new ArrayList<>();
        orders.forEach(order -> {
            try{
                validate(order);
                helpers.add(new TransformerHelper<>(order));
            }catch (Validator.ValidationException exception){
                helpers.add(new TransformerHelper<>(exception.getErrorList(), order));
            }
        });
        return helpers;
    }

    /**
     * method to validate client order model
     * @param order client order
     * @throws Validator.ValidationException if data validation fails
     */
    void validate(C order) throws Validator.ValidationException;

    /**
     * To godam order list.
     *
     * @param order the order
     * @return the list
     */
    List<G> toGodamOrder(List<C> order);


}
