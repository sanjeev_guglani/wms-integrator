package com.delhivery.godam.integrator.contracts.adaptor;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by delhivery on 24/6/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdapterRequestPayload {

    private String pulledObject;

    @JsonProperty("meta")
    private AdapterRequestMeta meta;

    public AdapterRequestMeta getMeta() {
        return meta;
    }

    public void setMeta(AdapterRequestMeta meta) {
        this.meta = meta;
    }

    @JsonGetter("orders")
    public String getPulledObject() {
        return pulledObject;
    }

    @JsonSetter("orders")
    public void setPulledObject(String pulledObject) {
        this.pulledObject = pulledObject;
    }

    @JsonSetter("data")
    private void setPulledDataObject(String data){
        this.pulledObject = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
