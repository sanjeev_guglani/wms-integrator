package com.delhivery.godam.integrator.contracts;


import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.oms.order.OrderCreateTransformer;

/**
 * Created by sanjeev guglani on 5/21/18.
 */
public interface OMSTransformerFactory {

    OrderCreateTransformer getOrderCreateTransformer() throws TransformerNotFoundException;
}
