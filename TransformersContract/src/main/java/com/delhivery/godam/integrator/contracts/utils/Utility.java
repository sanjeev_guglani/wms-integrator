package com.delhivery.godam.integrator.contracts.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.util.stream.Collectors.joining;

/**
 * class to define general use case methods used in whole project
 * Created by administrator on 12/1/17.
 */
public class Utility {

    private static String dateTimeFormatSpecifier = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    private static String timeZone = "Asia/Kolkata";

    /*
        ^\s*                #Line start, match any whitespaces at the beginning if any.
        (?:\+?(\d{1,5}))?   #GROUP 1: The country code. Optional.
        [-. (]*             #Allow certain non numeric characters that may appear between the Country Code and the Area Code.
        (\d{1,5})           #GROUP 2: The Area Code. Required.
        [-. )]*             #Allow certain non numeric characters that may appear between the Area Code and the Exchange number.
        (\d{1,3})           #GROUP 3: The Exchange number. Optional.
        [-. ]*              #Allow certain non numeric characters that may appear between the Exchange number and the Subscriber number.
        (\d{4})             #Group 4: The Subscriber Number. Required.
        (?: *x(\d+))?       #Group 5: The Extension number. Optional.
        \s*$                #Match any ending whitespaces if any and the end of string.
   */
    private static String phoneNumberRegEx = "^\\s*(?:\\+?(\\d{1,5}))?[-. (]*(\\d{1,5})[-. )]*(?:(\\d{1,3}))[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";

    //Utility class so private constructor
    private Utility() {

    }

    public static String createUUID() {
        return UUID.randomUUID().toString();
    }

    public static String getGodamFormatDateTime(String orderDate) {
        try {
            ZonedDateTime zonedDateTime = null;
            boolean isZonedTime = false;

            try {
                zonedDateTime = ZonedDateTime.parse(orderDate);
                isZonedTime = true;
            } catch (Exception ignored) {

            }

            LocalDateTime localDateTime = null;
            if (!isZonedTime) {
                try {
                    localDateTime = LocalDateTime.parse(orderDate, DateTimeFormatter.ISO_DATE_TIME);
                } catch (DateTimeParseException ex) {
                    try {
                        String time = orderDate.trim().replace(' ', 'T');
                        localDateTime = LocalDateTime.parse(time, DateTimeFormatter.ISO_DATE_TIME);
                    } catch (DateTimeParseException e) {
                        localDateTime = LocalDate.parse(orderDate).atTime(LocalTime.now());
                    }
                }
            }

            DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
            builder.appendPattern(dateTimeFormatSpecifier);
            DateTimeFormatter dateTimeFormatter = builder.toFormatter();
            if (!isZonedTime) {
                return ZonedDateTime.of(localDateTime, ZoneId.of(timeZone)).format(dateTimeFormatter);
            }
            return dateTimeFormatter.format(zonedDateTime.withZoneSameInstant(ZoneId.of(timeZone)));

        } catch (Exception e) {
            throw new RuntimeException("Invalid time format found.", e);
        }
    }

/*
   public static void main(String[] args){
        System.out.println(getGodamFormatDateTime("2017-06-01 09:46:03.494000"));

       DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
       builder.appendPattern(DateTimeFormats.FORMAT_FIVTH.getDateFormat());
       DateTimeFormatter dateTimeFormatter = builder.toFormatter();
        System.out.println(getGodamFormatDateTime("2017-06-01"));
        System.out.println(getGodamFormatDateTime("2017-06-01T09:46:03.494000"));
        System.out.println(getGodamFormatDateTime("2017-06-01T09:46:03"));
        System.out.println(getGodamFormatDateTime("1997-07-16T19:20:30+01:00"));
        System.out.println(getGodamFormatDateTime("2017-06-01T09:46:03.49+05:30"));
        System.out.println(getGodamFormatDateTime("2017-07-18 10:48:55"));
        System.out.println(getGodamFormatDateTime("2017-07-18T10:48:55"));
    }
*/

    /**
     * USED for custom DateTime string format
     * Converts incoming orderDate string in the client response into Datetime format expected by godam
     *
     * @param orderDate         the orderDate string that comes in the response
     * @param dateTimeFormatter to indicate the format in which the orderDate string is
     * @return DateTime format as expected by godam
     */
    public static String getGodamFormatDateTime(String orderDate, DateTimeFormatter dateTimeFormatter) {
        try {
            LocalDate zonedDateTime = LocalDate.parse(orderDate, dateTimeFormatter);
            DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
            builder.appendPattern(dateTimeFormatSpecifier);
            DateTimeFormatter godamDateTimeFormat = builder.toFormatter();
            return ZonedDateTime.of(zonedDateTime, LocalTime.MIDNIGHT, ZoneId.of(timeZone)).format(godamDateTimeFormat);
        } catch (Exception e) {
            //TODO Remove Throwing Runtime Exception
            throw new RuntimeException("Invalid time format found.", e);
        }
    }


    /**
     * method to get zoned date time object from date time string.
     *
     * @param orderDate order date in String
     * @return zoned date time object
     */
    public static ZonedDateTime getGodamFormatZonedDateTime(String orderDate) {
        String dateTime = getGodamFormatDateTime(orderDate);
        return ZonedDateTime.parse(dateTime);
    }

    /**
     * USED for custom DateTime string format
     * Converts incoming date string in the given datetime format
     *
     * @param date              the date string
     * @param formats to indicate the format in which the date string to be formatted
     * @return DateTime format as expected as ZonedDateTime
     */
    public static ZonedDateTime getZonedDateTimeForDate(String date, String... formats) {
        DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
        for(String format : Arrays.asList(formats))
            builder.appendOptional(DateTimeFormatter.ofPattern(format));

        return LocalDate.parse(date, builder.toFormatter()).atStartOfDay(ZoneId.of("+05:30"));

    }

    public static String getCurrentDateForClient() {
        return Instant.now().atZone(ZoneId.of(timeZone)).toLocalDate().toString().replaceAll("-", "");
    }

    public static String getDateMinusDaysForClient(int days) {
        return Instant.now().atZone(ZoneId.of(timeZone)).minusDays(days).toLocalDate().toString().replaceAll("-", "");
    }

    /**
     * get current time in given format
     * e.g format = HHmmSSS will give 1535123
     *
     * @param format time format
     * @return current time
     */
    public static String getCurentTimeInFormat(String format) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return now.format(formatter);
    }

    /**
     * get current date in given format
     * e.g format = ddMMyyyy will give 15122018
     *
     * @param format date format
     * @return current date
     */
    public static String getCurentDateInFormat(String format) {
        LocalDate now = LocalDate.now(ZoneId.of(timeZone));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return now.format(formatter);
    }

    public static String getCurrentTime() {
        return ZonedDateTime.now(ZoneId.of("GMT")).toOffsetDateTime().toString();
    }

    /**
     * method to get current IST date and time
     *
     * @return string value of current IST date time
     */
    public static String getCurrentISTDateTime() {
        return Instant.now().atZone(ZoneId.of(timeZone)).toLocalDateTime().toString();
    }

    /**
     * Used to convert String request into MD5 hash data.
     *
     * @param data string data for generate md5 hash
     * @return encrypty md5 hash of raw data
     * @throws NoSuchAlgorithmException if encryption not supported
     */
    public static String toMD5Hash(String data) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] byteData = md.digest(data.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : byteData) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();

    }

    /**
     * Used to convert String request into Base64 hash data.
     *
     * @param data raw data to encode in base 64
     * @return encoded string
     */
    public static String toBase64Encode(String data) {
        return Base64.getEncoder().encodeToString(data.getBytes());
    }

    /**
     * Used to convert string into md5 hash and encode it into base64
     *
     * @param message method to generate md5 hash and get it's base64 encode string.
     * @return base64 encoded md5 hash
     * @throws NoSuchAlgorithmException if encryption not supported
     */
    public static String getBase64MD5Hash(String message) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");

        return new String(Base64Utils.encode(md.digest(message.getBytes())));
    }

    /**
     * this method created to avoid logging epoch time in milli seconds or seconds at various places inside project.
     *
     * @return current Epoch time in second.
     */
    public static long getCurrentEpochTime() {
        return Instant.now().getEpochSecond();
    }

    /**
     * method is used to create get route name by passing
     * channel name and activity for a route
     *
     * @param channel  channelName
     * @param activity activity
     * @return routeEndpoint
     */
    public static String getRouteEndpoint(String channel, String activity) {
        return String.format("direct:%s%sRoute", channel.toLowerCase(), activity);
    }

    /*
    * utility method to get trimmed stack trace for log.
     * @param exception exception occurred
     * @return  string trace of exception
     */
    public static String getTraceToLog(Exception exception) {
        if (Objects.nonNull(exception)) {
            return ExceptionUtils.getStackTrace(exception);
        }
        return StringUtils.EMPTY;
    }

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return StringUtils.EMPTY;
        }
    }

    public static String getEncodedUrlBody(Map<String, String> uriParams) {
        return uriParams.keySet().stream()
                .map(key -> key + "=" + encodeValue(uriParams.get(key)))
                .collect(joining("&", "", ""));
    }

    /**
     * method to validate auth token for v2 api
     *
     * @param authToken v2 api auth token
     */
    public static void validateVTwoAuthToken(String authToken) {
        Assert.notNull(authToken, "Auth token cannot be null.");
        if (StringUtils.isNotEmpty(authToken) && StringUtils.startsWithIgnoreCase(authToken, "Token")) {
            throw new RuntimeException(String.format("invalid auth token scheme for v2 api's. token : [%s]", authToken));
        }
    }

    /**
     * method to change auth token to v2 api token
     * removes Token prefix from auth token
     *
     * @param authToken auth token
     * @return returns modified token
     */
    public static String changeTokenToVTwoScheme(String authToken) {
        Assert.notNull(authToken, "Auth token cannot be null.");
        return StringUtils.removeStartIgnoreCase(authToken, "Token").trim();
    }

    /**
     * method to remove all non ascii chars from String
     *
     * @param nonCleanData non cleaned String
     * @return clean string
     */
    public static String cleanNonAsciiChars(String nonCleanData) {
        if (StringUtils.isBlank(nonCleanData))
            return nonCleanData;
        return nonCleanData.replaceAll("[^\\x00-\\x7F]", StringUtils.EMPTY);
    }

    /**
     * get zoned date time with default zone if of +05:30
     *
     * @param date date
     * @return zoned date time
     */
    public static ZonedDateTime getZonedDateTime(String date) {
        return getZonedDateTime(date, "+05:30");
    }

    /**
     * this method will be used to get zoned date time
     * for multiple datetime formats
     * <p>
     * refer to UtilityTest to check all
     * supported date format examples
     *
     * @param date date
     * @return zoned date time
     */
    public static ZonedDateTime getZonedDateTime(String date, String zoneId) {

        CommonDateTimeFormatter commonDateTimeFormatter = CommonDateTimeFormatter.getCommonDateTimeFormatter();
        DateTimeFormatter dateTimeFormatter = commonDateTimeFormatter.getDateTimeFormatter();

        return ZonedDateTime.of(LocalDateTime.parse(date, dateTimeFormatter), ZoneId.of(zoneId));

    }

    /**
     * extract number from given string
     *
     * @param number number
     * @return extracted number
     */
    public static String extractMobileNumber(String number) {
        if (StringUtils.isBlank(number)){
            return number;
        }else{
            String phoneNumber = removeNonDigitCharacters(number);
            boolean matches = phoneNumber.matches(phoneNumberRegEx);
            return  matches ? StringUtils.right(phoneNumber, 10) : number;
        }
    }

    /**
     * removes +91 / 0 type prefixes from given number
     * ^ represents replace only if a prefix
     * \\ to escape + and accept as a literal
     * | OR i.e +91 or 0
     *
     * @param number number
     * @return number without prefix
     */
    public static String removeNumberPrefixes(String number) {
        Assert.isTrue(StringUtils.isNotBlank(number), "string cannot be empty before removing prefix");
        return number.trim().replaceFirst("^(\\+91|0)", StringUtils.EMPTY);
    }

    /**
     * removes non digit characters from string
     *
     * @param number number
     * @return number
     */
    public static String removeNonDigitCharacters(String number) {
        Assert.isTrue(StringUtils.isNotBlank(number), "string cannot be empty before removing non digit characters");
        return number.replaceAll("[\\D]", StringUtils.EMPTY).trim();
    }

    /**
     * get Zoned time with Z appended.
     * @param date original date to be modified
     * @return zoned time with Z appended
     */
    public static String getZAppendedDate(String date) {
        String modifiedDate = null;
        if(Objects.nonNull(date)) {
            date = Utility.getGodamFormatDateTime(date);
            modifiedDate = OffsetDateTime.parse(date).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            modifiedDate = modifiedDate + "Z";

        }
        return modifiedDate;
    }

}
