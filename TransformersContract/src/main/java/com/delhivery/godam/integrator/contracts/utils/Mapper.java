package com.delhivery.godam.integrator.contracts.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * utility class for logging
 */
public class Mapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(Mapper.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String toString(Object object){
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("error in json convert. class {} trace {}",
                    object.getClass().getCanonicalName(), ExceptionUtils.getStackTrace(e));
            return ToStringBuilder.reflectionToString(object);
        }
    }

}
