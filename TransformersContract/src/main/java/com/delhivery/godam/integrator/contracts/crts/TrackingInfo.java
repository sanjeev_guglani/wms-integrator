package com.delhivery.godam.integrator.contracts.crts;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TrackingInfo {

    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("api_key")
    private String apiKey;

    @JsonProperty("entity")
    private Entity entity;

    @JsonProperty("participant_id")
    private String participantId;

    @JsonProperty("uid")
    private String uniqueId;

    @JsonProperty("rec_state")
    private String recordState;

    @JsonProperty("channel")
    private String channel;

    @JsonProperty("req_state")
    private RequestState requestState;

    // preferred type is a pojo or list of pojo
    private Object data;

    private List<Map<String, Object>> errors;

    /**
     * entity defines type of data e.g order/product etc
     */
    public enum Entity {

        ORDER, PRODUCT, ASN, NOTIFICATION, SUPPLIER,WAYBILL;

        @JsonCreator
        public static Entity fromString(String key) {
            return Objects.isNull(key)
                    ? null
                    : Entity.valueOf(StringUtils.trim(key.toUpperCase()));
        }
    }

    public List<Map<String, Object>> getErrors() {
        if(Objects.isNull(errors)){
            errors = new ArrayList<>();
        }
        return errors;
    }

    /**
     * log state defines state while logging e.g in_progress, completed, failed
     */
    public enum RequestState {

        QUEUED, IN_PROGRESS, FAILED, PARTIALLY_FAILED, COMPLETED, SUCCESS;
        @JsonCreator
        public static RequestState fromString(String key) {
            return Objects.isNull(key)
                    ? null
                    : RequestState.valueOf(StringUtils.trim(key.toUpperCase()));
        }
    }

    @Override
    public String toString() {
         return Mapper.toString(this);
    }
}
