package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * @author Dewbrat Kumar on 16/4/18
 *
 * Exception class thrown when product status mapping not found
 * in case of order cancellation
 */
public class ProductStatusMappingNotFoundException extends Exception {

    private String channel;
    private String productStatus;

    public ProductStatusMappingNotFoundException(String channel, String productStatus){
        super("Product status mapping not defined");
        this.channel = channel;
        this.productStatus = productStatus;
    }

    @Override
    public String getMessage() {
        return String.format("%s channel :: [%s] product status :: [%s]", super.getMessage(), channel, productStatus);
    }
}
