package com.delhivery.godam.integrator.contracts.commons.constants;

/**
 * Created by delhivery on 3/5/16.
 */
public class ValidationMessages {

    public static final String NOT_NULL_MESSAGE = "This is a required field, can not be null/empty.";
    public static final String NOT_EMPTY_MESSAGE="This is a required field, can not be empty.";
    public static final String NOT_BLANK_MESSAGE = "This is a required field, can not be null/empty.";
    public static final String CHAR_LIMIT_MESSAGE = "Field value should be less than 255 characters.";
    public static final String CHAR_500_LIMIT_MESSAGE = "Field value should be less than 500 characters.";
    public static final String GSTIN_VALIDATION_MESSAGE = "This field's value should be precisely 15 characters long.";
    public static final String PIN_CODE_VALIDATION_MESSAGE = "This field's value should be 3-9 characters long.";
    public static final String PHONE_NUMBER_VALIDATION_MESSAGE = "This field's value should be precisely 10 characters long.";
    public static final String PRE_PHONE_NUMBER_VALIDATION_MESSAGE = "This field's value should be between 10-14 characters long.";
    public static final String PRICE_VALIDATION_MESSAGE = "This field's value should be positive and in number format.";
    public static final String VALUE_GREATER_THAN_1 = "This field's value should be greater than 1.";
    public static final String VALUE_GREATER_THAN_0 = "This field's value should be greater than 0.";
    public static final String MAX_DIGIT_ALLOWED_12 = "This field maximum digit allowed is 12";
    public static final String SUCCESSFULLY_QUEUED = "Request has been successfully queued.";
    public static final String DECIMAL_POINT_LIMIT_MESSAGE = "Field has exceeded decimal points limit";
    public static final String EMAIL_VALIDATION_MESSAGE = "Email is invalid.";
    public static final String URL_VALIDATION_MESSAGE = "Url is invalid.";
    public static final String COD_AMOUNT_NOT_NULL_FOR_COD = "Cod Amount cannot be null with COD payment mode";
    public static final String PARENT_OR_COMBO_CHILD_INVALID = "parent for this child is missing or invalid OR other products in this combo invalid";
    public static final String INVALID_CHILD_MESSAGE = "mandatory combo fields - master_sku or quantity missing";
    public static final String UPDATE_PRODUCT_WITH_COMBO_FIELDS = "combo fields : combo/master_sku/quantity cannot be defined while update";
    public static final String CHILD_WITH_NO_PARENT = "child with no or missing or non-combo master_sku defined";
    public static final String NULL_COMBO_WITH_CHILD_MESSAGE = "combo null but found child associated ";
    public static final String TRUE_COMBO_WITH_NO_CHILD_MESSAGE = "combo true but no child associated ";
    public static final String NOT_REQUIRED_FIELDS_ADDED = "combo property is null/true, master_sku/quantity expected to be null";
}