package com.delhivery.godam.integrator.contracts;

import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.inbound.ASNCreateTransformer;


/**
 * Created by rahulbeniwal on 17/8/16.
 */
public interface InboundTransformerFactory {

    ASNCreateTransformer getASNV2Transformer() throws TransformerNotFoundException;
}
