package com.delhivery.godam.integrator.contracts.godam;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.CHAR_LIMIT_MESSAGE;
import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by delhivery on 27/6/16.
 *
 * @param <T> the type parameter
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadWrapper<T> implements GodamRequestBody<T> {

    @JsonProperty("authentication")
    @NotNull(message = NOT_NULL_MESSAGE)
    @Valid
    private Authentication authentication;

    @JsonProperty("channel")
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_NULL_MESSAGE)
    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    private String channel;

    @JsonProperty("load")
    @NotNull(message = NOT_NULL_MESSAGE)
    @Valid
    private T payload;

    @JsonProperty("activity")
    private String activity;

    /**
     * Gets activity.
     *
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets activity.
     *
     * @param activity the activity
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }


}
