package com.delhivery.godam.integrator.contracts;

import com.delhivery.godam.integrator.contracts.oms.model.OrderCreateParameters;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Sanjeev Guglani on 2/5/19.
 */

@Builder
@Data
public class AbstractBlob {

    @JsonProperty("workflow_id")
    private String workflowId;

    @JsonUnwrapped
    private OrderCreateParameters orderCreateParameters;

}
