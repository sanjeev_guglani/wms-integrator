package com.delhivery.godam.integrator.contracts.oms.model;

/**
 * The enum Order type.
 */
public enum OrderType {

    /**
     * Fwd order type.
     */
    FWD("FWD"),
    /**
     * Rvo order type.
     */
    RVO("RVO"),
    /**
     * To order type.
     */
    TO("TO");

    private String orderType;

    OrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * Gets order type.
     *
     * @return the order type
     */
    public String getOrderType() {
        return orderType;
    }
}
