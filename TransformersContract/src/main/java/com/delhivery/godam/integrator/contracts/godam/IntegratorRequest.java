package com.delhivery.godam.integrator.contracts.godam;

import com.delhivery.godam.integrator.contracts.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by delhivery on 22/6/16.
 *
 * @param <P> the type parameter
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntegratorRequest<P> extends Request<P> {

    private String requestId;

    private String channel;

    /**
     * Gets channel.
     *
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets channel.
     *
     * @param channel the channel
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * Gets request id.
     *
     * @return the request id
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets request id.
     *
     * @param requestId the request id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
