package com.delhivery.godam.integrator.contracts.exceptions;


import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Created by marutsingh on 5/21/16.
 */
public class TransformerNotFoundException extends Exception {

    private String channelName;
    private String method;

    public TransformerNotFoundException(){
        super();
    }
    public TransformerNotFoundException(@NotNull String channelName){
        super();
        this.channelName = channelName;
    }

    public TransformerNotFoundException(@NotNull String channelName, @NotNull String method){
        super();
        this.channelName = channelName;
        this.method = method;
    }

    public String getMessage() {
        if (Optional.ofNullable(method).isPresent()){
            return String.format("No Transformer found for channel [%s] method [%s]", channelName, method);
        }
        return String.format("No Transformer found for channel [%s] ", channelName);
    }
}
