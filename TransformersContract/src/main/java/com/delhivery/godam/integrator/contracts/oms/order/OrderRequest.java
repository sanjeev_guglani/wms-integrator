package com.delhivery.godam.integrator.contracts.oms.order;

import com.delhivery.godam.integrator.contracts.model.Request;
import com.delhivery.godam.integrator.contracts.utils.Mapper;

/**
 * Created by delhivery on 22/6/16.
 */
public class OrderRequest<T> extends Request<T> {

    private String requestId;

    private String channel;

    private String activity;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {

        return Mapper.toString(this);

    }
}
