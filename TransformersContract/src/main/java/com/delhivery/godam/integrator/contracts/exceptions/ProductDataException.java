package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * Created By Sanjeev Guglani
 * exception class to wrap all external order format errors.
 * external orders come through REST or pulled from market places etc.
 */
public class ProductDataException extends DataException {

    // private String client;
    private Object data;

    public ProductDataException(String message) {
        super(message);
    }

    public ProductDataException(String client, String message, Object data) {
        super(message);
        this.client = client;
        this.data = data;
    }

    public ProductDataException(String client, String message, Object data, Exception e) {
        super(message, e);
        this.client = client;
        this.data = data;
    }

    public ProductDataException(String client, ErrorMessage errorMessage, String activity, Object data) {
        super(errorMessage.getMessage(client, activity));
        this.client = client;
        this.activity = activity;
        this.data = data;
    }


    @Override
    public String getMessage() {
        return String.format("client: %s, error: [%s], data :: %s", client, super.getMessage(), data);
    }

    public String getPlainMessage() {
        return super.getMessage();
    }
}
