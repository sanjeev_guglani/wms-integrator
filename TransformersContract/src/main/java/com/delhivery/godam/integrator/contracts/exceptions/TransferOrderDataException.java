package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * exception class to wrap all external return format errors.
 * return request come through REST or pulled from market places etc.
 * Created by Dewbrat Kumar on 21/9/17.
 */
public class TransferOrderDataException extends Exception{
    private String client;
    private Object data;

    public TransferOrderDataException(String client, String message, Object data) {
        super(message);
        this.client = client;
        this.data = data;
    }

    public TransferOrderDataException(String client, String message, Object data, Exception e) {
        super(message, e);
        this.client = client;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return String.format("client [%s], error [%s], data :: %s", client, super.getMessage(), data);
    }
}
