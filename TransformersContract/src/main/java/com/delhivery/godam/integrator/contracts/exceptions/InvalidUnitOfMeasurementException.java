package com.delhivery.godam.integrator.contracts.exceptions;

/*
    created by sachin bhogal
    exception is used to capture when we received
    invalid unit of measurement factor for product f
 */
public class InvalidUnitOfMeasurementException extends RuntimeException {

    private final String channel;

    private final String productId;

    private final String uom;

    private final String message;

    private final String referenceNumber;

    private final String warehouse;

    private final int quantity;

    public InvalidUnitOfMeasurementException(String channel, String message, String productId,
                                             String uom, String referenceNumber, String warehouse, int quantity) {
        super(message);
        this.channel = channel;
        this.productId = productId;
        this.uom = uom;
        this.message = message;
        this.referenceNumber = referenceNumber;
        this.warehouse = warehouse;
        this.quantity = quantity;
    }

    public String getProductId() {
        return productId;
    }

    public String getUom() {
        return uom;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String getMessage() {
        return String.format("error : [%s], channel [%s], Warehouse : [%s], " +
                "Reference No : [%s], Item Id [%s], UOM [%s], Quantity [%s].", message,channel, warehouse, referenceNumber, productId, uom, quantity);
    }
}
