package com.delhivery.godam.integrator.contracts.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by delhivery on 12/4/16.
 */
public class Validator {

    private static final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    public static class ValidationException extends Exception {

        private List<String> errorList = new ArrayList<>();
        private List<String> plainMessageList = new ArrayList<>();
        private String data;

        public ValidationException(List<String> errorList, List<String> plainMessageList) {
            super();
            this.errorList = errorList;
            this.plainMessageList = plainMessageList;
        }


        public List<String> getErrorList() {
            return errorList;
        }

        public void setErrorList(List<String> errorList) {
            this.errorList = errorList;
        }

        public List<String> getPlainMessageList() {
            return plainMessageList;
        }

        public void setPlainMessageList(List<String> plainMessageList) {
            this.plainMessageList = plainMessageList;
        }

        public String getErrorListAsString() {
            return getErrorList().toString();
        }

        public String getPlainMessageListAsString() {
            return getPlainMessageList().toString();
        }

        public String getData() {
            return data;
        }

        @Override
        public String getMessage() {
            return getErrorListAsString();
        }

        @Override
        public String toString() {
            try {
                return new ObjectMapper().writeValueAsString(this);
            } catch (JsonProcessingException e) {
                return super.toString();
            }
        }
    }

    private static <T> Set<ConstraintViolation<T>> getConstraintViolationSet(T t) {
        return factory.getValidator().validate(t);
    }

    private static <T> Set<ConstraintViolation<T>> getConstraintViolationSet(T t, Class... validationGroups) {
        return factory.getValidator().validate(t, validationGroups);
    }

    private static <T> List<String> getError(Set<ConstraintViolation<T>> validation) {
        if (!validation.isEmpty()) {
            return validation.stream().map(constraintViolation -> String.format(
                    "Property: {%s}, Model: {%s}, Error: {%s}",
                    constraintViolation.getPropertyPath().toString(),
                    constraintViolation.getRootBeanClass().getSimpleName(),
                    constraintViolation.getMessage())).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private static <T> List<String> getErrorMessages(Set<ConstraintViolation<T>> validation) {
        if (!validation.isEmpty()) {
            return validation.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static <T> void validate(@NotNull T t) throws ValidationException {
        Set<ConstraintViolation<T>> constraintViolations = getConstraintViolationSet(t);
        List<String> errorList = getError(constraintViolations);
        List<String> plainMessageList = getErrorMessages(constraintViolations);
        if (!errorList.isEmpty()) {
            throw new ValidationException(errorList, plainMessageList);
        }

    }

    public static <T> void validate(@NotNull T t, Class... validationGroups) throws ValidationException {
        Set<ConstraintViolation<T>> constraintViolations = getConstraintViolationSet(t, validationGroups);
        List<String> errorList = getError(constraintViolations);
        List<String> plainMessageList = getErrorMessages(constraintViolations);
        if (!errorList.isEmpty()) {
            throw new ValidationException(errorList, plainMessageList);
        }
    }
}
