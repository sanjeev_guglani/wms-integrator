package com.delhivery.godam.integrator.contracts.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Sanjeev Guglani on 19/2/18
 * annotation class used to create and initialize currency fileds.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Currency {
    int precision();
}
