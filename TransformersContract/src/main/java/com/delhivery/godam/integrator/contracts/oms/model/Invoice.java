package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Invoice {

    @JsonProperty("invoice_number")
    private String invoiceNumber;

    @JsonProperty("payment_mode")
    private String paymentMode;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("total_price")
    private BigDecimal totalPrice;

    @JsonProperty("cod_amount")
    private BigDecimal codAmount;

    @JsonProperty("unit_price")
    private BigDecimal unitPrice;

    @JsonProperty("unit_taxes")
    private BigDecimal unitTaxes;

    @JsonProperty("total_cst")
    private BigDecimal totalCST;

    @JsonProperty("total_vat")
    private BigDecimal totalVat;

    @JsonProperty("total_taxes")
    private BigDecimal totalTaxes;

    @JsonProperty("shipping_price")
    private BigDecimal shippingPrice;

    @JsonProperty("gross_value")
    private BigDecimal grossValue;

    @JsonProperty("discount")
    private BigDecimal discount;

    @JsonProperty("vat_percentage")
    private BigDecimal vatPercentage;

    @JsonProperty("cst_percentage")
    private BigDecimal cstPercentage;

    @JsonProperty("tax_percentage")
    private BigDecimal taxPercentage;

    @JsonProperty("net_amount")
    private BigDecimal netAmount;

    @JsonProperty("advance_payment")
    private BigDecimal advancePayment;

    @JsonProperty("round_off")
    private BigDecimal roundOff;

    @JsonProperty("mrp")
    private BigDecimal mrp;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("cgst_percentage")
    private BigDecimal cgstPercentage;

    @JsonProperty("cgst_amount")
    private BigDecimal cgstAmount;

    @JsonProperty("sgst_percentage")
    private BigDecimal sgstPercentage;

    @JsonProperty("sgst_amount")
    private BigDecimal sgstAmount;

    @JsonProperty("igst_percentage")
    private BigDecimal igstPercentage;

    @JsonProperty("igst_amount")
    private BigDecimal igstAmount;

    @JsonProperty("tan_number")
    private String tanNumber;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("cst_number")
    private String cstNumber;

    @JsonProperty("service_tax_number")
    private String serviceTaxNumber;

    @JsonProperty("cin_number")
    private String cinNumber;

    @JsonProperty("gstin_number")
    private String gstinNumber;
}
