package com.delhivery.godam.integrator.contracts.utils;

import java.util.Arrays;
import java.util.List;

/**
 * different date formats supported in Utility method
 * separators supported are also listed
 *
 * @author megha on 7/2/18
 */
public enum DateTimeFormats {

    FORMAT_TWO("yyyy-MM-dd['T'][ ]HH:mm:ss.SSSSSS"),
    FORMAT_THREE("yyyy-MM-dd['T'][ ]HH:mm:ss[.SSS][ ][Z]"),
    FORMAT_FOURTH("dd-MM-yyyy['T'][ ]HH:mm:ss.SSSSSS"),
    FORMAT_FIVTH("dd-MM-yyyy['T'][ ]HH:mm:ss[.SSS][ ][Z]"),
    FORMAT_SIXTH("yyyy-M-d['T'][ ]k:m:s"),
    FORMAT_SEVENTH("d-M-yyyy['T'][ ]k:m:s"),
    FORMAT_EIGTH("M-d-yyyy k:m");


    // NOTE : cannot take both d-M-u and u-M-d here as while parsing, system gets confused on
    // whether a date e.g 06/3/17 represents 2006/03/17 or 06/03/2017 so its better
    // to specify year as yyyy

    String dateFormat;

    DateTimeFormats(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * get date format
     * @return date format
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * get different separators possible
     * @return separator
     */
    public static List<String> getSeparators() {
        return Arrays.asList("/");
    }
}
