package com.delhivery.godam.integrator.contracts.inbound.model;

import com.delhivery.godam.integrator.contracts.utils.Utility;
import com.fasterxml.jackson.annotation.*;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by Sanjeev Guglani on 28/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class ASN extends ASNContract{

    /**
     * The Source number.
     */
    @JsonProperty("source_number")
    private String sourceNumber;
    /**
     * The Client uuid.
     */
    @JsonProperty("client_uuid")
    private String clientUuid;
    /**
     * The Fulfillment center uuid.
     */
    @JsonProperty("fulfillment_center_uuid")
    private String fulfillmentCenterUuid;

    @JsonProperty("mode_of_transport")
    private ModeOfTransportation modeOfTransportation;

    /**
     * The Expected arrival date.
     */
    @JsonProperty("expected_arrival_date")
    private ZonedDateTime expectedArrivalDate;

    @NotNull(message = NOT_NULL_MESSAGE)
    @JsonProperty("agn_type")
    private AsnType asnType;

    /**
     * The Products.
     */
    @JsonProperty("products")
    private List<ASNProduct> products = new ArrayList<>();


    /**
     * Gets expected arrival date.
     *
     * @return the expected arrival date
     */
    @JsonIgnore
    public ZonedDateTime getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    /**
     * Sets expected arrival date.
     *
     * @param expectedArrivalDate the expected arrival date
     */
    @JsonIgnore
    public void setExpectedArrivalDate(ZonedDateTime expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    /**
     * Gets expected arrival date string.
     *
     * @return the expected arrival date string
     */
    @JsonGetter("expected_arrival_date")
    public String getExpectedArrivalDateString() {
        return Objects.isNull(expectedArrivalDate) ? null : expectedArrivalDate.toString();

    }

    /**
     * Sets expected arrival date string.
     *
     * @param expectedArrivalDate the expected arrival date
     */
    @JsonSetter("expected_arrival_date")
    public void setExpectedArrivalDateString(String expectedArrivalDate) {
        this.expectedArrivalDate = StringUtils.isBlank(expectedArrivalDate) ? null :
                Utility.getGodamFormatZonedDateTime(expectedArrivalDate);
    }


    /**
     * The enum Mode of transportation.
     */
    public enum ModeOfTransportation {
        /**
         * Fc drop mode of transportation.
         */
        DF,
        /**
         * Pickup mode of transportation.
         */
        PU;

        /**
         * Parse mode of transportation mode of transportation.
         *
         * @param mode the mode
         * @return the mode of transportation
         */
        public static ModeOfTransportation parseModeOfTransportation(String mode) {
            for (ModeOfTransportation modeOfTransportation : ModeOfTransportation.values()) {
                if (modeOfTransportation.name().equalsIgnoreCase(mode))
                    return modeOfTransportation;
            }
            return DF;
        }
    }


    /**
     * The enum Asn type.
     */
    public enum AsnType {
        /**
         * Fwd asn type.
         */
        FWD ("FORWARD"),
        /**
         * Cor asn type.
         */
        COR ("COURIER-RETURN"),
        /**
         * Cur asn type.
         */
        CUR ("CUSTOMER-RETURN"),
        /**
         * To asn type.
         */
        TO("TRANSFER-ORDER"),
        /**
         * Tso asn type.
         */
        TSO("TRANSFER-STOCK-ORDER");

        /**
         * The Alias.
         */
        String alias;

        AsnType(String alias) {
            this.alias = alias;
        }

        /**
         * Gets alias.
         *
         * @return the alias
         */
        public String getAlias() {
            return alias;
        }

        /**
         * Parse asn type asn type.
         *
         * @param name the name
         * @return the asn type
         */
        public static AsnType parseAsnType(String name) {
            for (AsnType asnType : AsnType.values()) {
                if (asnType.getAlias().equalsIgnoreCase(name) || asnType.name().equalsIgnoreCase(name))
                    return asnType;
            }
            return AsnType.FWD;
        }
    }


}


