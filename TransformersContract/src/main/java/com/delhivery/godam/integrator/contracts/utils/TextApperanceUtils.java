package com.delhivery.godam.integrator.contracts.utils;


import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by pradeepverma on 29/7/17.
 */
public class TextApperanceUtils {

    private static final ToStringStyle configPrintStyle = ToStringStyle.MULTI_LINE_STYLE;
    private static final ToStringStyle emailPrintStyle = ToStringStyle.MULTI_LINE_STYLE;
    private static final ToStringStyle jsonPrintSyle = ToStringStyle.JSON_STYLE;

    public static ToStringStyle getConfigPrintStyle() {
        return configPrintStyle;
    }

    public static ToStringStyle getEmailPrintStyle() {
        return emailPrintStyle;
    }

    public static ToStringStyle getJSonPrintStyle() {
        return jsonPrintSyle;
    }
}
