package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * @author Dewbrat Kumar on 10/10/18
 */
public class InternalServiceException extends RuntimeException{

    /**
     * Exception thrown in case of internal service used and
     * some error occurred.
     * @param message message thrown by internal services
     * @param ex Exception
     */
    public InternalServiceException(String message, Exception ex){
        super(message, ex);
    }

}
