package com.delhivery.godam.integrator.contracts.utils;

import org.apache.commons.lang3.StringUtils;

import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Activity.*;
import static org.apache.commons.lang3.StringUtils.capitalize;

/**
 * Created by Sanjeev Guglani.
 * class contains channel's entity's and activity's
 */
public class GodamUtils {

    /**
     * list of master channels
     * contains slug name in arguments
     * channel name's used across project is referencing
     * from this class
     * //TODO remove other refrences of channel's which are not used from this class
     */
    public enum MasterChannel {

        A2Z("gadgetsnow", "gadgetsnow"),
        FLIPKART("flipkart-fbd", "flipkart-fbd"),
        AMAZON_MWS("amazon-india", "amazon-india"),
        AMAZON("amazon", "amazon"),
        LIMEROAD("limeroad", "limeroad"),
        PAYTM("paytm", "paytm"),
        SHOPCLUES("shopclues-basic", "shopclues-basic"),
        SNAPDEAL_ONESHIP("snapdeal-oneship", "snapdeal"),
        SNAPDEAL_DROPSHIP("snapdeal-dropship", "snapdeal"),
        VOONIK_FULLFILLED("voonikvoonik_fulfilled", "voonik"),
        VOONIK_VENDOR_FULLFILLED("voonikvendor_fulfilled", "voonik"),
        TRENT("trent", "trent"),
        XIAOMI("xiaomi", "xiaomi"),
        DSNS("defaultDsns", "defaultDsns"),
        DEFAULT("default", "default"),
        WELSPUN("welspun", "welspun"),
        HILTI("hilti", "hilti"),
        ONEPLUS("oneplus", "oneplus"),
        LEVIS("levis", "levis"),
        FLIPLEARN("fliplearn", "fliplearn"),
        API_INTEGRATION("api_integration", "api_integration"),
        DEFAULT_CSV("default-csv", "default-csv"),
        PAYTM_CHINA("paytmchina", "paytmchina"),
        YMS("appointment","appointment"),
        TI_CYCLE("ticycle","ticycle");

        private String slugName;

        private String notificationSubscriberId;

        MasterChannel(String slugName, String notificationSubscriberId) {
            this.slugName = slugName;
            this.notificationSubscriberId = notificationSubscriberId;
        }

        public String getSlugName() {
            return slugName;
        }

        public String getNotificationSubscriberId() {
            return notificationSubscriberId;
        }

        public static MasterChannel getMasterChannel(String slugName) {

            for (MasterChannel masterChannel : MasterChannel.values()) {

                if (masterChannel.getSlugName().equalsIgnoreCase(slugName)) {
                    return masterChannel;
                }
            }
            throw new RuntimeException("Invalid Slug Name ");
        }
    }

    /**
     * list of entities
     * here entities is mapped with corresponding activities
     */
    public enum Entity {

        ASN(CREATE, UPDATE, RETURN, HOLD, CANCEL, MULTI_COMPONENT),
        ORDER(CREATE, PACK, CANCEL_CHECK, MANIFEST,PACK_PULL),
        ORDERV2(CREATE, PACK, CANCEL_CHECK, MANIFEST),
        INVENTORY(ADJUSTMENT, UPDATE),
        PRODUCT(CREATE,ON_DEMAND),
        NONE,
        NOTIFICATION,
        FILE(DOWNLOAD);

        private Activity[] activities;

        Entity(Activity... activities) {
            this.activities = activities;
        }

        public String getActivity(Activity activity) {

            String[] splitActivity = activity.name().split("_");
            String activityName = StringUtils.EMPTY;

            if(this.name().equalsIgnoreCase(NOTIFICATION.name())){
                return   String.format("%s%s", capitalize(this.name().toLowerCase()), activityName);
            }
            for (String tempActivity : splitActivity) {
                activityName = activityName + capitalize(tempActivity.toLowerCase());
            }
            return String.format("%s%s", capitalize(this.name().toLowerCase()), activityName);
        }

        /**
         * use in case of notification entity
         * @return notification entity as activity
         */
        public String getActivity(){

            return String.format("%s", capitalize(this.name().toLowerCase()));
        }
    }

    /**
     * list of activities
     */
    public enum Activity {

        CREATE,
        CANCEL_CHECK,
        PICK,
        PACK,
        MANIFEST,
        SHIP,
        ADJUSTMENT,
        UPDATE,
        ON_DEMAND,
        DOWNLOAD,
        PACK_PULL,
        NONE,
        RETURN,
        RETURN_CALLBACK,
        ADJUSTMENT_PO,
        LEVEL,
        RECEIPT,
        DELIVER,
        STATE,
        RECALL,
        TRANSFER_ORDER,
        EDIT_COMPONENT,
        HOLD,
        APPOINTMENT,
        CANCEL,
        RETRIEVE,
        DETAIL,
        MULTI_COMPONENT
    }

}
