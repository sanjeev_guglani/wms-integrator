package com.delhivery.godam.integrator.contracts.utils;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.stream.Collectors;

/**
 * class to build date time formatter
 * for various date time formats
 *
 * @author megha on 8/3/18
 */
public class CommonDateTimeFormatter {

    private DateTimeFormatter dateTimeFormatter;

    private static CommonDateTimeFormatter commonDateTimeFormatter;

    static {
        commonDateTimeFormatter = new CommonDateTimeFormatter();
    }

    private CommonDateTimeFormatter() {
        dateTimeFormatter = buildDateTimeFormatter();
    }

    /**
     * get singleton instance
     * @return instance
     */
    public static CommonDateTimeFormatter getCommonDateTimeFormatter() {
        return commonDateTimeFormatter;
    }

    /**
     * get datetimeformatter
     * @return object
     */
    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }

    /**
     * this method provides datetimeformatter
     * appended with multiple datetime patterns
     *
     * @return formatter
     */
    private static DateTimeFormatter buildDateTimeFormatter() {
        DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();

        builder.appendOptional(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        for(DateTimeFormats timeFormat : DateTimeFormats.values()) {
            builder.appendOptional(DateTimeFormatter.ofPattern(timeFormat.getDateFormat()));

            List<String> dateTimeFormatsWithDifferentSeparator = DateTimeFormats.getSeparators()
                    .stream().map(sep -> timeFormat.getDateFormat().replace("-", sep))
                    .collect(Collectors.toList());

            dateTimeFormatsWithDifferentSeparator.stream().map(DateTimeFormatter::ofPattern)
                    .forEach(builder::appendOptional);
        }

        return builder.toFormatter();
    }
}
