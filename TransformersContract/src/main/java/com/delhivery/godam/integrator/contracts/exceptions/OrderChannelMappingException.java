package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * Created by Kumar Vaibhav on 27/9/17.
 * <p>
 * exception to wrap all errors when
 * channel order mapping is missing in the repository
 */
public class OrderChannelMappingException extends Exception {

    private String client;
    private String order;

    public OrderChannelMappingException(String client, String message, String order) {
        super(message);
        this.client = client;
        this.order = order;
    }

    public OrderChannelMappingException(String client, String message, String order, Exception e) {
        super(message, e);
        this.client = client;
        this.order = order;
    }

    @Override
    public String getMessage() {
        return String.format("client [%s], error [%s], order :: %s", client, super.getMessage(), order);
    }
}
