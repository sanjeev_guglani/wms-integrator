package com.delhivery.godam.integrator.contracts;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Sanjeev Guglani on 2/5/19.
 */
@Builder
@Data
public class ServiceBlob {

    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("channel")
    private String channel;

    @JsonProperty("activity_name")
    private Activity activity;

    @JsonProperty("service_name")
    private Entity serviceName;

    @JsonProperty("data")
    private AbstractBlob load;
}
