package com.delhivery.godam.integrator.contracts.inbound;

import com.delhivery.godam.integrator.contracts.TransformerHelper;
import com.delhivery.godam.integrator.contracts.exceptions.ASNDataException;
import com.delhivery.godam.integrator.contracts.inbound.model.ASNContract;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanjeev Guglani on 28/3/19.
 *
 * @param <C> the type parameter represent Client ASN Type
 * @param <G> the type parameter represent Godam ASN Type
 */
public interface ASNCreateTransformer<C,G extends ASNContract> {

    /**
     * The constant logger.
     */
    Logger logger = LoggerFactory.getLogger(ASNCreateTransformer.class);

    /**
     * method to convert raw payload into list of client Asn.
     *
     * @param asnRawData raw String having products information, can be JSON, XML etc.
     * @return list of client asn objects
     * @throws ASNDataException invalid product data format received from client
     */
    List<C> getClientASNs(String asnRawData) throws ASNDataException;

    /**
     * method to validate client's asn model
     *
     * @param asn client product
     * @throws ASNDataException if product data validation fails
     */
    void validate(C asn) throws ASNDataException, ASNDataException;



    default void postValidate(G asn) throws Validator.ValidationException {
        Validator.validate(asn);
    }


    /**
     * Transform list.
     *
     * @param asnList the asn list
     * @return the list
     * @throws ASNDataException the asn data exception
     */
    default List<TransformerHelper<G>> transform(List<C> asnList) throws ASNDataException {
        List<TransformerHelper<G>> transformedASNHelpers = new ArrayList<>();
        for (C asn : asnList) {
            validate(asn);
            List<G> asns = toGodamASN(asn);
            asns.forEach(tASN -> transformedASNHelpers.add(new TransformerHelper(tASN)));
        }

        return transformedASNHelpers;
    }


    /**
     * method transform client's asn model to godam asn model
     *
     * @param asn client asn model
     * @return list of godam asn
     */
    List<G> toGodamASN(C asn);


}
