package com.delhivery.godam.integrator.contracts.exceptions;

import java.util.Objects;

/**
 * @author Dewbrat Kumar on 19/1/18
 * this exception will be thrown in case fc detail will no be found
 */
public class FCDetailDataException extends Exception{

    private String client;

    private Object data;

    public FCDetailDataException(String client, String message, Object data){
        super(message);
        this.client = client;
        this.data = data;
    }

    public FCDetailDataException(String client, String message, Objects data, Exception e){
        super(message, e);
        this.client = client;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return String.format("client [%s], error [%s], data :: %s", client, super.getMessage(), data);
    }
}
