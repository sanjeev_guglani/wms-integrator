package com.delhivery.godam.integrator.contracts.exceptions;


import com.delhivery.godam.integrator.contracts.exceptions.database.DBException;

/**
 * Created by pradeepverma on 29/11/16.
 */
public class ProductSellerMappingNotFoundException extends DBException {

    private String productId;
    private String sellerId;

    public ProductSellerMappingNotFoundException(String message, String productId, String dbName, String operation) {
        super(message, dbName, operation);
        this.productId = productId;
    }

    public ProductSellerMappingNotFoundException(String message, String productId, String sellerId, String dbName, String operation) {
        super(message, dbName, operation);
        this.productId = productId;
        this.sellerId = sellerId;
    }

    public ProductSellerMappingNotFoundException(String message, String productId, String sellerId, String dbName, String operation, Throwable cause) {
        super(message, dbName, operation, cause);
        this.productId = productId;
        this.sellerId = sellerId;
    }

    public String getMessage() {
        return String.format("Product seller mapping not found, dbName [%s], operation [%s], prod id [%s], seller id [%s], error [%s] ", dbName, operation, productId, sellerId,
                super.getMessage());
    }
}
