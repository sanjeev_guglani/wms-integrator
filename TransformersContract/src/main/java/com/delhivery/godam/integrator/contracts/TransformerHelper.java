package com.delhivery.godam.integrator.contracts;

import com.delhivery.godam.integrator.contracts.utils.Mapper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by marutsingh on 4/11/16.
 * type T is model in which transformation has to be done.
 *
 * @param <T> the type parameter
 */
public class TransformerHelper<T> {
    /*
    * Object to store transformedObject object and the error, if any, associated with the transformedObject
     */
    private T transformedObject;
    private List<String> error;
    private Object data;


    /**
     * Instantiates a new Transformer helper.
     *
     * @param transformedObject the transformed object
     */
    public TransformerHelper(T transformedObject) {
        this.transformedObject = transformedObject;
    }

    /**
     * Instantiates a new Transformer helper.
     *
     * @param error the error
     * @param data  the data
     */
    public TransformerHelper(List<String> error, Object data) {
        this.error = error;
        this.data = data;
    }

    /**
     * Gets transformed object.
     *
     * @return the transformed object
     */
    public T getTransformedObject() {
        return transformedObject;
    }

    /**
     * Sets transformed object.
     *
     * @param transformedObject the transformed object
     */
    public void setTransformedObject(T transformedObject) {
        this.transformedObject = transformedObject;
    }

    /**
     * Is valid boolean.
     *
     * @return the boolean
     */
    public boolean isValid() {
        return getError() == null || getError().isEmpty();
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public List<String> getError() {
        return error;
    }

    /**
     * Sets error.
     *
     * @param error the error
     */
    public void setError(List<String> error) {
        this.error = error;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Gets invalid objects.
     *
     * @param <R>                the type parameter
     * @param transformerHelpers the transformer helpers
     * @return the invalid objects
     */
    public static <R> List<TransformerHelper<?>> getInvalidObjects(List<TransformerHelper<R>> transformerHelpers) {

        return transformerHelpers.stream()
                .filter(helper -> !helper.isValid()).collect(Collectors.toList());

    }

    /**
     * Gets valid objects.
     *
     * @param <R>                the type parameter
     * @param transformerHelpers the transformer helpers
     * @return the valid objects
     */
    public static <R> List<TransformerHelper> getValidObjects(List<TransformerHelper<R>> transformerHelpers) {

        return transformerHelpers.stream()
                .filter(TransformerHelper::isValid).collect(Collectors.toList());

    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }

}
