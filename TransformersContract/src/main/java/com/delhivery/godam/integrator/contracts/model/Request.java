package com.delhivery.godam.integrator.contracts.model;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpMethod;

import java.util.Map;

/**
 * Created by delhivery on 22/6/16.
 *
 * @param <X> the type parameter
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request<X> {

    @JsonProperty("headers")
    private Map headers;

    @JsonProperty("queryParams")
    private Map params;

    @JsonProperty("path")
    private Map path;

    @JsonProperty("body")
    private X payload;

    @JsonProperty("method")
    private HttpMethod method;

    @JsonProperty("end_point")
    private String endPoint;

    /**
     * Gets headers.
     *
     * @return the headers
     */
    public Map getHeaders() {
        return headers;
    }

    /**
     * Sets headers.
     *
     * @param headers the headers
     */
    public void setHeaders(Map headers) {
        this.headers = headers;
    }

    /**
     * Gets params.
     *
     * @return the params
     */
    public Map getParams() {
        return params;
    }

    /**
     * Sets params.
     *
     * @param params the params
     */
    public void setParams(Map params) {
        this.params = params;
    }

    /**
     * Gets path.
     *
     * @return the path
     */
    public Map getPath() {
        return path;
    }

    /**
     * Sets path.
     *
     * @param path the path
     */
    public void setPath(Map path) {
        this.path = path;
    }

    /**
     * Gets payload.
     *
     * @return the payload
     */
    public X getPayload() {
        return payload;
    }

    /**
     * Sets payload.
     *
     * @param payload the payload
     */
    public void setPayload(X payload) {
        this.payload = payload;
    }

    /**
     * Gets method.
     *
     * @return the method
     */
    public HttpMethod getMethod() {
        return method;
    }

    /**
     * Sets method.
     *
     * @param method the method
     */
    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    /**
     * Gets end point.
     *
     * @return the end point
     */
    public String getEndPoint() {
        return endPoint;
    }

    /**
     * Sets end point.
     *
     * @param endPoint the end point
     */
    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    /**
     * Get query string string.
     *
     * @return the string
     */
    @JsonIgnore
    public String getQueryString(){
        StringBuilder queryStringBuilder = new StringBuilder();

        if (getParams() != null && !getParams().isEmpty()) {
            getParams().keySet().forEach(key -> queryStringBuilder.append(String.format("%s=%s&", key, getParams().get(key))));
            queryStringBuilder.deleteCharAt(queryStringBuilder.length() - 1);
        }
        return queryStringBuilder.toString();
    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }
}

