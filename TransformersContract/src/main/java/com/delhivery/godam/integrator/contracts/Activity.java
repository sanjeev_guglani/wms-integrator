package com.delhivery.godam.integrator.contracts;

/**
 * The enum Activity.
 */
public enum Activity {

    /**
     * Create activity.
     */
    CREATE,

    /**
     * Order create activity.
     */
    ORDER_CREATE,

    /**
     * Update activity.
     */
    UPDATE,

    /**
     * Order update activity.
     */
    ORDER_UPDATE,

    /**
     * Order allocated activity.
     */
    ORDER_ALLOCATED,

    /**
     * Order picked activity.
     */
    ORDER_PICKED,

    /**
     * Pickwave create activity.
     */
    PICKWAVE_CREATE,

    /**
     * Allocate activity.
     */
    ALLOCATE,

    /**
     * Pick activity.
     */
    PICK,

    /**
     * Pick tbp activity.
     */
    PICK_TBP,

    /**
     * Pick complete activity.
     */
    PICK_COMPLETE,

    /**
     * Pack complete activity.
     */
    PACK_COMPLETE,

    /**
     * Pack waybill activity.
     */
    PACK_WAYBILL,

    /**
     * Rts activity.
     */
    RTS,

    /**
     * Rts complete activity.
     */
    RTS_COMPLETE,

    /**
     * Dispatch activity.
     */
    DISPATCH,

    /**
     * Dispatch complete activity.
     */
    DISPATCH_COMPLETE,

    /**
     * Receiving activity.
     */
    RECEIVING,

    /**
     * Fim activity.
     */
    FIM,

    /**
     * Gim activity.
     */
    GIM,

    /**
     * Agn activity.
     */
    AGN,

    /**
     * Oms activity.
     */
    OMS,

    /**
     * Cancel activity.
     */
    CANCEL

}
