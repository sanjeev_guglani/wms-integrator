package com.delhivery.godam.integrator.contracts.exceptions;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

/**
 * The type Warehouse mapping not found exception.
 */
public class WarehouseMappingNotFoundException extends RuntimeException {

    private String channel;
    private String warehouse;

    /**
     * Instantiates a new Warehouse mapping not found exception.
     *
     * @param channel   the channel
     * @param warehouse the warehouse
     */
    public WarehouseMappingNotFoundException(String channel, String warehouse) {
        super("warehouse mapping not defined.");
        this.channel = channel;
        this.warehouse = warehouse;
    }

    public WarehouseMappingNotFoundException(GodamUtils.MasterChannel channel, String message) {
        super(message);
        this.channel = channel.getSlugName();
    }
/*
    public WarehouseMappingNotFoundException(String channel, String warehouse, String message) {
        super(String.format("warehouse mapping not defined for %s: %s",));
        this.channel = channel;
        this.warehouse = warehouse;
    }*/

    /**
     * Instantiates a new Warehouse mapping not found exception.
     *
     * @param channel   the channel
     * @param warehouse the warehouse
     * @param ex        the ex
     */
    public WarehouseMappingNotFoundException(String channel, String warehouse, Exception ex) {
        super("warehouse mapping not defined.", ex);
        this.channel = channel;
        this.warehouse = warehouse;
    }

    @Override
    public String getMessage() {
        return String.format("%s channel :: [%s] warehouse :: [%s]", super.getMessage(), channel, warehouse);
    }

    public String getChannel() {
        return channel;
    }

    public String getWarehouse() {
        return warehouse;
    }
}
