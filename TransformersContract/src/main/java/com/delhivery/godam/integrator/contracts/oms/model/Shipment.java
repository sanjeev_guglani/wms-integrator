package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Shipment {

    @JsonProperty("number")
    private String number;

    @JsonProperty("workflow")
    @NotNull(message = NOT_NULL_MESSAGE)
    private Integer workflow;

    @JsonProperty("fc")
    @NotNull(message = NOT_NULL_MESSAGE)
    private String fc;

    @JsonProperty("status")
    private ShipmentStatus shipmentStatus;

    @JsonProperty("parent_courier")
    private String parentCourier;

    @JsonProperty("child_courier")
    private String childCourier;

    @JsonProperty("waybill")
    private String waybill;

    @JsonProperty("ewaybill")
    private String ewayBill;

    @JsonProperty("ewaybill_expiry_date")
    private long eWaybillExpiryDate;

    @JsonProperty("pick_wave_reference")
    private String pickWaveReference;

    @JsonProperty("invoice")
    @Valid
    private Invoice invoice;

    @JsonProperty("order_lines")
    private List<OrderLine> orderLines;

    public List<OrderLine> getOrderLines() {
        if(Objects.isNull(orderLines)){
            orderLines = new ArrayList<>();
        }
        return orderLines;
    }

    /**
     * The enum Payment mode v 2.
     */
    public enum PaymentMode {
        /**
         * Prepaid payment mode v 2.
         */
        PREPAID,
        /**
         * Cod payment mode v 2.
         */
        COD;

        /**
         * Gets from string.
         *
         * @param value the value
         * @return the from string
         */
        public static PaymentMode getFromString(String value) {
            for (PaymentMode paymentMode : PaymentMode.values()) {
                if (paymentMode.name().equalsIgnoreCase(value))
                    return paymentMode;
            }
            return COD;
        }
    }

}
