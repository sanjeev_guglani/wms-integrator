package com.delhivery.godam.integrator.contracts.oms.model;

import com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;
import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.VALUE_GREATER_THAN_0;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderLine {

    @JsonProperty("number")
    private String number;


    @JsonProperty("product_sku")
    @NotBlank(message = ValidationMessages.NOT_NULL_MESSAGE)
    private String productSku;


    @JsonIgnore
    private String delSku;

    @JsonProperty("quantity")
    @NotNull(message = NOT_NULL_MESSAGE)
    @Min(value = 1, message = VALUE_GREATER_THAN_0)
    private Integer quantity;

    @JsonIgnore
    private Integer uomCalculatedQuantity;

    @JsonProperty("bucket")
    private String bucket;

    @JsonIgnore
    private Integer rejectedQuatity;

    @JsonProperty("client_id")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String clientId;

    @JsonProperty("invoice")
    private Invoice invoice;

    @JsonProperty("waybills")
    private Set<Waybill> waybills;

    @JsonProperty("extras")
    private Map<String, String> extras;
}
