package com.delhivery.godam.integrator.contracts.crts;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author megha on 10/10/17
 */
@Data
public class TrackerBulkApiRequest {

    @JsonProperty("log_request_list")
    private List<TrackingInfo> trackingInfoList;

    public TrackerBulkApiRequest(List<TrackingInfo> trackingInfos) {
        this.trackingInfoList = trackingInfos;
    }

    @Override
    public String toString() {
         return Mapper.toString(this);
    }

}
