package com.delhivery.godam.integrator.contracts.inbound.model;

import com.delhivery.godam.integrator.contracts.inbound.asn.AsnUpdateValidationGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import java.util.Map;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.CHAR_LIMIT_MESSAGE;
import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by Sanjeev Guglani on 28/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ASNProduct {

    @NotBlank(message = NOT_NULL_MESSAGE, groups = {Default.class, AsnUpdateValidationGroup.class})
    @Size(max = 255, message = CHAR_LIMIT_MESSAGE, groups = {Default.class, AsnUpdateValidationGroup.class})
    @JsonProperty("name")
    private String name;

    @NotNull(message = NOT_NULL_MESSAGE)
    @JsonProperty("quantity")
    private Integer quantity;

    @JsonProperty("sku")
    private String sku;

    @JsonProperty("extra_details")
    private Map<String, Object> extra;

}
