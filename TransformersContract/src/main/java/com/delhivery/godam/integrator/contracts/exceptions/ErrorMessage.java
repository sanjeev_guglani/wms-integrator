package com.delhivery.godam.integrator.contracts.exceptions;


/**
 * The enum Error message.
 * created by sachin bhogal
 */
public enum ErrorMessage {

    /**
     * The Invalid data.
     */
    INVALID_DATA("101", "Invalid Data Received") {
        @Override
        public String getMessage(String client, String activity) {
            return String.format("%s %s error: %s ", client, activity, this.getErrorMessage());
        }
    },
    /**
     * The Validate failed.
     */
    VALIDATE_FAILED("102", "Some of the Validations Failed") {
        @Override
        public String getMessage(String client, String activity) {
            return String.format("%s %s error: %s ", client, activity, this.getErrorMessage());
        }
    },
    /**
     * The Multiple entity not supported.
     */
    MULTIPLE_ENTITY_NOT_SUPPORTED("103", "Multiple Entities not Supported") {
        @Override
        public String getMessage(String client, String activity) {
            return String.format("%s %s error: %s", client, activity, this.getErrorMessage());
        }
    },

    /**
     * The Parsing fail.
     */
    PARSING_FAIL("104", "Unable to Parse data") {
        @Override
        public String getMessage(String client, String activity) {
            return String.format("%s %s error: %s ", client, activity, this.getErrorMessage());
        }
    };


    private String errorCode;
    private String errorMessage;

    ErrorMessage(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * Gets message.
     *
     * @param client   the client
     * @param activity the activity
     * @return the message
     */
    public abstract String getMessage(String client, String activity);

    /**
     * Gets error code.
     *
     * @return the error code
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Gets error message.
     *
     * @return the error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }
}
