package com.delhivery.godam.integrator.contracts.exceptions;

/*
    created by Sanjeev Guglani
    class is used to wrap godam entity related exceptions
    like order data , asn data , product data exception

 */
public class DataException extends Exception {

    protected String activity;

    protected String client;

    protected ErrorMessage errorMessage;

    protected String referenceNo;

    public DataException(String message) {
        super(message);
    }

    public DataException(String message, Exception e) {
        super(message, e);
    }


    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public String getReferenceNo() {
        return referenceNo;
    }
}