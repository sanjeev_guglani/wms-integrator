package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * exception class to wrap all external order format errors.
 * external orders come through REST or pulled from market places etc.
 * Created by pradeepverma on 01/05/17.
 */
public class OrderDataException extends DataException {

    private String rawData;

    public OrderDataException(String message) {
        super(message);
    }

    public OrderDataException(String client, String message) {
        super(message);
        this.client = client;
    }

    public OrderDataException(String client, String message, String data) {
        super(message);
        this.client = client;
        this.rawData = data;
    }

    public OrderDataException(String client, String message, String data, Exception e) {
        super(message, e);
        this.client = client;
        this.rawData = data;
    }

    public OrderDataException(String client, ErrorMessage errorMessage, String activity, String data) {
        super(errorMessage.getMessage(client, activity));
        this.client = client;
        this.activity = activity;
        this.errorMessage = errorMessage;
        this.rawData = data;
    }

    @Override
    public String getMessage() {
        return String.format("client [%s], error [%s], data :: %s",
                client, super.getMessage(),rawData);
    }

    /**
     * method returns raw data object
     *
     * @return data
     */
    public Object getRawData() {
        return this.rawData;
    }

}
