package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Sanjeev Guglani on 2/5/19.
 */
@Data
public class OrderCreateParameters {
    @JsonProperty("orders")
    private List<? extends OrderContract> orders;
}