package com.delhivery.godam.integrator.contracts.godam;

/**
 * Godam's request body interface
 *
 * @param <T> the type parameter
 */
public interface GodamRequestBody<T> {

    /**
     * Gets channel.
     *
     * @return the channel
     */
    String getChannel();

    /**
     * Sets channel.
     *
     * @param channel the channel
     */
    void setChannel(String channel);

    /**
     * Gets authentication.
     *
     * @return the authentication
     */
    Authentication getAuthentication();

    /**
     * Sets authentication.
     *
     * @param authentication the authentication
     */
    void setAuthentication(Authentication authentication);

    /**
     * Gets payload.
     *
     * @return the payload
     */
    T getPayload();

    /**
     * Sets payload.
     *
     * @param payload the payload
     */
    void setPayload(T payload);
}
