package com.delhivery.godam.integrator.contracts.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Sanjeev Guglani on 24/3/19.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GodamNotificationTransformer {
    String channelName();
    String type();
}
