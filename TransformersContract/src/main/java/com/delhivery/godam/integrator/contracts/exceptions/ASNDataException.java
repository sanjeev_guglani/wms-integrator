package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * exception class to wrap all external ASN format errors.
 * external ASN come through REST or pulled from market places etc.
 * Created by Dewbrat Kumar on 2/5/17.
 */
public class ASNDataException extends DataException {

    private String data;

    public ASNDataException(String message) {
        super(message);
    }

    public ASNDataException(String client, String message, String data) {
        super(message);
        this.client = client;
        this.data = data;
    }

    public ASNDataException(String client, String message, String data, Exception e) {
        super(message, e);
        this.client = client;
        this.data = data;
    }

    public ASNDataException(String client, ErrorMessage errorMessage, String activity, String data) {
        super(errorMessage.getMessage(client, activity));
        this.client = client;
        this.activity = activity;
        this.errorMessage = errorMessage;
        this.data = data;
    }


    @Override
    public String getMessage() {
        return String.format("client [%s], error [%s], data :: %s", client, super.getMessage(), data);
    }

    public String getRawData() {
        return this.data;
    }

}
