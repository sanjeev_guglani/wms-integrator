package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order extends OrderContract{

    @JsonProperty("order_number")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String orderNumber;

    @JsonProperty("order_date")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String orderDate;

    @JsonProperty("order_type")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String orderType;

    @JsonProperty("channel")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String channel;

    @JsonProperty("shipments")
    @NotEmpty(message = NOT_NULL_MESSAGE)
    private List<Shipment> shipments;

    @JsonProperty("consignee")
    @NotNull(message = NOT_NULL_MESSAGE)
    @Valid
    private Consignee consignee;

    @JsonProperty("user")
    @NotBlank(message = NOT_NULL_MESSAGE)
    private String user;

    @JsonProperty("extras")
    private Map<String, String> extras;

    /**
     * Gets shipments.
     *
     * @return the shipments
     */
    public List<Shipment> getShipments() {
        if (Objects.isNull(shipments))
            shipments = new ArrayList<>();
        return shipments;
    }

    /**
     * Gets extras.
     *
     * @return the extras
     */
    public Map<String, String> getExtras() {
        if (Objects.isNull(extras))
            extras = new HashMap<>();
        return extras;
    }
}

