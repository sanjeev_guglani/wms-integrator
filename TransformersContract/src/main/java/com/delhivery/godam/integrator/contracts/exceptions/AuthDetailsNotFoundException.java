package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * The type Auth details not found exception.
 */
public class AuthDetailsNotFoundException extends RuntimeException {

    private String channel;

    /**
     * Instantiates a new Auth details not found exception.
     *
     * @param channel the channel
     */
    public AuthDetailsNotFoundException(String channel){
        super("auth details not found in integrator database.");
        this.channel = channel;
    }

    /**
     * Instantiates a new Auth details not found exception.
     *
     * @param channel the channel
     */
    public AuthDetailsNotFoundException(String channel, String message){
        super(message);
        this.channel = channel;
    }

    /**
     * Instantiates a new Auth details not found exception.
     *
     * @param channel the channel
     * @param ex      the ex
     */
    public AuthDetailsNotFoundException(String channel, Exception ex) {
        super("auth details not found in integrator database.", ex);
        this.channel = channel;
    }

    @Override
    public String getMessage() {
        return String.format("%s channel :: [%s]",super.getMessage(), channel);
    }
}