package com.delhivery.godam.integrator.contracts.oms.model;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
public enum ShipmentStatus {

    VERIFIED,
    ALLOCATED,
    CANCELED;

}
