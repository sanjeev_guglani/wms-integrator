package com.delhivery.godam.integrator.contracts.exceptions;

/**
 * The type Empty message queue exception.
 */
public class EmptyMessageQueueException extends RuntimeException {

    /**
     * Instantiates a new Empty message queue exception.
     *
     * @param message the message
     */
    public EmptyMessageQueueException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Empty message queue exception.
     *
     * @param message the message
     * @param e       the e
     */
    public EmptyMessageQueueException(String message, Exception e) {
        super(message, e);
    }

}