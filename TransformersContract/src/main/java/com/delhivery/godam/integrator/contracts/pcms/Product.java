package com.delhivery.godam.integrator.contracts.pcms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 * Model For Product Creation in WMS-2.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {

    @JsonProperty("sku")
    private String sku;

    @JsonProperty("name")
    private String name;

    @JsonProperty("scannable_id")
    private String scannableId;

    @JsonProperty("description")
    private String description;

    @JsonProperty("category")
    private String category;

    @JsonProperty("mrp")
    private Integer mrp;

    @JsonProperty("hsn_code")
    private String hsnCode;

    @JsonProperty("length")
    private Integer length;

    @JsonProperty("width")
    private Integer width;

    @JsonProperty("height")
    private Integer height;

    @JsonProperty("weight")
    private Integer weight;

    @JsonProperty("url")
    private String url;

    @JsonProperty("uom")
    private String uom;

    @JsonProperty("uom_factor")
    private Integer uomFactor;

    @JsonProperty("billing_category")
    private Integer billingCategory;
}