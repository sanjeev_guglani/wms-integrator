package com.delhivery.godam.integrator.contracts.exceptions.database;

/**
 * Class to log exceptions in DB CRUD operations.
 * Created by pradeepverma on 11/1/2016
 */
public class DBException extends Exception {

    protected String dbName;
    protected String operation;

    /**
     * method to log exception message and database name, db operation.
     *
     * @param message error message
     * @param dbName database name
     */
    public DBException(String message, String dbName, String operation) {
        super(message);
        this.dbName = dbName;
        this.operation = operation;
    }

    /**
     * method to log exception message and database name, db operation, throwable cause
     *
     * @param message error message
     * @param dbName database name
     * @param cause error cause
     */
    public DBException(String message, String dbName, String operation, Throwable cause) {
        super(message, cause);
        this.dbName = dbName;
        this.operation = operation;
    }
}
