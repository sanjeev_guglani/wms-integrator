package com.delhivery.godam.integrator.contracts.utils;

import com.delhivery.godam.integrator.contracts.utils.CustomSerializerProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.math.BigDecimal;

/**
 * common serializer store for entities
 * that need to use an object mapper with
 * big decimal serializing capability used with
 * '@Currency' annotation
 *
 * @author sanjeev guglani on 10/4/18
 */
public class BigDecimalSerializerStore {

    private ObjectMapper objectMapper;

    private static BigDecimalSerializerStore bigDecimalSerializerStore;

    static {
        bigDecimalSerializerStore = new BigDecimalSerializerStore();
    }

    private BigDecimalSerializerStore() {
        objectMapper = initializeSerializer();
    }

    /**
     * initialize object mapper with big decimal serializer module
     * @return object mapper
     */
    private ObjectMapper initializeSerializer() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new BigDecimalJsonSerializer());
        objectMapper.registerModule(module);
        objectMapper.setSerializerProvider(new CustomSerializerProvider());
        return objectMapper;
    }

    /**
     * Gets big decimal serializer store.
     *
     * @return the big decimal serializer store
     */
    public static BigDecimalSerializerStore getBigDecimalSerializerStore() {
        return bigDecimalSerializerStore;
    }


    /**
     * Gets object mapper.
     *
     * @return the object mapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
