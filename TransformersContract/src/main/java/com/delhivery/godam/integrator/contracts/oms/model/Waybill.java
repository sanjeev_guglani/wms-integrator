package com.delhivery.godam.integrator.contracts.oms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Sanjeev Guglani on 27/3/19.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Waybill {

    @JsonProperty("waybill_number")
    private String waybillNumber;

    @JsonProperty("master_waybill_number")
    private String masterWaybillNumber;

    @JsonProperty("fulfillment_center_uuid")
    private String fulfillmentCenterUUID;

    @JsonProperty("courier_name")
    private String courierName;

    @JsonProperty("courier_status")
    private String courierStatus;

    @JsonProperty("courier_remarks")
    private String courierRemarks;

    @JsonProperty("tracking_url")
    private String trackingURL;

    @JsonProperty("last_scan_location")
    private String lastScanLocation;

    @JsonProperty("packing_slip_link")
    private String packingSlipLink;

    @JsonProperty("payment_mode")
    private String paymentCode;

    @JsonProperty("status")
    private String status;

    @JsonProperty("dispatch_number")
    private String dispatchNumber;

    @JsonProperty("manifest_link")
    private String manifestLink;

    @JsonProperty("height")
    private Float height;

    @JsonProperty("width")
    private Float width;

    @JsonProperty("length")
    private Float length;

    @JsonProperty("dimension_unit")
    private String dimensionUnit;

    @JsonProperty("weight")
    private Float weight;

    @JsonProperty("weight_unit")
    private String weightUnit;

    @JsonProperty("volumetric_wight")
    private Float volumetricWeight;

    @JsonProperty("volumetric_unit")
    private String volumetricUnit;

    @JsonProperty("billable_weight")
    private Float billableWeight;

    @JsonProperty("pack_box_id")
    private String packBoxId;

    @JsonProperty("box_shipment_id")
    private String boxShipmentId;

    @JsonProperty("master_box_shipment_id")
    private String masterBoxShipmentId;

    @JsonProperty("quantity")
    private Integer quantity;

    @JsonProperty("ship_by_date")
    private String shipByDate;

    @JsonProperty("updated_by")
    private String updatedBy;

}
