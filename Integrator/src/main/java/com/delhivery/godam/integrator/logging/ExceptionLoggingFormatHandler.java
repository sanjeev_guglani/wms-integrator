package com.delhivery.godam.integrator.logging;

import com.delhivery.godam.db.utils.CustomExceptionHandlers;
import com.delhivery.godam.integrator.logging.utils.InternalExceptionHandlers;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * created by sachin bhogal
 */
@Service
public class ExceptionLoggingFormatHandler {

    @Autowired
    private InternalExceptionHandlers internalExceptionHandlers;

    /**
     * map contains method reference for specific exception implementation
     */
    private Map<String, Function<Exception, Map<String, Object>>> handlers = new HashMap<>();

    @PostConstruct
    public void init() {
        initCustomExceptionHandlers();
        initInternalExceptionHandlers();
    }

    private void initInternalExceptionHandlers() {

        handlers.put(HttpOperationFailedException.class.getSimpleName(),
                (ex -> internalExceptionHandlers.getErrorLog((HttpOperationFailedException) ex)));

        handlers.put(Exception.class.getSimpleName(),
                (ex -> internalExceptionHandlers.getErrorLog(ex)));

    }

    private void initCustomExceptionHandlers() {

        for (CustomExceptionHandlers exception : CustomExceptionHandlers.values()) {
            handlers.put(exception.getExceptionName(), exception.getErrorLog());
        }
    }

    public Map<String, Object> getFormattedErrorMap(Exception ex) {
        return getExceptionHandlerFunction(ex).apply(ex);
    }


    private Function<Exception, Map<String, Object>> getExceptionHandlerFunction(Exception ex) {
        return handlers.getOrDefault(ex.getClass().getSimpleName(),handlers.get(Exception.class.getSimpleName()));
    }
}
