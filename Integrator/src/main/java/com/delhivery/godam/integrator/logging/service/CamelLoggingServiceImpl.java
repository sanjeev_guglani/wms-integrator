package com.delhivery.godam.integrator.logging.service;

import com.delhivery.godam.integrator.constants.RequestStatus;
import com.delhivery.godam.integrator.logging.ExceptionLoggingFormatHandler;
import com.delhivery.godam.integrator.logging.LoggingFormat;
import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

/**
 * implementation class for logging service
 */
@Component
public class CamelLoggingServiceImpl implements CamelLoggingService {

    @Autowired
    private ExceptionLoggingFormatHandler loggingFormatHandler;

    @Override
    public LoggingFormat createLogFromExchange(Exchange exchange) {

        String channel = (String) exchange.getProperties().getOrDefault(CHANNEL, StringUtils.EMPTY);
        String requestId = (String) exchange.getProperties().getOrDefault(REQUEST_ID, StringUtils.EMPTY);
        String logger = exchange.getFromRouteId();
        String activity = exchange.getProperty(ACTIVITY, String.class);
        String remarks = exchange.getProperty(LOGGING_MESSAGE, String.class);
        String entity = exchange.getProperty(ENTITY, String.class);
        Map<String, Object> headers = exchange.getIn().getHeaders();
        RequestStatus status = exchange.getProperty(STATUS, RequestStatus.class);
        Object data = exchange.getIn().getBody();

        if (data instanceof byte[]){
            data = exchange.getIn().getBody(String.class);
        }

        return new LoggingFormat.LogBuilder(requestId, channel, logger)
                .activity(activity)
                .entity(entity)
                .remarks(remarks)
                .status(RequestStatus.getFormattedString(status))
                .data(data)
                .meta(headers)
                .error(getErrorMap(exchange))
                .build();

    }


    private Map<String,Object> getErrorMap(Exchange exchange) {
        Map<String, Object> errors = null;
        if (exchange.getProperties().containsKey(Exchange.EXCEPTION_CAUGHT)) {

            String failureRouteId = exchange.getProperty(Exchange.FAILURE_ROUTE_ID, String.class);
            String failureEndpoint = exchange.getProperty(Exchange.FAILURE_ENDPOINT, String.class);

            errors = loggingFormatHandler.getFormattedErrorMap((Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT));

            errors.put(Exchange.FAILURE_ROUTE_ID, failureRouteId);
            errors.put(Exchange.FAILURE_ENDPOINT, failureEndpoint);

        }

        return errors;
    }
}
