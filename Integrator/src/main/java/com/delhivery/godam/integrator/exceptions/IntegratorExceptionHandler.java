package com.delhivery.godam.integrator.exceptions;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spi.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.LOGGING_LEVEL;


/**
 * created by sachin bhogal
 * <p>
 * exception handler to handle unhandled exceptions from camel routes
 */
@Component
public class IntegratorExceptionHandler implements ExceptionHandler {

    @Autowired
    private CamelContext camelContext;

    private ProducerTemplate producerTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegratorExceptionHandler.class);

    @PostConstruct
    public void init() {

        producerTemplate = camelContext.createProducerTemplate();
    }

    @Override
    public void handleException(Throwable throwable) {

        LOGGER.warn("Unhandled Error Occurred: ", throwable);
    }

    @Override
    public void handleException(String s, Throwable throwable) {

        LOGGER.warn(String.format("Unhandled Error Occurred: %s-%s", s, throwable));
    }

    @Override
    public void handleException(String s, Exchange exchange, Throwable throwable) {

        String fromRouteId = exchange.getFromRouteId();

        LoggingLevel loggingLevel =
                Optional.ofNullable((LoggingLevel) exchange.getProperty(LOGGING_LEVEL)).orElseGet(this::getDefaultLoggingLevel);

        producerTemplate
                .send(String.format("log:%s?level=%s", fromRouteId, loggingLevel), exchange);
    }

    private LoggingLevel getDefaultLoggingLevel() {

        return LoggingLevel.WARN;
    }

}

