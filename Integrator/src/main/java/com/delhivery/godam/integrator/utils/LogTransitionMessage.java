package com.delhivery.godam.integrator.utils;

/**
 * log transition messages enum
 *
 * @author megha on 20/3/18
 */
public enum LogTransitionMessage {

    REQUEST_FROM_CLIENT("request received from client"),
    REQUEST_TO_CLIENT("request sent to client"),
    RESPONSE_FROM_CLIENT("response received from client"),
    REQUEST_ADAPTOR("request received from adaptor"),
    REQUEST_FETCHER("request received from fetcher"),
    REQUEST_VALIDATED("request validated"),
    REQUEST_REQ_TRACKER("request sent to request tracker"),
    HTTP_OP_FAILED("http operation failed"),
    INVALID_REQUEST("invalid request"),
    REQUEST_FROM_GODAM("request received from godam"),
    REQUEST_TO_GODAM("request sent to godam"),
    RESPONSE_GODAM("response received from godam"),
    RESPONSE_CLIENT("response recieved from client"),
    REQUEST_TO_YMS("request sent to yms"),
    RESPONSE_FROM_YMS("response received from yms"),
    REQUEST_TO_FETCHER("request sent to fetcher queue"),
    REQUEST_TO_DSNS("request sent to dsns"),
    RESPONSE_FROM_DSNS("response received from dsns"),
    REQUEST_TO_MLTS_SERVICE("request sent to mlts service"),
    RESPONSE_FROM_MLTS_SERVICE("response from mlts service");;

    String message;

    LogTransitionMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
