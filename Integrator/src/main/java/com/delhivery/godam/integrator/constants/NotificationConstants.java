package com.delhivery.godam.integrator.constants;

/**
 * This class contains constants used in the Notifications package
 * Created by Ankit Kumar on 22/3/17.
 */
public class NotificationConstants {

    //DSNS Constants
    public static final String FINAL_MESSAGE = "FINAL_MESSAGE";


    //DSNS Notification Route Constants
    public static final String DSNS_NOTIFICATION_ROUTE = "direct:defaultDsnsNotificationRoute";
    public static final String DSNS_NOTIFICATION_ROUTE_ID = "DsnsNotificationRoute";
    public static final String DEFAULT_DSNS_NOTIFICATION_ROUTE_ID = "DefaultDsnsNotificationRoute";

    public static final String NOTIFICATION_REQUEST_VALIDATION_ROUTE = "direct:notificationRequestValidationRoute";
    public static final String NOTIFICATION_REQUEST_VALIDATION_ROUTE_ID = "NotificationRequestValidationRoute";

    public static final String DSNS_NOTIFICATION_PUBLISH_ROUTE = "direct:dsnsNotificationPublishRoute";
    public static final String DSNS_NOTIFICATION_PUBLISH_ROUTE_ID = "DsnsNotificationPublishRoute";

    public static final String DSNS_V1_NOTIFICATION_PUBLISH_ROUTE = "direct:dsnsV1NotificationPublishRoute";
    public static final String DSNS_V1_NOTIFICATION_PUBLISH_ROUTE_ID = "DsnsV1NotificationPublishRoute";

    public static final String  AUTH_TOKEN = "authtoken";

    public enum NotificationChannels {

        IAP("InventoryAdjustmentPo"),
        ORD("Order"),
        ORDPCK("OrderPack"),
        FWD("ForwardASN"),
        RTN("ReturnASN"),
        EMAIL("EmailUpdate");

        String channel;

        NotificationChannels(String channel) {
            this.channel = channel;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String status) {
            this.channel = status;
        }
    }

    public enum OrderStatus{
        SHIP("SHP"),
        PACK("PAK"),
        CREATE("CRT"),
        READY_TO_SHIP("RTS"),
        FULFILLED("FF");

        String status;

        OrderStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
