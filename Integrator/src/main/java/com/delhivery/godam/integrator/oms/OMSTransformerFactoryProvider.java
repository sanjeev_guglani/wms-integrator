package com.delhivery.godam.integrator.oms;

import com.delhivery.godam.integrator.contracts.OMSTransformerFactory;
import com.delhivery.godam.integrator.contracts.commons.annotation.GodamTransformer;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Factory class to provide Godam transformers.
 * Created by delhivery on 30/4/16.
 */
public final class OMSTransformerFactoryProvider {

    private Map<String, OMSTransformerFactory> transformerMap = new HashMap<>();

    /**
     * method to initialize all Godam transformers having GodamTransformer annotation.
     * @param omsTransformerFactories order transformer factories
     */
    public void init(List<OMSTransformerFactory> omsTransformerFactories) {
        omsTransformerFactories.forEach(transformerFactory -> {
            // list all classes having GodamTransformer annotation.
            GodamTransformer[] annotations = transformerFactory.getClass().getAnnotationsByType(GodamTransformer.class);
            for (GodamTransformer annotation : annotations) {
                initTransformer(transformerFactory, annotation);
            }
        });
    }

    /**
     * initialize all GodamTransformer annoted transformer classes
     * @param transformerFactory order transformer factory
     * @param annotation Godam transformer annotation
     */
    private void initTransformer(OMSTransformerFactory transformerFactory, GodamTransformer annotation) {
        if (annotation != null) {
            for (String channel : annotation.channelName()) {
                transformerMap.put(channel, transformerFactory);
            }
        }
    }

    /**
     * method returns transformer based on specified channel
     * @param channel Godam master channel slug name
     * @return order transformer factory
     * @throws TransformerNotFoundException if client transformation not found
     */
    public OMSTransformerFactory getOMSTransformerFactory(String channel)
            throws TransformerNotFoundException {
        if (!transformerMap.containsKey(channel)) {
            throw new TransformerNotFoundException(channel);
        }
        return transformerMap.get(channel);
    }
}
