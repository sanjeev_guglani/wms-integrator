package com.delhivery.godam.integrator.utils;

import com.delhivery.godam.integrator.contracts.exceptions.*;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.camel.ValidationException;
import org.apache.camel.http.common.HttpOperationFailedException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dewbrat Kumar on 22/3/18
 * Utility class used to get common list of handled exceptions
 */
public class ExceptionUtil {

    /**
     * method used to create list of order exceptions to be handled through
     * common exception handler
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForOrder() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(OrderDataException.class);
        exceptionList.addAll(getCommonExceptions());
        exceptionList.addAll(getCommonExceptionsForExtApi());
        return exceptionList;
    }

    /**
     * method used to create list of ASN exceptions to be handled through
     * common exception handler
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForAsn() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(ASNDataException.class);
        exceptionList.addAll(getCommonExceptions());
        return exceptionList;
    }

    /**
     * method used to create list of ASN exceptions to be handled through
     * common exception handler
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForProduct() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(ProductDataException.class);
        exceptionList.addAll(getCommonExceptions());
        return exceptionList;
    }


    /**
     * method used to create list of Notification exceptions to be handled through
     * common exception handler
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForNotification() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(NotificationsDataException.class);
        exceptionList.addAll(getCommonExceptions());
        exceptionList.add(ValidationException.class);
        exceptionList.add(IllegalArgumentException.class);
        return exceptionList;
    }

    /**
     * method used to create list of Notification exceptions to be handled through
     * common exception handler
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForRecallOrder() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(TransferOrderDataException.class);
        exceptionList.add(FCDetailDataException.class);
        exceptionList.addAll(getCommonExceptions());
        return exceptionList;
    }

    /**
     * method to create list of common exceptions which are used for order pack
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getCommonExceptionsForPack(){
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(JsonProcessingException.class);
        return exceptionList;
    }

    /**
     * method to create list of common exceptions which are used on mostly places
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getCommonExceptionsForExtApi(){
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(HttpOperationFailedException.class);
        exceptionList.add(JsonProcessingException.class);
        return exceptionList;
    }

    public static List<Class<? extends Exception>> getCommonExceptionsForManifest(){
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.addAll(getCommonExceptionsForExtApi());
        exceptionList.add(OrderDataException.class);
        return exceptionList;
    }

    /**
     * common exception handler for order cancel
     * @return list of exceptions
     */
    public static List<Class<? extends Exception>> getExceptionListForOrderCancel(){
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(OrderDataException.class);
        exceptionList.add(ProductSellerMappingNotFoundException.class);
        exceptionList.add(ProductStatusMappingNotFoundException.class);
        exceptionList.add(WarehouseMappingNotFoundException.class);
        exceptionList.addAll(getCommonExceptions());
        return exceptionList;
    }

    /**
     * common exception handler used in mostly cases
     * @return list of exceptions
     */
    private static List<Class<? extends Exception>> getCommonExceptions() {
        List<Class<? extends Exception>> exceptionList = new ArrayList<>();
        exceptionList.add(IllegalStateException.class);
        exceptionList.add(EmptyMessageQueueException.class);
        exceptionList.add(InternalServiceException.class);
        exceptionList.add(TransformerNotFoundException.class);
        return exceptionList;
    }

}
