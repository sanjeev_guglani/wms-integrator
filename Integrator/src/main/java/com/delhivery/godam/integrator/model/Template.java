package com.delhivery.godam.integrator.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kumar Vaibhav on 14/11/17.
 * <p>
 * This class contains model of email
 * that is neede to be sent via DSNS
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Channel", "Activity", "Tracking Id", "Error", "Error Class", "url", "Response", "Status Code", "Headers", "Entity", "Subject", "Data"})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Template {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @JsonProperty("Channel")
    private String channel;

    @JsonProperty("Activity")
    private String activity;

    @JsonProperty("Tracking Id")
    private String requestId;

    @JsonProperty("Data")
    private Object body;

    @JsonProperty("Error")
    private String error;

    @JsonProperty("Error Class")
    private String errorClass;

    @JsonProperty("url")
    private String url;

    @JsonProperty("Response")
    private String responseBody;

    @JsonProperty("Status Code")
    private Integer statusCode;

    @JsonProperty("Headers")
    private Map<String, Object> headers;

    @JsonProperty("Subject")
    private String subject;

    @JsonProperty("Entity")
    private String entity;

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return String.format("<font color='red'>%s</font>", error);
    }

    /**
     * method is used to convert template in html email message
     * for sending notification to dsns
     *
     * @return html string content
     * @throws IOException
     */
    @JsonIgnore
    public String getHtmlMessage() {

        StringBuilder emailBuilder = new StringBuilder();
        String htmlTemplate = null;
        try {
            htmlTemplate = IOUtils.toString(Template.class.getResourceAsStream("/template.txt"));

            Map<String, String> substitutedMap = new LinkedHashMap<>();

            Map<String, Object> emailMap = MAPPER.convertValue(this, new TypeReference<LinkedHashMap<String, Object>>() {
            });

            emailMap.forEach((key, value) ->
                    emailBuilder.append(String.format("<span><b>%s</b>: </span> <span>%s</span><br /><br />", key, value.toString())));

            substitutedMap.put("content", emailBuilder.toString());

            return new StrSubstitutor(substitutedMap).replace(htmlTemplate);
        } catch (IOException e) {
            //TODO need to discuss; can;t throw exception here as this method itself called when excepiton ocurred
            return String.format("unable to convert template to map req_id: %s error: %s", requestId, e.getMessage());
        }

    }


}
