package com.delhivery.godam.integrator.config.marketplaces;

/**
 * Created by Sanjeev Guglani on 3/6/19.
 */

import com.delhivery.godam.integrator.contracts.exceptions.AuthDetailsNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.TextApperanceUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class HiltiConfig {

    private Logger logger = LoggerFactory.getLogger(HiltiConfig.class);

    @Value("${endpoint.domain.rabbitmq}")
    private String rabbimqHost;

    @Value("${endpoint.queue.hilti.asn.create}")
    private String asnCreateQueueEndpoint;

    @Value("${endpoint.queue.hilti.order.create}")
    private String orderCreateQueueEndpoint;

    @Value("${hilti.inbound.seller.id}")
    private String sellerId;

    @Value("${auth.hilti.master.user.token}")
    private String masterUserToken;

    @Value("${auth.hilti.client.user.token}")
    private String clientUserToken;

    @Value("${auth.hilti.cs.access.key}")
    private String clientStoreAccessKey;

    @Value("${auth.hilti.inbound.client.key}")
    private String clientKey;

    @Value("${auth.hilti.v2.inbound.client.key}")
    private String clientKeyV2;

    @PostConstruct
    public void init() throws AuthDetailsNotFoundException {
        logger.info(toString());
    }

    public String getRabbimqHost() {
        return rabbimqHost;
    }

    public void setRabbimqHost(String rabbimqHost) {
        this.rabbimqHost = rabbimqHost;
    }

    public String getAsnCreateQueueEndpoint() {
        return rabbimqHost.concat(asnCreateQueueEndpoint);
    }

    public void setAsnCreateQueueEndpoint(String asnCreateQueueEndpoint) {
        this.asnCreateQueueEndpoint = asnCreateQueueEndpoint;
    }

    public String getOrderCreateQueueEndpoint() {
        return rabbimqHost.concat(orderCreateQueueEndpoint);
    }

    public void setOrderCreateQueueEndpoint(String orderCreateQueueEndpoint) {
        this.orderCreateQueueEndpoint = orderCreateQueueEndpoint;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getMasterUserToken() {
        return masterUserToken;
    }

    public void setMasterUserToken(String masterUserToken) {
        this.masterUserToken = masterUserToken;
    }

    public String getClientUserToken() {
        return clientUserToken;
    }

    public void setClientUserToken(String clientUserToken) {
        this.clientUserToken = clientUserToken;
    }

    public String getClientStoreAccessKey() {
        return clientStoreAccessKey;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public String getClientKeyV2() {
        return clientKeyV2;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, TextApperanceUtils.getConfigPrintStyle());
    }

}
