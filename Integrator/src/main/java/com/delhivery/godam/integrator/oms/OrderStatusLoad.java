package com.delhivery.godam.integrator.oms;

import com.delhivery.godam.integrator.contracts.oms.model.Invoice;
import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.*;

/**
 * Created by sachinbhogal on 6/7/17.
 * class is used to create default order ship notification request for dsns
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderStatusLoad {

    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_EMPTY_MESSAGE)
    @JsonProperty("status")
    private String status;

    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_EMPTY_MESSAGE)
    @JsonProperty("order_id")
    private String orderId;

    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_EMPTY_MESSAGE)
    @JsonProperty("order_line_id")
    private String orderLineId;

    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_EMPTY_MESSAGE)
    @JsonProperty("fulfillment_center")
    private String fulfillmentCenter;

    @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
    @JsonProperty("client_store")
    private String clientStore;

    @JsonProperty("weight")
    private String weight;

    @JsonProperty("invoice_date")
    private String invoiceDate;

    @JsonProperty("waybill")
    private String waybill;

    @JsonProperty("waybill_number")
    private String waybillNumber;

    @JsonProperty("shipping_date")
    private String shippingDate;

    @JsonProperty("courier")
    private String courier;

    @Valid
    @JsonProperty("products")
    private List<Product> products;

    @JsonProperty("invoice_details")
    private Invoice invoiceDetails;

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets order id.
     *
     * @return the order id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets order id.
     *
     * @param orderId the order id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets order line id.
     *
     * @return the order line id
     */
    public String getOrderLineId() {
        return orderLineId;
    }

    /**
     * Sets order line id.
     *
     * @param orderLineId the order line id
     */
    public void setOrderLineId(String orderLineId) {
        this.orderLineId = orderLineId;
    }

    /**
     * Gets fulfillment center.
     *
     * @return the fulfillment center
     */
    public String getFulfillmentCenter() {
        return fulfillmentCenter;
    }

    /**
     * Sets fulfillment center.
     *
     * @param fulfillmentCenter the fulfillment center
     */
    public void setFulfillmentCenter(String fulfillmentCenter) {
        this.fulfillmentCenter = fulfillmentCenter;
    }

    /**
     * Gets client store.
     *
     * @return the client store
     */
    public String getClientStore() {
        return clientStore;
    }

    /**
     * Sets client store.
     *
     * @param clientStore the client store
     */
    public void setClientStore(String clientStore) {
        this.clientStore = clientStore;
    }

    /**
     * Gets products.
     *
     * @return the products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     * Sets products.
     *
     * @param products the products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Gets weight.
     *
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * Sets weight.
     *
     * @param weight the weight
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * Gets invoice date.
     *
     * @return the invoice date
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets invoice date.
     *
     * @param invoiceDate the invoice date
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * Gets waybill.
     *
     * @return the waybill
     */
    public String getWaybill() {
        return waybill;
    }

    /**
     * Sets waybill number.
     *
     * @param waybillNumber the waybill
     */
    public void setWaybillNumber(String waybillNumber) {
        this.waybillNumber = waybillNumber;
    }

    /**
     * Gets waybill number.
     *
     * @return the waybillNumber
     */
    public String getWaybillNumber() {
        return waybillNumber;
    }

    /**
     * Sets waybill.
     *
     * @param waybill the waybill
     */
    public void setWaybill(String waybill) {
        this.waybill = waybill;
    }

    /**
     * Gets shipping date.
     *
     * @return the shipping date
     */
    public String getShippingDate() {
        return shippingDate;
    }

    /**
     * Sets shipping date.
     *
     * @param shippingDate the shipping date
     */
    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    /**
     * Gets courier.
     *
     * @return the courier
     */
    public String getCourier() {
        return courier;
    }

    /**
     * Sets courier.
     *
     * @param courier the courier
     */
    public void setCourier(String courier) {
        this.courier = courier;
    }

    /**
     * The type Product.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Product {

        @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
        @JsonProperty("status")
        private String status;

        @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
        @NotNull(message = NOT_NULL_MESSAGE)
        @NotEmpty(message = NOT_EMPTY_MESSAGE)
        @JsonProperty("prod_sku")
        private String prodSku;

        @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
        @NotNull(message = NOT_NULL_MESSAGE)
        @NotEmpty(message = NOT_EMPTY_MESSAGE)
        @JsonProperty("prod_num")
        private String prodNum;

        @JsonProperty("prod_qty")
        private int prodQty;

        @Size(max = 255, message = CHAR_LIMIT_MESSAGE)
        @NotNull(message = NOT_NULL_MESSAGE)
        @NotEmpty(message = NOT_EMPTY_MESSAGE)
        @JsonProperty("prod_name")
        private String prodName;

        @JsonProperty("imei")
        private String imei;

        @JsonProperty("shipment_number")
        private String shipmentNumber;

        /**
         * Gets status.
         *
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets status.
         *
         * @param status the status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * Gets prod sku.
         *
         * @return the prod sku
         */
        public String getProdSku() {
            return prodSku;
        }

        /**
         * Sets prod sku.
         *
         * @param prodSku the prod sku
         */
        public void setProdSku(String prodSku) {
            this.prodSku = prodSku;
        }

        /**
         * Gets prod num.
         *
         * @return the prod num
         */
        public String getProdNum() {
            return prodNum;
        }

        /**
         * Sets prod num.
         *
         * @param prodNum the prod num
         */
        public void setProdNum(String prodNum) {
            this.prodNum = prodNum;
        }

        /**
         * Gets prod qty.
         *
         * @return the prod qty
         */
        public int getProdQty() {
            return prodQty;
        }

        /**
         * Sets prod qty.
         *
         * @param prodQty the prod qty
         */
        public void setProdQty(int prodQty) {
            this.prodQty = prodQty;
        }

        /**
         * Gets prod name.
         *
         * @return the prod name
         */
        public String getProdName() {
            return prodName;
        }

        /**
         * Sets prod name.
         *
         * @param prodName the prod name
         */
        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        /**
         * Gets imei.
         *
         * @return the imei
         */
        public String getImei() {
            return imei;
        }

        /**
         * Sets imei.
         *
         * @param imei the imei
         */
        public void setImei(String imei) {
            this.imei = imei;
        }

        /**
         * Gets shipment number.
         *
         * @return the shipment number
         */
        public String getShipmentNumber() {
            return shipmentNumber;
        }

        /**
         * Sets shipment number.
         *
         * @param shipmentNumber the shipment number
         */
        public void setShipmentNumber(String shipmentNumber) {
            this.shipmentNumber = shipmentNumber;
        }

        @Override
        public String toString() {
            return Mapper.toString(this);
        }
    }

    /**
     * Gets invoice details.
     *
     * @return the invoice details
     */
    public Invoice getInvoiceDetails() {
        return invoiceDetails;
    }

    /**
     * Sets invoice details.
     *
     * @param invoiceDetails the invoice details
     */
    public void setInvoiceDetails(Invoice invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }
}
