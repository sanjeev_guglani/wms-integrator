package com.delhivery.godam.integrator.oms;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Kumar Vaibhav on 21/12/17.
 * <p>
 * This class contains model to
 * wrap Default Ordership Request
 */
public class OrderStatusWrapper {

    @JsonProperty("orderlines")
    private List<OrderStatusLoad> orderStatusLoadList;

    /**
     * Gets order ship load list.
     *
     * @return the order ship load list
     */
    public List<OrderStatusLoad> getOrderStatusLoadList() {
        return orderStatusLoadList;
    }

    /**
     * Sets order ship load list.
     *
     * @param orderStatusLoadList the order ship load list
     */
    public void setOrderStatusLoadList(List<OrderStatusLoad> orderStatusLoadList) {
        this.orderStatusLoadList = orderStatusLoadList;
    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }
}
