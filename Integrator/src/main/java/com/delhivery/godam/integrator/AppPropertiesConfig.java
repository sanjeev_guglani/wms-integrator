package com.delhivery.godam.integrator;

/*
  @author saurabhsharma
 * @since 07/07/16
 * This class provide utility methods for the properties
 */

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AppPropertiesConfig {

    private static final Logger logger = LoggerFactory.getLogger(AppPropertiesConfig.class);

    @Autowired
    ConfigurableEnvironment env;

    public String getStringProperty(String key, String defaultValue) {
        if (StringUtils.isEmpty(env.getProperty(key)))
            return defaultValue;
        else
            return env.getProperty(key);
    }

    public Optional<String> getStringProperty(String key) {
        if (StringUtils.isEmpty(env.getProperty(key)))
            return Optional.empty();
        else
            return Optional.of(env.getProperty(key));
    }

    public List<String> convertStringTokentoListWSeperator(String key, String seperator, String defaultValue) {
        return Arrays.stream(getStringProperty(key, defaultValue).split(seperator)).collect(Collectors.toList());
    }

    public List<String> convertStringTokentoList(String key, String defaultValue) {
        return convertStringTokentoListWSeperator(key, ",", defaultValue);
    }

    public boolean isCamelContextTraceEnabled() {
        return Boolean.valueOf(env.getProperty("integrator.camel.context.log.trace.enable", "true"));
    }

    public enum RouteGroups {
        DEFAULT,
        HILTI,
    }
}
