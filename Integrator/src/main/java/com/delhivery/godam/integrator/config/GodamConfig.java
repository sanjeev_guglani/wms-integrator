package com.delhivery.godam.integrator.config;

import com.delhivery.godam.integrator.contracts.utils.TextApperanceUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * updated by pradeepverma on 29/7/17.
 * class have all configurations for Godam.
 * use this wherever you need to read client property.
 */
@Component
public class GodamConfig {

    private Logger logger = LoggerFactory.getLogger(GodamConfig.class);

    @Value("${endpoint.domain.godam}")
    private String godamHost;

    @Value("${endpoint.domain.wms.version.two}")
    private String wmsTwoHost;

    @Value("${endpoint.api.godam.order.create.v1}")
    private String apiCreateOrderVOne;

    @Value("${endpoint.api.wms.v2.asn.create}")
    private String apiWMSV2AsnCreate;

    @Value("${endpoint.api.wms.v2.order.create}")
    private String apiWMSV2OrderCreate;

    @Value("${endpoint.api.godam.bulk.order.create.v1}")
    private String apiCreateBulkOrderVOne;

    @Value("${endpoint.api.godam.order.create.v2}")
    private String apiCreateOrderVTwo;

    @Value("${endpoint.api.godam.order.update.v2}")
    private String apiUpdateOrderVTwo;

    @Value("${endpoint.api.godam.order.update.v3}")
    private String apiUpdateOrderVThree;

    @Value("${endpoint.api.godam.product.create.v1}")
    private String apiProductCreateVOne;

    @Value("${endpoint.api.godam.product.create.v2}")
    private String apiProductCreateVTwo;

    @Value("${endpoint.api.godam.product.update.v2}")
    private String apiProductUpdateVTwo;

    @Value("${endpoint.api.godam.asn.create.v1}")
    private String apiCreateAsnVOne;

    @Value("${endpoint.api.godam.asn.create.v2}")
    private String apiCreateAsnVTwo;

    @Value("${endpoint.api.godam.asn.update.v2}")
    private String apiUpdateAsnVTwo;

    @Value("${endpoint.api.godam.order.fullfilment.v1}")
    private String apiFulfillOrderVOne;

    @Value("${endpoint.api.godam.asn.edit.v1}")
    private String apiEditAsnVOne;

    @Value("${endpoint.api.godam.product.channel.mapping.list.v1}")
    private String apiListProductChannelMappings;

    @Value("${endpoint.api.godam.product.channel.mapping.create.v1}")
    private String apiCreateProductChannelMapping;

    @Value("${endpoint.api.godam.product.recall}")
    private String apiProductRecall;

    @Value("${endpoint.api.godam.product.detail.v1}")
    private String apiFetchProductDetails;

    @Value("${endpoint.api.godam.bulk.error.log.v1}")
    private String apiCreateBulkErrorLog;

    @Value("${endpoint.api.godam.rules.v1}")
    private String apiRulesVOne;

    @Value("${endpoint.api.godam.product.search}")
    private String apiProductSearch;

    @Value("${endpoint.api.godam.asn.detail}")
    private String apiAsnDetail;

    @Value("${endpoint.api.godam.product.edit.component}")
    private String apiProductEditComponent;

    @Value("${endpoint.api.godam.invoice.update}")
    private String apiInvoiceUpdate;

    @Value("${godam.aws.s3.key.invoices}")
    private String invoiceBucket;

    @Value("${godam.aws.s3.key.packslips}")
    private String packBucket;

    @Value("${godam.aws.s3.key.manifest}")
    private String manifestBucket;

    @Value("${order.batch.size}")
    private int orderBatchSize;

    @Value("${asn.batch.size}")
    private int asnBatchSize;

    @Value("${product.batch.size}")
    private int productBatchSize;

    @Value("${endpoint.api.godam.fc.detail}")
    private String apiFCDetails;

    @Value("${endpoint.api.godam.order.cancel}")
    private String apiOrderCancel;

    @Value("${endpoint.api.godam.product.channel.mapping.list.v2}")
    private String apiListProductChannelMappingsV2;

    @Value("${godam.invoice.url.prefix}")
    private String godamInvoiceUrlPrefix;

    @Value("${endpoint.api.godam.asn.product.search}")
    private String apiASNProductDetail;

    @Value("${endpoint.api.godam.waybill.status.update}")
    private String apiWaybillStatus;

    @Value("${waybill.status.update.batch.size}")
    private int waybillStatusUpdateBatchSize;

    @Value("${endpoint.api.godam.order.process.history.v1}")
    private String apiGetOrderHistory;

    @Value("${wms.two.integrator.uuid}")
    private String integratorUuid;

    public String getIntegratorUuid() {
        return integratorUuid;
    }

    public String getApiCreateOrderVOne() {
        return godamHost.concat(apiCreateOrderVOne);
    }

    public String getApiCreateBulkOrderVOne() {
        return godamHost.concat(apiCreateBulkOrderVOne);
    }

    public String getApiCreateOrderVTwo() {
        return godamHost.concat(apiCreateOrderVTwo);
    }

    public String getApiUpdateOrderVTwo() {
        return godamHost.concat(apiUpdateOrderVTwo);
    }

    public String getApiWMSV2AsnCreate() {
        return wmsTwoHost.concat(apiWMSV2AsnCreate);
    }

    public String getApiUpdateOrderVThree() {
        return godamHost.concat(apiUpdateOrderVThree);
    }

    public String getApiProductCreateVOne() {
        return godamHost.concat(apiProductCreateVOne);
    }

    public String getApiCreateAsnVOne() {
        return godamHost.concat(apiCreateAsnVOne);
    }

    public String getApiFulfillOrderVOne() {
        return godamHost.concat(apiFulfillOrderVOne);
    }

    public String getApiEditAsnVOne() {
        return godamHost.concat(apiEditAsnVOne);
    }

    public String getApiListProductChannelMappings() {
        return godamHost.concat(apiListProductChannelMappings);
    }

    public String getApiCreateProductChannelMapping() {
        return godamHost.concat(apiCreateProductChannelMapping);
    }

    public String getApiProductRecall() {
        return godamHost.concat(apiProductRecall);
    }

    public String getApiFetchProductDetails() {
        return godamHost.concat(apiFetchProductDetails);
    }

    public String getApiCreateBulkErrorLog() {
        return godamHost.concat(apiCreateBulkErrorLog);
    }

    public String getApiRulesVOne() {
        return godamHost.concat(apiRulesVOne);
    }

    public String getApiProductCreateVTwo() {
        return godamHost.concat(apiProductCreateVTwo);
    }

    public String getApiProductUpdateVTwo() {
        return godamHost.concat(apiProductUpdateVTwo);
    }

    public String getApiCreateAsnVTwo() {
        return godamHost.concat(apiCreateAsnVTwo);
    }

    public String getApiUpdateAsnVTwo() {
        return godamHost.concat(apiUpdateAsnVTwo);
    }

    public String getApiProductSearch() {
        return godamHost.concat(apiProductSearch);
    }

    public String getApiProductEditComponent() {
        return godamHost.concat(apiProductEditComponent);
    }

    public String getApiInvoiceUpdate() {
        return godamHost.concat(apiInvoiceUpdate);
    }

    public String getApiAsnDetail() {
        return godamHost.concat(apiAsnDetail);
    }

    public String getInvoiceBucket() {
        return invoiceBucket;
    }

    public String getPackBucket() {
        return packBucket;
    }

    public String getManifestBucket() {
        return manifestBucket;
    }

    public int getOrderBatchSize() {
        return orderBatchSize;
    }

    public int getAsnBatchSize() {
        return asnBatchSize;
    }

    public int getProductBatchSize() {
        return productBatchSize;
    }

    public String getApiFCDetails() {
        return godamHost.concat(apiFCDetails);
    }

    public String getApiOrderCancel() {
        return godamHost.concat(apiOrderCancel);
    }

    public String getApiListProductChannelMappingsV2() {
        return godamHost.concat(apiListProductChannelMappingsV2);
    }

    public String getApiASNProductDetail() {
        return godamHost.concat(apiASNProductDetail);
    }

    public String getApiWaybillStatus() {
        return godamHost.concat(apiWaybillStatus);
    }

    public int getWaybillStatusUpdateBatchSize() {
        return waybillStatusUpdateBatchSize;
    }

    public String getApiOrderHistory() {
        return godamHost.concat(apiGetOrderHistory);
    }

    public String getApiWMSV2OrderCreate() {
        return wmsTwoHost.concat(apiWMSV2OrderCreate);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, TextApperanceUtils.getConfigPrintStyle());
    }

    @PostConstruct
    private void init() {
        logger.info(toString());
    }
}
