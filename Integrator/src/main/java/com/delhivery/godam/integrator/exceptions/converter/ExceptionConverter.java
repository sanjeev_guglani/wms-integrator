package com.delhivery.godam.integrator.exceptions.converter;

import com.amazonaws.SdkBaseException;
import com.delhivery.godam.integrator.contracts.exceptions.*;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.delhivery.godam.integrator.model.Template;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.Exchange;
import org.apache.camel.http.common.HttpOperationFailedException;

import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.IOException;

/**
 * created by sachin bhogal
 * interface to handle exceptions
 */
public interface ExceptionConverter {

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(HttpOperationFailedException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(ASNDataException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(OrderDataException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(ProductDataException ex, Exchange exchange);



    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(NotificationsDataException ex, Exchange exchange);


    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(EmptyMessageQueueException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    //Template convert(WarehouseMappingNotFoundException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(CamelExecutionException ex, Exchange exchange);


    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(JsonProcessingException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(TransferOrderDataException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(FCDetailDataException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(InvalidUnitOfMeasurementException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(AuthDetailsNotFoundException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(IllegalArgumentException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(WebServiceException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(SOAPFaultException ex, Exchange exchange);



    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(SdkBaseException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(OrderChannelMappingException ex, Exchange exchange);

    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(IOException ex, Exchange exchange);


    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(ProductSellerMappingNotFoundException ex, Exchange exchange);



    /**
     * Convert template.
     *
     * @param ex       the ex
     * @param exchange the exchange
     * @return the template
     */
    Template convert(ProductStatusMappingNotFoundException ex, Exchange exchange);


    /**
     * creates an email template to be sent
     * @param ex the exception IllegalStateException
     * @param exchange camel exchange
     * @return template for email
     */
    Template convert(IllegalStateException ex, Exchange exchange);

    /**
     * creates an email template to be sent
     * @param ex the exception InternalServiceException
     * @param exchange camel exchange
     * @return template for email
     */
    Template convert(InternalServiceException ex, Exchange exchange);

}
