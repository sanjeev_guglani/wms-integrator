package com.delhivery.godam.integrator.utils;

import com.delhivery.godam.integrator.logging.service.CamelLoggingService;
import org.apache.camel.Exchange;
import org.apache.camel.spi.ExchangeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sachin bhogal on 31/7/17.
 * <p>
 * class is used to format the incoming messages from exchanges
 * so that we can follow same logging format across application
 * <p>
 * Note: bean name of this class must be "logFormatter" otherwise
 * camel will not able to find it from registry
 * alternative to this is manually create logComponent and
 * explicitly set exchangeFormatter in manually created log component
 */
@Component("logFormatter")
public class LogFormatter implements ExchangeFormatter {


    @Autowired
    private CamelLoggingService loggingService;

    /**
     * method is used to format the log message
     * return string will be actual log message
     *
     * @param exchange exchange
     * @return log message
     */
    @Override
    public String format(Exchange exchange) {

        return loggingService.createLogFromExchange(exchange).toString();
    }


}
