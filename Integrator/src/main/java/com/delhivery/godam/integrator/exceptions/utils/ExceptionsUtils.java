package com.delhivery.godam.integrator.exceptions.utils;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel;

import java.util.EnumSet;

/**
 * created by sachin bhogal
 * helper class for exception handling
 */
public class ExceptionsUtils {

    private ExceptionsUtils(){
        //Private constructor
    }

    public static final String LOGGER = "logger";
    public static final String LOGGING_ENDPOINT = "log:${exchangeProperty.logger}?level=WARN";

    static EnumSet<MasterChannel> getDSNSClients() {

        return EnumSet.complementOf(EnumSet.of(MasterChannel.LEVIS, MasterChannel.DEFAULT_CSV));
    }

    static EnumSet<MasterChannel> getRequestTrackerClients() {

        return EnumSet.of(MasterChannel.LEVIS, MasterChannel.DEFAULT_CSV);
    }
}
