package com.delhivery.godam.integrator.exceptions.utils;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import com.delhivery.godam.integrator.model.Template;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import static com.delhivery.godam.integrator.exceptions.utils.ExceptionsUtils.getDSNSClients;
import static com.delhivery.godam.integrator.exceptions.utils.ExceptionsUtils.getRequestTrackerClients;

/**
 * created by sachin bhogal
 */
@Service
public class DefaultConfigurator implements Configurator {

    private static Map<String, ActionType> actionTypeMap = new HashMap<>();

    @PostConstruct
    public void init() {

        Arrays.stream(ActionType.values()).forEach(actionType -> {
            getClients(actionType)
                    .forEach(masterChannel -> actionTypeMap.put(masterChannel.getSlugName(), actionType));
        });
    }

    @Override
    public ActionType getActionType(Template template) {
        return actionTypeMap.getOrDefault(template.getChannel(),ActionType.DSNS);
    }

    private static EnumSet<GodamUtils.MasterChannel> getClients(ActionType actionType) {
        if (actionType == ActionType.REQUEST_TRACKER) {
            return getRequestTrackerClients();
        } else {
            return getDSNSClients();
        }
    }
}
