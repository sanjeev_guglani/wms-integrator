package com.delhivery.godam.integrator.exceptions;

import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;

import javax.validation.constraints.NotNull;

/**
 * Created by Dewbrat Kumar on 27/3/17.
 */
public class NotificationTransformerNotFoundException extends TransformerNotFoundException {

    private String channelName;

    private String type;

    public NotificationTransformerNotFoundException(@NotNull String channelName, @NotNull String type) {
        super();
        this.channelName = channelName;
        this.type = type;
    }

    public String getMessage() {
        return String.format("[%s] notification transformer not defined for channel [%s]", type, channelName);
    }
}
