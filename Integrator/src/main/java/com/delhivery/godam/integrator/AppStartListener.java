package com.delhivery.godam.integrator;

import com.delhivery.godam.db.dao.EnvironmentProperties.Impl.EnvironmentPropertiesDaoImpl;
import com.delhivery.godam.db.entity.EnvironmentProperties;
import com.delhivery.godam.integrator.logging.InternalLogger;
import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.apache.camel.management.event.CamelContextStartedEvent;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.support.EventNotifierSupport;
import org.apache.camel.util.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by Kumar Vaibhav on 16/6/17.
 * Gets all routes associated with CamelContextEvent
 * Gets all enabled groups
 * Starts the route for enabled groups
 */
@Component
class AppStartListener extends EventNotifierSupport implements ApplicationListener<ContextRefreshedEvent    > {

    public static final Logger logger = LoggerFactory.getLogger(AppStartListener.class);

    @Autowired
    private CamelContext camelContext;

    @Value("${enabled.camel.route.groups}")
    private String enabledRouteGroups;

    @Autowired
    private ConfigurableEnvironment environment;

    @Autowired
    private EnvironmentPropertiesDaoImpl environmentPropertiesDao;
    /**
     * Captures all routes defined & enabled groups
     * @param event Event Object
     */
    @Override
    public void notify(EventObject event) throws Exception {

        if (event instanceof CamelContextStartedEvent) {
            CamelContextStartedEvent camelContextEvent = (CamelContextStartedEvent) event;

            Set<String> enabledRouteGroups = getEnabledRouteGroups();
            startGroupEnabledRoutes(camelContextEvent.getContext(), enabledRouteGroups);
        }
    }

    /**
     * starts all routes from the enabled groups
     * @param context camel context
     * @param enabledRouteGroups groups need to be enabled
     */
    private void startGroupEnabledRoutes(CamelContext context, Set<String> enabledRouteGroups) throws Exception {

        for (Route route : context.getRoutes()) {
            RouteDefinition routeDefinition = context.getRouteDefinition(route.getId());
            if (isGroupDefined(routeDefinition)) {
                String group = routeDefinition.getGroup();
                boolean isRouteStarted = false;

                if (enabledRouteGroups.contains(group.toLowerCase())) {
                    ServiceHelper.startService(route.getConsumer());
                    isRouteStarted = true;
                }

                InternalLogger.info(logger, String.format("route group [%s], group-enabled [%s], route [%s], started [%s]",
                        group, isRouteStarted, route.getId(), isRouteStarted));

            } else {
                String error = String.format(
                        "Unable to start: %s, Please define group of route.", routeDefinition.getId());
                throw new RuntimeException(error);
            }
        }
    }

    /**
     * Checks whether group is defined for a route
     * @param routeDefinition check group name defined or not in context
     */
    private boolean isGroupDefined(RouteDefinition routeDefinition) {

        return routeDefinition.getGroup() != null;
    }

    @Override
    public boolean isEnabled(EventObject eventObject) {

        return true;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        camelContext.getManagementStrategy().addEventNotifier(this);
        camelContext.setTracing(false);
    }

    /**
     * returns all enabled groups in the properties file
     * @return Set of enabled route groups
     */
    private Set<String> getEnabledRouteGroups() {
        return new HashSet<>(Arrays.asList(enabledRouteGroups.trim().toLowerCase().split(",")));
    }

    /**
     * Updating the environment properties from the Database
     * @return void
     */

    @PostConstruct
    private void onConstruct(){
        String environmentPropertyName = "applicationConfig: [classpath:/application-"+environment.getActiveProfiles()[0]+".properties]";
        String fromDBPropertiesName = "database-"+environment.getActiveProfiles()[0]+" Properties";

        /**
         * Default properties from environment
         *
         * String defaultPropertyName = "applicationConfig: [classpath:/application.properties]";
         * Hashtable<String, String> defaultProperties = (Hashtable<String, String>) environment.getPropertySources().get(defaultPropertyName).getSource();
         **/
        MutablePropertySources propertySources = environment.getPropertySources();
        Properties environmentPropertiesWrapper = new Properties();
        List<EnvironmentProperties> environmentPropertiesFromDB = environmentPropertiesDao.getAllProperties();
        environmentPropertiesFromDB.forEach(environmentPropertyFromDB -> environmentPropertiesWrapper.put(environmentPropertyFromDB.getKey(), environmentPropertyFromDB.getValue()));
        if(environment.getPropertySources().contains(environmentPropertyName)){
            propertySources.addBefore(environmentPropertyName, new PropertiesPropertySource(fromDBPropertiesName,environmentPropertiesWrapper));
        }else {
            propertySources.addFirst(new PropertiesPropertySource(fromDBPropertiesName,environmentPropertiesWrapper));
        }

    }
}