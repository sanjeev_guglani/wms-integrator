package com.delhivery.godam.integrator.utils;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils.Activity;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils.Entity;
import com.delhivery.godam.integrator.contracts.utils.Utility;
import org.springframework.util.Assert;

/**
 * Created by sachin bhogal on 31/7/17.
 * <p>
 */
public interface DirectEndpointMapper {

    /**
     * method is used to create route name by combination of channel entity and activity
     * for which the route is being written
     * method will read channel name and acitiviy from
     * IntegratorRoute annotation from class which contains route
     * and provides a method to extract meta info from the annotation
     *
     * @return dynamically generated routeName from @IntegratorRoute annotation
     */
    default String getRouteEndpoint() {

        return Utility.getRouteEndpoint(getChannelName(), getActivity());
    }

    /**
     * method to get channel name from @IntegratorRoute annotation
     *
     * @return slug name of channel
     */
    default String getChannelName() {

        Assert.notNull(getClass().getAnnotation(IntegratorRoute.class),
                String.format("Please Put @IntegratorRoute on the class:[%s]", getClass().getSimpleName()));

        return getClass().getAnnotation(IntegratorRoute.class).channel().getSlugName();
    }

    /**
     * method to get activity name from @IntegratorRoute annotation
     *
     * @return activity name
     */
    default String getActivity() {

        Entity entity = getClass().getAnnotation(IntegratorRoute.class).entity();
        Activity activity = getClass().getAnnotation(IntegratorRoute.class).activity();

        Assert.doesNotContain(entity.name(),
                Entity.NONE.name(), String.format("Please Provide Valid Entity for Route:[%s]", getClass().getSimpleName()));

        return entity.getActivity(activity);
    }
}
