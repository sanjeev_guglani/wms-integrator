package com.delhivery.godam.integrator.utils;

/**
 * created by sachin bhogal
 * class contains various constants , expression related to camel routes
 */
public class CamelUtils {

    public interface SimpleExpressions {

        String INFO_LOG_ENDPOINT = "log:${routeId}?level=INFO";
        String WARN_LOG_ENDPOINT = "log:${routeId}?level=WARN";
        String ERROR_LOG_ENDPOINT = "log:${routeId}?level=ERROR";
        String DEBUG_LOG_ENDPOINT = "log:${routeId}?level=DEBUG";

    }
}
