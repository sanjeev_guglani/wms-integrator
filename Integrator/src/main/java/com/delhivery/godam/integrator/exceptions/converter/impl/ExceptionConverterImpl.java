package com.delhivery.godam.integrator.exceptions.converter.impl;

import com.amazonaws.SdkBaseException;
import com.delhivery.godam.integrator.contracts.crts.TrackingInfo;
import com.delhivery.godam.integrator.contracts.exceptions.*;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.delhivery.godam.integrator.exceptions.converter.ExceptionConverter;
import com.delhivery.godam.integrator.model.Template;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.camel.Body;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.Exchange;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.IOException;

import static com.delhivery.godam.integrator.EmailProcessor.REQUEST_BODY;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

@Service
public class ExceptionConverterImpl implements ExceptionConverter {

    @Autowired
    private Environment environment;

    private static String activeEnv;

    private static final String HTTP_OPERATION_FAILED = "Http Operation Failed";
    private static final String ASN_DATA_ERROR = "Asn Data Error";
    private static final String ORDER_DATA_ERROR = "Order Data Error";
    private static final String PRODUCT_DATA_ERROR = "Product Data Error";
    private static final String MULTI_COMPONENT_DATA_ERROR = "Multi Component Data Error";
    private static final String NOTIFICATION_DATA_ERROR = "Notification Data Error";
    private static final String CONFIGURATION_DATA_ERROR = "Configuration Error";
    private static final String ITEM_DATA_ERROR = "Items Data Error";
    private static final String DATA_ERROR = "Data error";
    private static final String EMPTY_MESSAGE_QUEUE_ERROR = "Empty Message Queue Error";
    private static final String RECALL_DATA_ERROR = "Recall Data Error";
    private static final String MAPPING_DATA_ERROR = "Mapping Data Error";
    private static final String CARRIER_MANAGER_ERROR = "Carrier Allocation Error";
    private static final String SDK_DATA_ERROR = "Amazon SDK Error";
    private static final String RUNTIME_ERROR = "Runtime Error";
    private static final String INVALID_JSON_DATA = "Invalid JSON Data";
    private static final String FC_DETAIL_ERROR = "fc detail error";
    private static final String WEB_SERVICE_ERROR = "Web Service Exception";
    private static final String SOAP_FAULT_ERROR = "Soap Fault Exception";
    private static final String REQUEST_REJECTED = "Request Rejected";
    private static final String FILE_EXTENSION_ERROR = "wrong file extension";
    private static final String ORDER_CHANNEL_ERROR = "Order Channel mapping missing";
    private static final String RETURN_PICKUP_DATA_ERROR = "Return pickup data error";

    @PostConstruct
    public void init() {

        activeEnv = environment.getActiveProfiles()[0];
    }

    @Override
    public Template convert(@Body HttpOperationFailedException ex, Exchange exchange) {
        Template template = getTemplate(ex, exchange, HTTP_OPERATION_FAILED);
        template.setResponseBody(ex.getResponseBody());
        template.setStatusCode(ex.getStatusCode());
        template.setEntity(exchange.getProperty(ENTITY, String.class));
        return template;
    }

    @Override
    public Template convert(@Body ASNDataException ex, Exchange exchange) {
        Template template = getTemplate(ex, exchange, ASN_DATA_ERROR);
        template.setEntity(TrackingInfo.Entity.ASN.name());
        return template;
    }

    @Override
    public Template convert(@Body OrderDataException ex, Exchange exchange) {
        Template template = getTemplate(ex, exchange, ORDER_DATA_ERROR);
        template.setEntity(TrackingInfo.Entity.ORDER.name());
        return template;
    }

    @Override
    public Template convert(@Body ProductDataException ex, Exchange exchange) {
        Template template = getTemplate(ex, exchange, PRODUCT_DATA_ERROR);
        template.setEntity(TrackingInfo.Entity.PRODUCT.name());
        return template;
    }



    @Override
    public Template convert(@Body NotificationsDataException ex, Exchange exchange) {
        Template template = getTemplate(ex, exchange, NOTIFICATION_DATA_ERROR);
        template.setEntity(TrackingInfo.Entity.NOTIFICATION.name());
        return template;
    }


    @Override
    public Template convert(EmptyMessageQueueException ex, Exchange exchange) {
        return getTemplate(ex, exchange, EMPTY_MESSAGE_QUEUE_ERROR);
    }



    @Override
    public Template convert(JsonProcessingException ex, Exchange exchange) {
        return getTemplate(ex, exchange, INVALID_JSON_DATA);
    }

    @Override
    public Template convert(TransferOrderDataException ex, Exchange exchange) {
        return getTemplate(ex, exchange, DATA_ERROR);
    }

    @Override
    public Template convert(FCDetailDataException ex, Exchange exchange) {
        return getTemplate(ex, exchange, FC_DETAIL_ERROR);
    }

    @Override
    public Template convert(InvalidUnitOfMeasurementException ex, Exchange exchange) {
        return getTemplate(ex, exchange, REQUEST_REJECTED);
    }

    @Override
    public Template convert(WarehouseMappingNotFoundException ex, Exchange exchange) {
        return getTemplate(ex, exchange, CONFIGURATION_DATA_ERROR);
    }

    @Override
    public Template convert(AuthDetailsNotFoundException ex, Exchange exchange) {
        return getTemplate(ex, exchange, REQUEST_REJECTED);
    }

    @Override
    public Template convert(IllegalArgumentException ex, Exchange exchange) {
        return getTemplate(ex, exchange, REQUEST_REJECTED);
    }

    @Override
    public Template convert(@Body WebServiceException ex, Exchange exchange) {
        return getTemplate(ex, exchange, WEB_SERVICE_ERROR);
    }

    @Override
    public Template convert(SOAPFaultException ex, Exchange exchange) {
        return getTemplate(ex, exchange, SOAP_FAULT_ERROR);
    }

    @Override
    public Template convert(@Body SdkBaseException ex, Exchange exchange) {
        return getTemplate(ex, exchange, SDK_DATA_ERROR);
    }


    @Override
    public Template convert(OrderChannelMappingException ex, Exchange exchange) {
        return getTemplate(ex, exchange, ORDER_CHANNEL_ERROR);
    }

    @Override
    public Template convert(IOException ex, Exchange exchange) {
        return getTemplate(ex, exchange, HTTP_OPERATION_FAILED);
    }


    @Override
    public Template convert(ProductSellerMappingNotFoundException ex, Exchange exchange) {
        return getTemplate(ex, exchange, MAPPING_DATA_ERROR);
    }

    @Override
    public Template convert(InternalServiceException ex, Exchange exchange) {
        return getTemplate(ex, exchange, DATA_ERROR);
    }



    @Override
    public Template convert(ProductStatusMappingNotFoundException ex, Exchange exchange) {
        return getTemplate(ex, exchange, MAPPING_DATA_ERROR);
    }

    @Override
    public Template convert(IllegalStateException ex, Exchange exchange) {
        return getTemplate(ex, exchange, ORDER_DATA_ERROR);
    }


    /**
     * method to create default template
     *
     * @param exchange consumes camel exchange
     * @return Template Object
     */
    private Template getTemplate(Exception ex, Exchange exchange, String message) {
        String channel = exchange.getProperty(CHANNEL, String.class);
        String activity = exchange.getProperty(ACTIVITY, String.class);
        return Template.builder()
                .requestId(exchange.getProperty(REQUEST_ID, String.class))
                .channel(channel)
                .activity(activity)
                .errorClass(ex.getClass().getCanonicalName())
                .error(ex.getMessage())
                .body(exchange.getProperty(REQUEST_BODY))
                .headers(exchange.getIn().getHeaders())
                .subject(createSubject(channel, activity, message))
                .build();
    }

    /**
     * method to create subject for dsns error email
     *
     * @param channel  master channel name
     * @param activity activity name for which mail to be sent
     * @param message  extra message required for email subject
     * @return formatted email subject
     */
    private String createSubject(String channel, String activity, String message) {
        return String.format("[%s] %s %s %s", StringUtils.upperCase(activeEnv), StringUtils.capitalize(channel), activity, message);
    }

    @Override
    public Template convert(CamelExecutionException ex, Exchange exchange) {
        Exception exception = (Exception) ex.getExchange().getProperty(Exchange.EXCEPTION_CAUGHT);

        if (exception instanceof ASNDataException) {
            return convert((ASNDataException) exception, ex.getExchange());
        }
        if (exception instanceof ProductDataException) {
            return convert((ProductDataException) exception, ex.getExchange());
        }
        if (exception instanceof OrderDataException) {
            return convert((OrderDataException) exception, ex.getExchange());
        }
        if (exception instanceof HttpOperationFailedException) {
            return convert((HttpOperationFailedException) exception, ex.getExchange());
        }
        if (exception instanceof InvalidUnitOfMeasurementException) {
            return convert((InvalidUnitOfMeasurementException) exception, ex.getExchange());
        }
        if (exception instanceof IllegalArgumentException) {
            return convert((IllegalArgumentException) exception, ex.getExchange());
        }
        if (exception instanceof com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException) {
            return convert((com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException) exception, ex.getExchange());
        }
        if (exception instanceof SOAPFaultException) {
            return convert((SOAPFaultException) exception, ex.getExchange());
        }

        if (exception instanceof WebServiceException) {
            return convert((WebServiceException) exception, ex.getExchange());
        }

        if (exception instanceof OrderChannelMappingException) {
            return convert((OrderChannelMappingException) exception, ex.getExchange());
        }

        if (exception instanceof IOException) {
            return convert((IOException) exception, ex.getExchange());
        }

        return new Template();
    }
}
