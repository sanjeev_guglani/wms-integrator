package com.delhivery.godam.integrator.logging;

import org.slf4j.Logger;

/**
 * class to wrap logger methods.
 * Created by marutsingh on 4/12/16.
 */
public class InternalLogger {

    private static String getPrefix() {
        return "[Integrator]";
    }

    public static void error(Logger logger, String message) {
//        String msg = String.format("AppName [%s] %s", getPrefix(), message);
        logger.error(message);
    }

    public static void warn(Logger logger, String message) {
//        String msg = String.format("AppName [%s] %s", getPrefix(), message);
        logger.warn(message);
    }

    public static void debug(Logger logger, String message) {
//        String msg = String.format("AppName [%s] %s", getPrefix(), message);
        logger.debug(message);
    }

    public static void info(Logger logger, String message) {
//        String msg = String.format("AppName [%s] %s", getPrefix(), message);
        logger.info(message);
    }

    public static void trace(Logger logger, String message) {
//        String msg = String.format("%s%s", getPrefix(), message);
        logger.trace(message);
    }
}
