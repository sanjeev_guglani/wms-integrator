package com.delhivery.godam.integrator.exceptions.processor.impl;

import com.delhivery.godam.integrator.contracts.crts.TrackerBulkApiRequest;
import com.delhivery.godam.integrator.contracts.crts.TrackingInfo;
import com.delhivery.godam.integrator.exceptions.processor.ExceptionProcessor;
import com.delhivery.godam.integrator.model.Template;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.ERROR_CODE;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.TRACK_REQUEST_LIST_ROUTE;
import static org.apache.cxf.message.Message.ERROR_MESSAGE;

public class RequestTrackerExceptionProcessor implements ExceptionProcessor<TrackerBulkApiRequest>{

    @Override
    public TrackerBulkApiRequest transform(Template emailTemplate) {

        TrackingInfo trackingInfo = TrackingInfo.builder()
                .requestId(emailTemplate.getRequestId())
                .uniqueId(UUID.randomUUID().toString())
                .entity(TrackingInfo.Entity.fromString(emailTemplate.getEntity()))
                .data(emailTemplate.getBody())
                .channel(emailTemplate.getChannel())
                .requestState(TrackingInfo.RequestState.FAILED).build();

        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put(ERROR_CODE, emailTemplate.getSubject());
        errorMap.put(ERROR_MESSAGE, String.format("%s : %s", emailTemplate.getErrorClass(), emailTemplate.getError()));
        trackingInfo.setErrors(Collections.singletonList(errorMap));

        return new TrackerBulkApiRequest(Collections.singletonList(trackingInfo));

    }

    @Override
    public String sendNotification() {
        return TRACK_REQUEST_LIST_ROUTE;
    }
}
