package com.delhivery.godam.integrator.exceptions.processor.impl;

import com.delhivery.godam.integrator.constants.NotificationConstants;
import com.delhivery.godam.integrator.exceptions.processor.ExceptionProcessor;
import com.delhivery.godam.integrator.model.Template;
import com.delhivery.godam.integrator.notification.DsnsRequestPayload;

import java.util.Collections;
import java.util.Objects;

import static com.delhivery.godam.integrator.constants.NotificationConstants.DSNS_V1_NOTIFICATION_PUBLISH_ROUTE;

public class DSNSExceptionProcessor implements ExceptionProcessor<DsnsRequestPayload> {

    @Override
    public DsnsRequestPayload transform(Template template) {

        // TODO add more mapping if required
        DsnsRequestPayload dsnsRequestPayload = new DsnsRequestPayload();
        dsnsRequestPayload.setSubject(template.getSubject());
        dsnsRequestPayload.setChannelName(NotificationConstants.NotificationChannels.EMAIL.getChannel());
        dsnsRequestPayload.setMessage(template.getHtmlMessage());
        if(Objects.nonNull(template.getChannel()))
            dsnsRequestPayload.setTargetDRN(Collections.singletonList(template.getChannel().toLowerCase()));
        dsnsRequestPayload.setRequestId(template.getRequestId());
        dsnsRequestPayload.setExtra(template);

        return dsnsRequestPayload;
    }

    @Override
    public String sendNotification() {
        return DSNS_V1_NOTIFICATION_PUBLISH_ROUTE;
    }
}
