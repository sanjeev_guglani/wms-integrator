package com.delhivery.godam.integrator.utils;

import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.godam.integrator.logging.CamelRouteErrorLog;
import com.delhivery.godam.integrator.logging.InternalLogger;
import net.minidev.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * ExceptionHandler is a Camel Processor, implements Processor interface
 * This will set the exception message and relevant information as exchange body.
 * Created by delhivery on 28/6/16.
 */
@Service
public class OnExceptionHandler implements Processor {

    private static final Logger logger = LoggerFactory.getLogger(OnExceptionHandler.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        Exception exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);


        // get HTTP 4xx 5xx 3xx response body
        CamelRouteErrorLog errorLog;

        if (exception instanceof HttpOperationFailedException) {
            errorLog = CamelRouteErrorLog.getLogMessageOnHttpOperationError(exchange, (HttpOperationFailedException) exception);
        } else if (exception instanceof Validator.ValidationException) {
            errorLog = CamelRouteErrorLog.getLogMessageOnValidationFailure(exchange, (Validator.ValidationException) exception);
        } else {
            errorLog = CamelRouteErrorLog.getErrorLogMessageFromExchange(exchange);
        }

        // set error in response body
        exchange.getOut().setBody(errorLog);

        //log the error as warning since it's handled and we don't want to flag these on sentry as error.
        InternalLogger.warn(logger, errorLog.toString());

        logErrorTrace(errorLog);
    }

    /**
     * //TODO this needs to be removed once properly designed exception layer.
     * @param errorLog camel error route log
     */
        private void logErrorTrace(CamelRouteErrorLog errorLog) {
        if (StringUtils.isNotEmpty(errorLog.getTrace())) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("request_id", errorLog.getRequestId());
            jsonObject.put("error_code", errorLog.getErrorCode());
            jsonObject.put("trace", errorLog.getTrace());
            InternalLogger.warn(logger,jsonObject.toJSONString());
        }
    }
}
