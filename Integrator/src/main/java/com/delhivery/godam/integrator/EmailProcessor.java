package com.delhivery.godam.integrator;

import com.delhivery.godam.integrator.contracts.exceptions.*;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.delhivery.godam.integrator.notification.DsnsRequestPayload;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.http.common.HttpOperationFailedException;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

public interface EmailProcessor {

    public static final String REQUEST_BODY = "request_body";

    /**
     * method to set header in case of order data exception
     * @param exchange camel exchange
     * @param body camel route body
     * @throws Exception Order Data Exception
     */
    void setExceptionHeader(Exchange exchange, @Body OrderDataException body) throws Exception;

    /**
     * method to set header in case of HttpOperationFailedException
     * @param exchange camel exchange
     * @param body camel route body
     * @throws Exception http operation failed exception
     */
    void setExceptionHeader(Exchange exchange, @Body HttpOperationFailedException body) throws Exception;

    /**
     * method to set header in case of ASNDataaexception
     * @param exchange camel exchange
     * @param body camel route body
     * @throws Exception ASNDataException
     */
    void setExceptionHeader(Exchange exchange, @Body ASNDataException body) throws Exception;

    /**
     * method to set header in case of NotificationsDataException
     * @param exchange camel route exchange
     * @param body camel route body
     * @throws Exception Notifications Data Exception
     */
    void setExceptionHeader(Exchange exchange, @Body NotificationsDataException body) throws Exception;

    /**
     * method to set header in case of WareHouseMappingException
     * @param exchange camel route exchange
     * @param body caamel route body
     * @throws Exception WareHouseMappingNotFoundException
     */
    void setExceptionHeader(Exchange exchange, @Body WarehouseMappingNotFoundException body) throws Exception;

    /**
     * method to set header in case of InvalidItemDetailsException
     * @param exchange camel route exchange
     * @param body camel route body
     * @throws Exception Invalid Item Details Exception
     */


    void setExceptionHeader(Exchange exchange, @Body TransferOrderDataException body) throws Exception;

    void setExceptionHeader(Exchange exchange, @Body FCDetailDataException body) throws Exception;



    DsnsRequestPayload<String> createDSNSRequest(@ExchangeProperty(CHANNEL) String channel,
                                                 @ExchangeProperty(ACTIVITY) String activity,
                                                 @ExchangeProperty(REQUEST_ID) String requestId,
                                                 @Body String body) throws JsonProcessingException;
}
