package com.delhivery.godam.integrator.inbound;

import com.delhivery.godam.integrator.contracts.InboundTransformerFactory;
import com.delhivery.godam.integrator.contracts.commons.annotation.GodamTransformer;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahulbeniwal on 17/8/16.
 */
public class InboundTransformerFactoryProvider {

    private Map<String, InboundTransformerFactory> transformerMap = new HashMap<>();

    public void registerFactory(InboundTransformerFactory factory){
        String channel = factory.getClass()
                    .getAnnotation(GodamTransformer.class).channelName()[0];
        if (channel != null){
            transformerMap.put(channel, factory);
        }
    }

    public InboundTransformerFactory getInboundTransformerFactory(String channel)
            throws TransformerNotFoundException {
        if(! transformerMap.containsKey(channel)){
            throw new TransformerNotFoundException(channel);
        }
        return transformerMap.get(channel);
    }

}
