package com.delhivery.godam.integrator.inbound.asn;

import com.delhivery.godam.integrator.contracts.TransformerHelper;
import com.delhivery.godam.integrator.contracts.adaptor.AdapterRequestPayload;
import com.delhivery.godam.integrator.contracts.exceptions.ASNDataException;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.inbound.ASNCreateTransformer;
import com.delhivery.godam.integrator.contracts.inbound.InboundRequest;
import com.delhivery.godam.integrator.contracts.inbound.model.ASNContract;
import com.delhivery.godam.integrator.logging.InternalLogger;
import com.delhivery.godam.integrator.utils.JSONConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.camel.ExchangeProperty;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.CHANNEL;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.ErrorMsgs.INVALID_ASN_PAYLOAD;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.NotificationModel.ASN;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.REQUEST_ID;

/**
 * Created By Sanjeev Guglani
 *
 * @param <C> the type parameter Client ASN
 * @param <G> the type parameter Godam ASN
 */
public interface ASNV2Processor<C,G extends ASNContract> {

    /**;
     * The constant logger.
     */
    Logger logger = LoggerFactory.getLogger(ASNV2Processor.class);


    /**
     * The constant converter.
     */
    JSONConverter<InboundRequest<AdapterRequestPayload>> converter = new JSONConverter<InboundRequest<AdapterRequestPayload>>() {
        @Override
        public TypeReference<InboundRequest<AdapterRequestPayload>> getTypeReference() {
            return new TypeReference<InboundRequest<AdapterRequestPayload>>() {
            };
        }
    };

    /**
     * method return create transformer based on channel name
     *
     * @param channel the channel
     * @return Instanse of client asn create Transformer
     * @throws TransformerNotFoundException if client transformer not defined
     */
    ASNCreateTransformer<C,G> asnCreateTransformer(String channel) throws TransformerNotFoundException;

    /**
     * method to create Inbound standard request defined for integrator.
     *
     * @param request ASN raw payload
     * @return Wrapped payload InboundRequest object
     * @throws ASNDataException the asn data exception
     */
    default InboundRequest<AdapterRequestPayload> setRequest(String request) throws ASNDataException {
        try {
            InboundRequest<AdapterRequestPayload> inboundRequest = converter.convertFrom(request);
            inboundRequest.setChannel((String) inboundRequest.getHeaders().get(CHANNEL));
            if (StringUtils.isBlank(inboundRequest.getRequestId()))
                inboundRequest.setRequestId(UUID.randomUUID().toString());
            return inboundRequest;
        } catch (IOException ex) {
            throw new ASNDataException(ASN.name(), INVALID_ASN_PAYLOAD, request);
        }

    }

    /**
     * Transforms client ASN to ClientASNPOJO
     *
     * @param inboundRequest the inbound request
     * @return transformed list of ASN
     * @throws TransformerNotFoundException if client asn transformer not defined
     * @throws ASNDataException             if incoming data is not parsable or validation fails
     */
    default List<C> transformToClientASN(InboundRequest<AdapterRequestPayload> inboundRequest) throws TransformerNotFoundException, ASNDataException {
        return asnCreateTransformer(inboundRequest.getChannel()).getClientASNs(inboundRequest.getPayload().getPulledObject());
    }

    /**
     * Transform toasn list.
     *
     * @param clientASN the client asn
     * @param channel   the channel
     * @param requestId the request id
     * @return the list
     * @throws TransformerNotFoundException the transformer not found exception
     * @throws ASNDataException             the asn data exception
     */
    default List<G> transformTOASN(List<C> clientASN, @ExchangeProperty(CHANNEL) String channel, @ExchangeProperty(REQUEST_ID) String requestId) throws TransformerNotFoundException, ASNDataException {
        List<G> asns = new ArrayList<>();
        List<TransformerHelper<G>> transformerHelpers = asnCreateTransformer(channel).transform(clientASN);
        transformerHelpers.forEach(transformerHelper -> {
            if (transformerHelper.isValid()) {
                asns.add(transformerHelper.getTransformedObject());
            }
        });

        transformerHelpers.stream().filter(tP -> !tP.isValid()).
                forEach(tP -> InternalLogger.error(logger, String.format("request_id : %s, invalid data error in asn feed, " +
                        "data %s, errors :: %s ", requestId, tP.getData(), tP.getError())));

        if(asns.isEmpty())
            throw new ASNDataException(channel, "validations failed for all asns",clientASN.toString());


        return asns;
    }


}
