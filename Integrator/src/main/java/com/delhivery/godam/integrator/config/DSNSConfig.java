package com.delhivery.godam.integrator.config;

import com.delhivery.godam.integrator.contracts.utils.TextApperanceUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * updated by pradeepverma on 29/7/17.
 * class have all configrations for dsns.
 * use this wherever you need to read client property.
 */
@Component
public class DSNSConfig {

    private Logger logger = LoggerFactory.getLogger(DSNSConfig.class);

    @Value("${endpoint.domain.dsns}")
    private String dsnsHost;

    @Value("${endpoint.api.dsns.publish}")
    private String apiNotificationPublish;

    @Value("${endpoint.domain.dsns.v1}")
    private String dsnsV1Host;

    @Value("${endpoint.api.dsns.publish.v1}")
    private String dsnsV1NotificationPublish;

    @Value("${auth.dsns.token}")
    private String apiToken;

    public String getApiNotificationPublish() {
        return dsnsHost.concat(apiNotificationPublish);
    }

    public String getV1ApiNotificationPublish() {

        return dsnsV1Host.concat(dsnsV1NotificationPublish);
    }

    public String getApiToken() {
        return apiToken;
    }

    @PostConstruct
    public void init() {
        logger.info(toString());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, TextApperanceUtils.getConfigPrintStyle());
    }
}
