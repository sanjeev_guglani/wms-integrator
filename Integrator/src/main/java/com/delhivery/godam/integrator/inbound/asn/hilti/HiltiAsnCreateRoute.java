package com.delhivery.godam.integrator.inbound.asn.hilti;

import com.delhivery.godam.integrator.AppPropertiesConfig;
import com.delhivery.godam.integrator.config.GodamConfig;
import com.delhivery.godam.integrator.config.marketplaces.HiltiConfig;
import com.delhivery.godam.integrator.contracts.exceptions.InvalidUnitOfMeasurementException;
import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;
import com.delhivery.godam.integrator.contracts.inbound.InboundRequest;
import com.delhivery.godam.integrator.exceptions.handler.CommonExceptionHandler;
import com.delhivery.godam.integrator.inbound.asn.ASNV2Processor;
import com.delhivery.godam.integrator.utils.IntegratorRoute;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Activity.CREATE;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Entity.ASN;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel.HILTI;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ASN_V2_ROUTE;
import static com.delhivery.godam.integrator.utils.ExceptionUtil.getCommonExceptionsForExtApi;
import static com.delhivery.godam.integrator.utils.ExceptionUtil.getExceptionListForAsn;
import static com.delhivery.transformer.hilti.ClientConstants.HILTI_ASN_V2_CREATE_ROUTE_ID;


/**
 *  Created By Sanjeev Guglani
 * The type Hilti asn create route.
 */
@Service
@IntegratorRoute(channel = HILTI, entity = ASN, activity = CREATE)
public class HiltiAsnCreateRoute extends RouteBuilder {


    @Autowired
    @Qualifier("hiltiAsnV2Processor")
    private ASNV2Processor asnv2Processor;

    @Autowired
    private GodamConfig godamConfig;

    @Autowired
    private HiltiConfig hiltiConfig;

    @Autowired
    private CommonExceptionHandler commonExceptionHandler;


    @Value("#{'${hilti.integrator.second.version.active.fc}'.split(',')}")
    private Set<String> activeFc;


    /**
     * The constant logger.
     */
    private Logger logger = LoggerFactory.getLogger(HiltiAsnCreateRoute.class);

    private static final String EXCEPTION_ASN_REQUEST = "opa on hold due to missing products";

    @Override
    public void configure() {

        List<Class<? extends Exception>> exceptionList = getExceptionListForAsn();
        exceptionList.addAll(getCommonExceptionsForExtApi());
        exceptionList.add(WarehouseMappingNotFoundException.class);
        exceptionList.add(InvalidUnitOfMeasurementException.class);

        commonExceptionHandler.addCallbackExceptionHandler(this, logger, exceptionList);

    /*        onException(IllegalArgumentException.class)
                    .handled(true)
                    .setProperty(LOGGING_MESSAGE, constant(EXCEPTION_ASN_REQUEST))
                    .toD(WARN_LOG_ENDPOINT)
                    .bean(asnv2Processor, "storeOPAInCacheDB")
                    .bean(exchangeProperty(Exchange.EXCEPTION_CAUGHT))
                    .bean(asnv2Processor, "sendNotification")
                    .to(DSNS_V1_NOTIFICATION_PUBLISH_ROUTE);*/



        from(hiltiConfig.getAsnCreateQueueEndpoint())
                .group(AppPropertiesConfig.RouteGroups.HILTI.name())
                .routeId(HILTI_ASN_V2_CREATE_ROUTE_ID)
                .bean(asnv2Processor, "setRequest")
                .setBody(bodyAs(InboundRequest.class))
                .setProperty(CHANNEL, simple("${body.payload.meta.channel}"))
                .setProperty(REQUEST_ID, simple("${body.requestId}"))
                .setProperty(REQUEST, bodyAs(InboundRequest.class))
                .setProperty(ACTIVITY, constant(ASN.getActivity(CREATE)))

                .bean(asnv2Processor, "transformToClientASN")
                .bean(asnv2Processor, "transformTOASN")

                .split(body())
                    .bean(asnv2Processor, "updateWMSClientDetails")
                    .to(WMS_ASN_V2_ROUTE)
                .end()

         .end();



    }
}
