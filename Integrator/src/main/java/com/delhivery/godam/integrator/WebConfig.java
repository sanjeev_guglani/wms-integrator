package com.delhivery.godam.integrator;

import com.delhivery.godam.integrator.contracts.OMSTransformerFactory;
import com.delhivery.godam.integrator.inbound.InboundTransformerFactoryProvider;
import com.delhivery.godam.integrator.oms.OMSTransformerFactoryProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Delhivery on 2/17/16.
 */
@Configuration
@EnableConfigurationProperties
public class WebConfig {

    @Bean
    AppPropertiesConfig getAppPropertiesConfig() {
        return new AppPropertiesConfig();
    }

    @Bean
    OMSTransformerFactoryProvider omsTransformerFactoryProvider() {
        OMSTransformerFactoryProvider omsTransformerFactoryProvider = new OMSTransformerFactoryProvider();
        List<OMSTransformerFactory> omsTransformerFactories = new ArrayList<>();

        com.delhivery.transformer.hilti.TransformerFactory hiltiTransformerFactory = new com.delhivery.transformer.hilti.TransformerFactory();
        omsTransformerFactories.add(hiltiTransformerFactory);
        omsTransformerFactoryProvider.init(omsTransformerFactories);
        return omsTransformerFactoryProvider;
    }


    @Bean
    InboundTransformerFactoryProvider inboundTransformerFactoryProvider() {
        InboundTransformerFactoryProvider inboundTransformerFactoryProvider = new InboundTransformerFactoryProvider();
        com.delhivery.transformer.hilti.TransformerFactory hiltiTrasnformerFactory = new com.delhivery.transformer.hilti.TransformerFactory();
        inboundTransformerFactoryProvider.registerFactory(hiltiTrasnformerFactory);
        return inboundTransformerFactoryProvider;


    }

}
