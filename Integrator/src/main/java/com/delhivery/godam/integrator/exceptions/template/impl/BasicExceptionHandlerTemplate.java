package com.delhivery.godam.integrator.exceptions.template.impl;

import com.delhivery.godam.integrator.exceptions.template.ExceptionHandlerTemplate;
import com.delhivery.godam.integrator.exceptions.utils.ActionType;
import com.delhivery.godam.integrator.exceptions.utils.Configurator;
import com.delhivery.godam.integrator.model.Template;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 *  created by sachin bhogal
 */
@Service
public class BasicExceptionHandlerTemplate extends ExceptionHandlerTemplate {

    @Autowired
    private CamelContext camelContext;

    @Autowired
    private Configurator actionTypeConfig;

    private ProducerTemplate producerTemplate;

    @PostConstruct
    public void init() {
        producerTemplate = camelContext.createProducerTemplate();
    }

    @Override
    public ActionType getActionType(Template template) {
        return actionTypeConfig.getActionType(template);
    }

    @Override
    public ProducerTemplate producerTemplate() {
        return producerTemplate;
    }
}
