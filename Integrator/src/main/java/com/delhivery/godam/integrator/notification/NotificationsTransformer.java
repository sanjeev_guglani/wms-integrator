package com.delhivery.godam.integrator.notification;

import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;

import java.util.List;

/**
 * contract need to be defined by client notification processors.
 * generic type G stands for Godam Notification format and N stand for client notification format.
 * Created by pradeepverma on 30/3/17.
 */
public interface NotificationsTransformer<N, G extends GodamNotification> {

    /**
     * method to parse incoming request String and make godam notifications from it.
     * @param jsonLoads notification raw payload
     * @return list of Godam notifications
     * @throws NotificationsDataException data error in notification load
     */
    List<G> getNotifications(String jsonLoads) throws NotificationsDataException;

    /**
     * method to transform Godam notification model to client notification model
     * @param payload incoming godam notification
     * @return transformed client notification
     * @throws NotificationsDataException data error in incoming notification
     */
    default N convert(G payload) throws NotificationsDataException{
        return null;
    }

    /**
     * method to transform Godam notification model to client notification model
     * @param payload incoming godam notification
     * @return transformed client notification
     * @throws NotificationsDataException data error in incoming notification
     */
    default N convert(List<G> payload) throws NotificationsDataException {
        return null;
    }

    /**
     * method to update extra fields required while
     * creating notification
     * specially in case of amazon
     * @param notification notification data
     * @param extras extras data
     */
    default <T> N updateExtras(N notification, T extras){
        return notification;
    }

    /**
     * method to validate client notification
     * @param notification client notification pojo
     * @throws Validator.ValidationException if data validations failed.
     */
    void validate(N notification) throws Validator.ValidationException, NotificationsDataException;

}
