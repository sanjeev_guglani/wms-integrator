package com.delhivery.godam.integrator;

import com.delhivery.godam.integrator.constants.NotificationConstants;
import com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants;
import com.delhivery.godam.integrator.contracts.exceptions.*;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.delhivery.godam.integrator.notification.DsnsRequestPayload;
import com.delhivery.godam.integrator.utils.JSONConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;

import static com.delhivery.godam.integrator.constants.ErrorUtils.ErrorCodes.*;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;


/**
 * class to set route header for
 * @author Dewbrat Kumar on 27/11/17
 */
@Service
public class EmailProcessorImpl implements EmailProcessor {

    private JSONConverter<Object> converter = new JSONConverter<Object>() {
        @Override
        public TypeReference<Object> getTypeReference() {
            return new TypeReference<Object>() {
            };
        }
    };

    @Override
    public void setExceptionHeader(Exchange exchange, FCDetailDataException exception) throws Exception {
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, FC_DETAIL_DATA_INVALID, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Config Error"));
        exchange.getIn().setBody(body);
    }

    @Override
    public void setExceptionHeader(Exchange exchange, @Body ASNDataException exception) throws Exception{
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, ASN_DATA_INVALID, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Data Error"));
        exchange.getIn().setBody(body);
    }

    @Override
    public void setExceptionHeader(Exchange exchange, @Body OrderDataException exception) throws Exception{
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, ORDER_DATA_EXCEPTION, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Data Error"));
        exchange.getIn().setBody(body);
    }

    @Override
    public void setExceptionHeader(Exchange exchange, NotificationsDataException exception) throws Exception {
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, NOTIFICATION_DATA_INVALID, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Data Error"));
        exchange.getIn().setBody(body);
    }

    private Object getErrorSubjectHeader(Map<String, Object> headers, String reason) {
        return StringUtils.capitalize(String.format("%s %s %s", headers.get(CHANNEL), headers.get(ACTIVITY), reason));
    }

    @Override
    public void setExceptionHeader(Exchange exchange, @Body HttpOperationFailedException exception) throws Exception{
        Object body = exchange.getProperty(REQUEST_BODY);
        exchange.getIn().setHeader(STATUS_CODE, exception.getStatusCode());
        exchange.getIn().setHeader(REDIRECTION_LOCATION, exception.getRedirectLocation());
        exchange.getIn().setHeader(REQUEST_URI, exception.getUri());
        exchange.getIn().setHeader(STATUS_TEXT, exception.getStatusText());
        exchange.getIn().setHeader(RESPONSE_BODY, exception.getResponseBody());
        exchange.getIn().setHeader(RESPONSE_HEADERS, exception.getResponseHeaders());
        updateExchangeHeader(exchange, HTTP_OPERATION_FAILED, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Http Error"));
        exchange.getIn().setBody(body, String.class);
    }


    @Override
    public void setExceptionHeader(Exchange exchange, WarehouseMappingNotFoundException exception) throws Exception {
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, INVALID_WAREHOUSE, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Config Error"));
        exchange.getIn().setBody(body);
    }


    @Override
    public void setExceptionHeader(Exchange exchange, TransferOrderDataException exception) throws Exception {
        Object body = exchange.getProperty(REQUEST_BODY);
        updateExchangeHeader(exchange, TRANSFER_ORDER_DATA_INVALID, exception.getMessage());
        exchange.getIn().setHeader(EMAIL_SUBJECT, getErrorSubjectHeader(exchange.getIn().getHeaders(), "Data Error"));
        exchange.getIn().setBody(body);
    }



    /**
     * method to update exchange header
     * @param exchange camel route exchange
     * @param errorCode error code
     * @param msg message
     */
    private void updateExchangeHeader(Exchange exchange, String errorCode, String msg){
        exchange.getIn().setHeader(IntegratorConstants.MESSAGE, msg);
        exchange.getIn().setHeader(ERROR, errorCode);
        exchange.getIn().setHeader(REQUEST_ID, exchange.getProperty(REQUEST_ID));
        exchange.getIn().setHeader(ACTIVITY, exchange.getProperty(ACTIVITY));
        exchange.getIn().setHeader(CHANNEL,exchange.getProperty(CHANNEL));
    }

    @Override
    public DsnsRequestPayload<String> createDSNSRequest(@Header(CHANNEL) String channel,
                                                        @Header(EMAIL_SUBJECT) String eamilSubject,
                                                        @Header(REQUEST_ID) String requestId,
                                                        @Body String body) throws JsonProcessingException {
        DsnsRequestPayload<String> dsnsRequestPayload = new DsnsRequestPayload<>();
        DsnsRequestPayload.DsnsRequestMeta meta = new DsnsRequestPayload.DsnsRequestMeta();

        dsnsRequestPayload.setMeta(meta);
        dsnsRequestPayload.setRequestId(requestId);
        dsnsRequestPayload.setTargetDRN(Collections.singletonList(channel.toLowerCase()));
        dsnsRequestPayload.setMessage(body);
        dsnsRequestPayload.setChannelName(NotificationConstants.NotificationChannels.EMAIL.getChannel());
        dsnsRequestPayload.setSubject(eamilSubject);

        return dsnsRequestPayload;
    }
}
