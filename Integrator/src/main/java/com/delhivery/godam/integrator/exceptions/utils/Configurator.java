package com.delhivery.godam.integrator.exceptions.utils;

import com.delhivery.godam.integrator.model.Template;

/**
 *  contract provides configuration which contains mapping of actions with channels
 */
public interface Configurator {

    ActionType getActionType(Template template);

}
