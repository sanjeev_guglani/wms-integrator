package com.delhivery.godam.integrator.notification;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.delhivery.godam.integrator.oms.OrderStatusWrapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.WordUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.delhivery.godam.integrator.contracts.commons.constants.ValidationMessages.NOT_NULL_MESSAGE;

/**
 * Created by rahulbeniwal on 3/8/16.
 */
public class DsnsRequestPayload<J> {

    public static class DsnsRequestMeta {

        private Map<String, String> headers;

        @JsonProperty("query_params")
        private Map<String, String> params;

        @JsonProperty("url_route_params")
        private Map<String, String> urlRouteParams;

        public DsnsRequestMeta(){
            this.headers = new HashMap<>();
            this.params = new HashMap<>();
            this.urlRouteParams = new HashMap<>();
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public void setHeaders(Map<String, String> headers) {
            this.headers = headers;
        }

        public Map<String, String> getParams() {
            return params;
        }

        public void setParams(Map<String, String> params) {
            this.params = params;
        }

        public Map<String, String> getUrlRouteParams() {
            return urlRouteParams;
        }

        public void setUrlRouteParams(Map<String, String> urlRouteParams) {
            this.urlRouteParams = urlRouteParams;
        }
    }

    @JsonProperty("channel_name")
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_NULL_MESSAGE)
    private String channelName;

    @JsonProperty("target_drn")
    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_NULL_MESSAGE)
    private List<String> targetDRN;

    @JsonProperty("consumer_group")
    private String consumerGroup;

    @NotNull(message = NOT_NULL_MESSAGE)
    private J message;

    @NotNull(message = NOT_NULL_MESSAGE)
    @NotEmpty(message = NOT_NULL_MESSAGE)
    private String subject;

    private DsnsRequestMeta meta;

    private Object extra;

    public String getConsumerGroup() {
        return consumerGroup;
    }

    public void setConsumerGroup(String consumerGroup) {
        this.consumerGroup = consumerGroup;
    }

    @JsonProperty("request_id")

    private String requestId;

    public String getChannelName() {
        return channelName;
    }

    public DsnsRequestPayload setChannelName(String channelName) {
        this.channelName = channelName;
        return this;
    }

    public List<String> getTargetDRN() {
        return targetDRN;
    }

    public DsnsRequestPayload setTargetDRN(List<String> targetDRN) {
        this.targetDRN = targetDRN;
        return this;
    }

    public J getMessage() {
        return message;
    }

    @JsonIgnore
    public String getPlainMessage(){
        try {
            return new ObjectMapper().writeValueAsString(getMessage());
        } catch (JsonProcessingException e) {
            return message.toString();
        }
    }
    public DsnsRequestPayload setMessage(J message) {
        this.message = message;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public DsnsRequestPayload setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public DsnsRequestPayload setSubject(NotificationRequest.NotificationType type){
        String subject = String.format(
                "%s %s Notification",
                WordUtils.capitalize(type.getEntity()),
                WordUtils.capitalize(type.getOperation()));
        setSubject(subject);
        return this;
    }

    public DsnsRequestMeta getMeta() {
        return meta;
    }

    public DsnsRequestPayload setMeta(DsnsRequestMeta meta) {
        this.meta = meta;
        return this;
    }

    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public DsnsRequestPayload() {
        this.meta = new DsnsRequestMeta();
    }

    /**
     * method to use where need to create copy of dsns request load with different load type
     * @param payload object from copy required
     * @param newMessage new message load
     * @param <R> R runtime message type
     * @return cloned DSNS request payload
     */
    public static <R> DsnsRequestPayload<R> createCopy(DsnsRequestPayload<OrderStatusWrapper> payload, R newMessage) {
        DsnsRequestPayload<R> clone = new DsnsRequestPayload<>();
        clone.setMeta(payload.getMeta());
        clone.setChannelName(payload.getChannelName());
        clone.setTargetDRN(payload.getTargetDRN());
        clone.setSubject(payload.getSubject());
        clone.setRequestId(payload.getRequestId());
        clone.setExtra(payload.getExtra());
        clone.setMessage(newMessage);
        return clone;
    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }
}
