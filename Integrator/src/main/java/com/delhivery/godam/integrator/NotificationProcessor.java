package com.delhivery.godam.integrator;

import com.delhivery.godam.integrator.constants.RequestStatus;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.godam.integrator.exceptions.NotificationsDataException;
import com.delhivery.godam.integrator.logging.LoggingFormat;
import com.delhivery.godam.integrator.notification.DsnsRequestPayload;
import com.delhivery.godam.integrator.notification.GodamNotification;
import com.delhivery.godam.integrator.notification.NotificationRequest;
import com.delhivery.godam.integrator.notification.NotificationsTransformer;
import org.apache.camel.Body;
import org.apache.camel.ExchangeProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

/**
 * contract to be implemented by all notification processors.
 * Created by pradeepverma on 30/3/17.
 */
public interface NotificationProcessor<T> {

    Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    /**
     * method to get notification transformer from channel and it's notification type.
     *
     * @param channel          client master channel name
     * @param notificationType type of notification basically decided based on entity, operation
     * @return transformer for corresponding channel and notification type
     * @throws TransformerNotFoundException if client transformer not defined
     */
    NotificationsTransformer getNotificationTransformer(String channel, NotificationRequest.NotificationType notificationType) throws TransformerNotFoundException;


    /**
     * method to convert godam notifications received as json into list of Godam Notifications objects.
     *
     * @param request incoming notification request load coming from Godam wrapped inside NotificationRequest Model
     * @return list of godam notification objects
     * @throws NotificationsDataException   invalid data error
     * @throws TransformerNotFoundException if transformer not defined for the client
     */
    default List getNotifications(NotificationRequest<String> request) throws NotificationsDataException, TransformerNotFoundException {

        NotificationsTransformer transformer = getNotificationTransformer(request.getChannel(), request.getNotificationType());
        String jsonLoads = request.getPayload().getPayload();

        return transformer.getNotifications(jsonLoads);
    }

    /**
     *
     * @param action action like PICK, PACK, ALLOCATE
     * @param notificationType type of notification
     * @param loads loads
     * @return list of oms notification loads
     */
    default List<GodamNotification> updateActivity(@ExchangeProperty(ACTION) String action,
                                                   @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType notificationType,
                                                   List<GodamNotification> loads){
        loads.forEach(e -> e.setActivity(action));
        return loads;
    }


    /**
     * method to transform GodamNotifications to Client specific notification model
     *
     * @return client custom notification object
     * @throws TransformerNotFoundException transformer not defined
     * @throws NotificationsDataException   invalid raw data for transformation.
     */
    default T transform(@ExchangeProperty(CHANNEL) String channel,
                        @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType notificationType,
                        @Body GodamNotification notification)
            throws TransformerNotFoundException, NotificationsDataException {

        NotificationsTransformer transformer = getNotificationTransformer(channel, notificationType);
        T clientNotification = (T) transformer.convert(notification);
        return clientNotification;
    }


    /**
     * method to transform List of GodamNotifications to Client specific notification model
     *
     * @return client custom notification object
     * @throws TransformerNotFoundException if client transformer not defined
     * @throws NotificationsDataException   invalid in incoming notification
     */
    default T transform(@ExchangeProperty(CHANNEL) String channel,
                        @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType notificationType,
                        @Body List<GodamNotification> notifications) throws TransformerNotFoundException, NotificationsDataException {
        NotificationsTransformer transformer = getNotificationTransformer(channel, notificationType);
        T clientNotification = (T) transformer.convert(notifications);
        return clientNotification;
    }


    default T updateExtras(@ExchangeProperty(CHANNEL) String channel,
                           @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType type,
                           @Body T notification, Object extras) throws NotificationsDataException, TransformerNotFoundException {
        return (T) getNotificationTransformer(channel, type).updateExtras(notification, extras);
    }

    /**
     * Method to create DSNS Payload with custom client notification as message and other
     * corresponding notification details
     *
     * @param notification client notification load
     * @param type         type of notification
     * @param targetDRN    unique subscriber identifier for DSNS. Generally client store sk or client sk.
     * @param dsnsChannel  DSNS publishing channel
     * @return DSNS request payload
     */
    default DsnsRequestPayload<?> createDSNSRequest(@ExchangeProperty(NOTIFICATION_NAME) String dsnsChannel, @ExchangeProperty(REQUEST_ID) String requestId,
                                                    @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType type,
                                                    @ExchangeProperty(TARGET_DRN) String targetDRN, Object notification) {
        DsnsRequestPayload<Object> dsnsRequest = new DsnsRequestPayload<>();
        dsnsRequest.setMessage(notification)
                .setChannelName(dsnsChannel)
                .setTargetDRN(Collections.singletonList(targetDRN))
                .setMeta(new DsnsRequestPayload.DsnsRequestMeta())
                .setSubject(type);

        dsnsRequest.setRequestId(requestId);
        return dsnsRequest;
    }

    /**
     * method to validate notification fro amazon
     *
     * @param clientNotification consume list of transmission
     * @param channel            consume channel
     * @param notificationType   notification type
     */
    default void validate(T clientNotification, @ExchangeProperty(CHANNEL) String channel,
                          @ExchangeProperty(NOTIFICATION_TYPE) NotificationRequest.NotificationType notificationType)
            throws Validator.ValidationException, NotificationsDataException, TransformerNotFoundException {
        getNotificationTransformer(channel, notificationType).validate(clientNotification);
    }


    /**
     * method to update headers, query params etc meta details in DSNS request
     *
     * @param dsnsRequestPayload DSNS request payload
     * @param metaInfo           additional meta info like headers, query params etc.
     * @return Updated DSNS request payload with meta info
     */
    default DsnsRequestPayload updateDSNSRequestMetaInfo(DsnsRequestPayload<?> dsnsRequestPayload, DsnsRequestPayload.DsnsRequestMeta metaInfo) {
        dsnsRequestPayload.setMeta(metaInfo);
        return dsnsRequestPayload;
    }

    default void logRequest(@Body DsnsRequestPayload dsnsRequestPayload,
                            @ExchangeProperty(NOTIFICATION_NAME) String notificationName, @ExchangeProperty(CHANNEL) String channel) {

        LoggingFormat format = new LoggingFormat.LogBuilder(dsnsRequestPayload.getRequestId(), channel, logger.getName())
                .remarks(String.format("%s-%s", notificationName, "Request"))
                .status(RequestStatus.REQUEST_TRANSFORMED.getStatus())
                .data(dsnsRequestPayload.getPlainMessage())
                .activity(notificationName)
                .build();
        logger.info(format.toString());

    }

    default void logResponse(@Body String response, @ExchangeProperty(REQUEST_ID) String requestId,
                             @ExchangeProperty(NOTIFICATION_NAME) String notificationName, @ExchangeProperty(CHANNEL) String channel) {

        LoggingFormat format = new LoggingFormat.LogBuilder(requestId, channel, logger.getName())
                .remarks(String.format("%s-%s", notificationName, "DsnsResponse"))
                .status(RequestStatus.COMPLETED.getStatus())
                .data(response)
                .activity(notificationName)
                .build();
        logger.info(format.toString());

    }
}
