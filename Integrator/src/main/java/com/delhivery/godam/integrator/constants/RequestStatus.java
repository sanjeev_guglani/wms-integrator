package com.delhivery.godam.integrator.constants;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Created by delhivery on 24/5/16.
 */
public enum RequestStatus {

    UNAUTHENTICATED("Unauthorized", 401),
    FORBIDDEN("Forbidden", 403),
    ACKNOWLEDGED("Acknowledged", 202),
    UNIDENTIFIED("Unidentified Formatter", 406),
    INVALID("Invalid Request Body", 400),
    GODAM_UNAVAILABLE("GODAM Not Found", 503),
    GODAM_TIMEOUT("GODAM Timeout", 504),
    DATASOURCE_UNAVAILABLE("GODAM Datasource Not Found", 503),
    DATASOURCE_TIMEOUT("GODAM Datasource Timeout", 504),
    UNKNOWN_CLIENT("Client not available on GODAM", 501),
    BAD_CONFIGURATION("Bad Configuration for client on GODAM", 417),
    CREATED("Created", 201),
    SUCCESS("Success", 200),
    UNEXPECTED_RESPONSE("Unexpected response format.", 500),

    REQUEST_VALIDATED("Request Validated", 100),
    REQUEST_TRANSFORMED("Request Transformed", 101),
    RESPONSE_VALIDATED("Response Validated", 102),
    RESPONSE_TRANSFORMED("Response Transformed", 103),
    COMPLETED("Completed", 110),
    FAIL("FAIL", 400),
    //SNAPDEAL ORDER CREATE REQUEST TOUCH POINT
    REQUEST_RECEIVED_INTEGRATOR("Request received at Integrator", 105),
    REQUEST_ORDER_SPLITTED("Order Splitted ", 106),
    SPLIT_REQUEST_SENT("Order Split Request sent to client ", 107);

    public static final String INTEGRATION_DEFAULT_ERROR_MSG ="Integration Request Unsuccessful. Please contact tech admin.";


    private String status;
    private Integer statusCode;

    RequestStatus(String status, int statusCode) {
        this.status = status;
        this.statusCode = statusCode;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getStatus() {
        return status;
    }

    public static String getFormattedString(RequestStatus status) {
        if (Objects.nonNull(status)){
            return String.format("status:[%s],code[%s]:",
                    status.getStatus(), status.getStatusCode());
        }
        return StringUtils.EMPTY;
    }
}
