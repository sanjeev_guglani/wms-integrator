package com.delhivery.godam.integrator.logging;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

import java.util.Map;

/**
 * class contains fields
 * required for logging from camel routes
 */
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoggingFormat {

    @JsonProperty("request_id")
    private String requestId;

    private String logger;

    private String channel;

    private String entity;

    private String activity;

    private String status;

    private Map<String, Object> meta;

    private Map<String, Object> error;

    private String remarks;

    private Object data;

    LoggingFormat(LogBuilder builder) {
        this.channel = builder.channel;
        this.requestId = builder.requestId;
        this.logger = builder.logger;
        this.entity = builder.entity;
        this.activity = builder.activity;
        this.data = builder.data;
        this.remarks = builder.remarks;
        this.status = builder.status;
        this.meta = builder.meta;
        this.error = builder.error;


    }

    public String getRequestId() {
        return requestId;
    }

    public String getChannel() {
        return channel;
    }

    public String getEntity() {
        return entity;
    }

    public String getActivity() {
        return activity;
    }

    public String getStatus() {
        return status;
    }

    public Map<String, Object> getError() {
        return error;
    }

    public String getRemarks() {
        return remarks;
    }

    public Object getData() {
        return data;
    }

    public String getLogger() {
        return logger;
    }

    public Map<String, Object> getMeta() {
        return meta;
    }

    @ToString
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class LogBuilder {

        @JsonProperty("request_id")
        private String requestId;

        private String logger;

        private String channel;

        private String entity;

        private String activity;

        private String status;

        private String remarks;

        private Map<String, Object> error;

        private Map<String, Object> meta;

        private Object data;

        public LogBuilder(String requestId, String channel , String logger) {

            this.requestId = requestId;
            this.channel = channel;
            this.logger = logger;
        }

        public LogBuilder(String requestId, String logger) {
            this.requestId = requestId;
            this.logger = logger;
        }

        public LogBuilder logger(String logger) {
            this.logger = logger;
            return this;
        }

        public LogBuilder activity(String activity) {
            this.activity = activity;
            return this;
        }

        public LogBuilder status(String status) {
            this.status = status;
            return this;
        }

        public LogBuilder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        public LogBuilder meta(Map<String, Object> meta) {
            this.meta = meta;
            return this;
        }

        public LogBuilder error(Map<String, Object> error) {
            this.error = error;
            return this;
        }

        public LogBuilder data(Object data) {
            this.data = data;
            return this;
        }

        public LogBuilder entity(String entity) {
            this.entity = entity;
            return this;
        }

        public LoggingFormat build() {

            return new LoggingFormat(this);
        }
    }

}
