package com.delhivery.godam.integrator.notification;

import com.delhivery.godam.integrator.AppPropertiesConfig;
import com.delhivery.godam.integrator.NotificationProcessor;
import com.delhivery.godam.integrator.config.DSNSConfig;
import com.delhivery.godam.integrator.constants.RequestStatus;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.godam.integrator.oms.order.aggregator.ListAggregator;
import com.delhivery.godam.integrator.utils.DirectEndpointMapper;
import com.delhivery.godam.integrator.utils.IntegratorRoute;
import com.delhivery.godam.integrator.utils.OnExceptionHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import static com.delhivery.godam.integrator.constants.NotificationConstants.*;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Entity.NOTIFICATION;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel.DSNS;
import static com.delhivery.godam.integrator.utils.CamelUtils.SimpleExpressions.INFO_LOG_ENDPOINT;
import static com.delhivery.godam.integrator.utils.LogTransitionMessage.REQUEST_TO_DSNS;
import static com.delhivery.godam.integrator.utils.LogTransitionMessage.RESPONSE_FROM_DSNS;


/**
 * This route will check the settings in application properties and
 * will push the all the notifications to queue or hit publish api of DSNS
 * according to it.
 * <p>
 * Created by dishantgupta on 21/9/16.
 * updated by sachinbhogal
 */

@Service
@IntegratorRoute(channel = DSNS, entity = NOTIFICATION)
public class DsnsNotificationRoute extends RouteBuilder implements DirectEndpointMapper {

    @Autowired
    @Qualifier("defaultDsnsNotificationProcessor")
    private NotificationProcessor processor;

    @Autowired
    private OnExceptionHandler onExceptionHandler;

    @Autowired
    private ListAggregator listAggregator;

    @Autowired
    private DSNSConfig dsnsConfig;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(DsnsNotificationRoute.class);

    @Override
    public void configure() throws Exception {

        // formatter to avoid double json dumping
        JacksonDataFormat formatter = new JacksonDataFormat();
        formatter.setUnmarshalType(Object.class);

        onException(Exception.class)
                .handled(true)
                .setProperty(STATUS, constant(RequestStatus.FAIL))
                .process(onExceptionHandler);

        // ignore all transformer not found notifications. don't log also, it's creating too much logs.
        onException(TransformerNotFoundException.class)
                .handled(true);

        onException(Validator.ValidationException.class)
                .handled(true)
                .setProperty(LOGGING_MESSAGE, constant("dsns validation error"))
                .toD(INFO_LOG_ENDPOINT);
        /*
          This Route:
               override a channel name to default
               parse the content into respective notification
               transform the request
               creates DSNS Request payload to publish to DSNS api
               sends exchange to publish the created payload to DSNS Publish route
         */
        from(getRouteEndpoint())
                .noAutoStartup()
                .routeId(DSNS_NOTIFICATION_ROUTE_ID)
                .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())

                .to(VALIDATE_AND_TRANSFORM_NOTIFICATION_ROUTE)
                .bean(processor, "updateDsnsRequestMetaInfo")
                .to(DSNS_V1_NOTIFICATION_PUBLISH_ROUTE)
                .end();


        /*
          This route:
               publishes the message to dsns depending on settings, whether to push in queue or api
         */
        from(DSNS_NOTIFICATION_PUBLISH_ROUTE)
                .noAutoStartup()
                .routeId(DSNS_NOTIFICATION_PUBLISH_ROUTE_ID)
                .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())

                .setHeader(AUTHORIZATION, simple(dsnsConfig.getApiToken()))
                .setHeader(CONTENT_TYPE, constant(APPLICATION_JSON))
                .setHeader(HttpHeaders.ACCEPT, constant(APPLICATION_JSON))
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))

                .bean(processor, "logRequest")
                .marshal().json(JsonLibrary.Jackson)

                .to(dsnsConfig.getApiNotificationPublish())
                .setBody(bodyAs(String.class))

                .bean(processor, "logResponse")

                .unmarshal(formatter)
                .end();


        from(DSNS_V1_NOTIFICATION_PUBLISH_ROUTE)
                .noAutoStartup()
                .routeId(DSNS_V1_NOTIFICATION_PUBLISH_ROUTE_ID)
                .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())

                .bean(method(this, "updateConsumerGroup"))

                .setHeader(CONTENT_TYPE, constant(APPLICATION_JSON))
                .setHeader(HttpHeaders.ACCEPT, constant(APPLICATION_JSON))
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .setHeader(Exchange.HTTP_QUERY,method(this,"getQueryParams"))

                .setProperty(LOGGING_MESSAGE, constant(REQUEST_TO_DSNS.getMessage()))

                .marshal().json(JsonLibrary.Jackson)

                .to(dsnsConfig.getV1ApiNotificationPublish())

                .setBody(bodyAs(String.class))

                .setProperty(LOGGING_MESSAGE, constant(RESPONSE_FROM_DSNS.getMessage()))

                .toD(INFO_LOG_ENDPOINT)

                .unmarshal(formatter)
                .end();
    }


    /**
     * method is temporary used to update dsnsRequestPayload
     * as targetDrn is mapped to consumerGroup in new dsns api
     * @param dsnsRequestPayload dsnsRequestPayload

     * @return updated dsnsRequestPayload
     */
    public DsnsRequestPayload updateConsumerGroup(@Body DsnsRequestPayload dsnsRequestPayload) {
        dsnsRequestPayload.setConsumerGroup((String) dsnsRequestPayload.getTargetDRN().get(0));

        logDsnsV1Request(dsnsRequestPayload);
        return dsnsRequestPayload;
    }

    /**
     * method is temporaly used to log dsns request payload
     * @param dsnsRequestPayload dsnsRequestPayload
     */
    private void logDsnsV1Request(@Body DsnsRequestPayload dsnsRequestPayload){

        try {
            LOGGER.info(MAPPER.writeValueAsString(dsnsRequestPayload));
        } catch (JsonProcessingException e) {
            LOGGER.info(String.format("error while logging dsns v1: %s",e.getMessage()));
        }
    }

    /**
     *  method is temporarly is used to get query param required to hit new dsns api
     * @param dsnsRequestPayload dsnsRequestPayload

     * @return queryParams
     */
    public String getQueryParams(@Body DsnsRequestPayload dsnsRequestPayload){

        return String.format("channel=%s&consumer_group=%s",dsnsRequestPayload.getChannelName(),dsnsRequestPayload.getConsumerGroup());
    }
}
