package com.delhivery.godam.integrator.notification;

import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * base model class for all types of notifications in Godam like Inventory, Order etc
 * Created by pradeepverma on 30/3/17.
 * Updated by Ankit Kumar on 4/4/17
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GodamNotification {

    @JsonProperty("notification_type")
    private NotificationRequest.NotificationType notificationType;

    @JsonProperty("client_warehouse")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long warehouseId;

    @JsonProperty
    private String activity;

    @Override
    public String toString() {
        return Mapper.toString(this);
    }

    public NotificationRequest.NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationRequest.NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    //no arg constructor
    public GodamNotification(){}

    //constructor which takes notification type
    public GodamNotification(NotificationRequest.NotificationType notificationType){
        this.notificationType=notificationType;
    }


}
