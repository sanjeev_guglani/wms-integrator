package com.delhivery.godam.integrator.inbound.asn;

/**
 * Created by Ankit Kumar on 22/3/17.
 */
public class ASNConstants {

    //Inbound ASN Route Constants
    public static final String CREATE_GODAM_ASN_ROUTE = "direct:createGodamAsn";
    public final static String ASN_CREATE_ROUTE_ID = "GodamAsnCreateRoute";

    //Inbound ASN Route Constants
    public static final String WMS_ASN_V2_ROUTE = "direct:createWMSv2Asn";
    public final static String WMS_ASN_V2_ROUTE_ID = "createWMSv2AsnID";

    public static final String WMS_ORDER_V2_ROUTE = "direct:orderCreateWMSV2";
    public final static String WMS_ORDER_V2_ROUTE_ID = "orderCreateWMSV2ID";

    public static final String ASN_REST_CALL_ROUTE_WITH_RETRY = "direct:asnCreateRouteWithRetry";
    public static final String ASN_CREATE_WITH_RETRY_ROUTE_ID = "AsnCreateRouteWithRetry";

    public static final String UPDATE_GODAM_ASN_ROUTE = "direct:updateGodamAsn";
    public final static String ASN_UPDATE_ROUTE_ID = "GodamAsnUpdateRoute";

    public static final String TRANSFORM_TO_ASN_V2_ROUTE = "direct:transformToV2Route";
    public static final String TRANSFORM_TO_ASN_V2_ROUTE_ID = "TransformToAsnV2Route";

    public static final String ASN_APPOINTMENT_ROUTE = "seda:asnBookingRoute";
    public final static String ASN_APPOINTMENT_ROUTE_ID = "AsnBookingRoute";
    public final static String ASN_APPOINTMENT_UPDATE_ROUTE_ID = "AsnBookingUpdateRoute";
    public final static String ASN_APPOINTMENT_CANCEL_ROUTE_ID = "AsnBookingCancelRoute";
    public final static String ASN_APPOINTMENT_RETRIEVE_ROUTE_ID = "AsnBookingRetrieveRoute";

    public static final String ASN_VALIDATE_REQUEST_ROUTE = "direct:validateAsnCreateRequest";
    public final static String ASN_VALIDATE_REQUEST_ROUTE_ID = "ValidateAsnCreateRequestRoute";

    public static final String SEND_ASN_CREATE_REQUEST_ROUTE = "direct:sendAsnCreateRequest";
    public final static String ASN_SEND_CREATE_REQUEST_ROUTE_ID = "SendAsnCreateRequestRoute";

    public static final String CREATE_GODAM_PRODUCT_CHANNEL_ROUTE = "direct:createGodamProductChannel";
    public static final String CREATE_GODAM_PRODUCT_CHANNEL_ROUTE_ID = "CreateGodamProductChannel";
}
