package com.delhivery.godam.integrator.utils;

import com.delhivery.godam.integrator.contracts.utils.GodamUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sachin bhogal on 2/8/17.
 *
 * since channel entity and activity is must while creating route for a client
 * annotation is used to provide meta data for writing camel routes for a client
 * so that we can follow naming convention for routes
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IntegratorRoute {

    GodamUtils.MasterChannel channel();

    GodamUtils.Entity entity() default GodamUtils.Entity.NONE;

    GodamUtils.Activity activity() default GodamUtils.Activity.NONE;
}
