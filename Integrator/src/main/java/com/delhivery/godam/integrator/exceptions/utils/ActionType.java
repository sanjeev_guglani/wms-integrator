package com.delhivery.godam.integrator.exceptions.utils;

import com.delhivery.godam.integrator.exceptions.processor.ExceptionProcessor;
import com.delhivery.godam.integrator.exceptions.processor.impl.DSNSExceptionProcessor;
import com.delhivery.godam.integrator.exceptions.processor.impl.RequestTrackerExceptionProcessor;

/**
 * created by sachin bhogal
 * enum denotes various action that can be performed for exceptions
 *
 */
public enum ActionType {

    DSNS(new DSNSExceptionProcessor()),
    REQUEST_TRACKER(new RequestTrackerExceptionProcessor());

    private ExceptionProcessor processor;

    ActionType(ExceptionProcessor processor) {

        this.processor = processor;
    }


    public ExceptionProcessor getProcessor() {
        return processor;
    }
}

