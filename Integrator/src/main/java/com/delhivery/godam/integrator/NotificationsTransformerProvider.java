package com.delhivery.godam.integrator;

import com.delhivery.godam.integrator.contracts.commons.annotation.GodamNotificationTransformer;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.exceptions.NotificationTransformerNotFoundException;
import com.delhivery.godam.integrator.notification.NotificationRequest;
import com.delhivery.godam.integrator.notification.NotificationsTransformer;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dewbrat Kumar on 24/3/17.
 * Factory class to provide notification transformers.
 */
@Service
public class NotificationsTransformerProvider {

    /**
     * map of notifications transformers based on channel, one channel can have multiple type transformers like Amazon has ASN,IAM,IRN etc transformers.
     */
    private Map<String, Map<String, NotificationsTransformer>> notificationsChannelTranformersMap = new HashMap<>();


    /**
     * method to register channel based notifications transformer factories.
     *
     * @param channel                              godam master channel slug names
     * @param notificationTypeTransformerFactories notification transformer factories
     */
    public void registerFactory(String channel, List<NotificationsTransformer> notificationTypeTransformerFactories) {

        Map<String, NotificationsTransformer> notificationsTypeTransformerMap = new HashMap<>();

        notificationTypeTransformerFactories.forEach(notificationTransformer -> {
            String notificationType = notificationTransformer.getClass().getAnnotation(GodamNotificationTransformer.class).type();
            notificationsTypeTransformerMap.put(notificationType, notificationTransformer);
            notificationsChannelTranformersMap.put(channel, notificationsTypeTransformerMap);
        });
    }

    /**
     * method to get notifications transformer based on channel and it's type.
     *
     * @param client           godam master channel slug name
     * @param notificationType notification type
     * @return notification transformer
     */
    public NotificationsTransformer getNotificationTransformer(String client, NotificationRequest.NotificationType notificationType) throws TransformerNotFoundException {
        String typeFormat = getTransformerKey(notificationType);
        if (!(notificationsChannelTranformersMap.containsKey(client) && notificationsChannelTranformersMap.get(client).containsKey(typeFormat))) {
            throw new NotificationTransformerNotFoundException(client, typeFormat);
        }
        return notificationsChannelTranformersMap.get(client).get(typeFormat);
    }

    /**
     * unique identier for notification transformers
     * @param notificationType object define notification properties like module, entity, operation
     * @return transformer key
     */
    private String getTransformerKey(NotificationRequest.NotificationType notificationType) {
        return String.format("%s-%s-%s", notificationType.getModule().toLowerCase(),
                    notificationType.getEntity().toLowerCase(), notificationType.getOperation().toLowerCase());
    }

}
