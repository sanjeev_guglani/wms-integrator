package com.delhivery.godam.integrator.exceptions.handler;

import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.exceptions.converter.ExceptionConverter;
import com.delhivery.godam.integrator.exceptions.template.ExceptionHandlerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.delhivery.godam.integrator.EmailProcessor.REQUEST_BODY;
import static com.delhivery.godam.integrator.exceptions.utils.ExceptionsUtils.LOGGER;
import static com.delhivery.godam.integrator.exceptions.utils.ExceptionsUtils.LOGGING_ENDPOINT;

/**
 * createdBy sachin bhogal on 31/08/2017
 * class is used to remove duplicated code of exception handling in camel routes
 */

@Component
public class CommonExceptionHandler extends RouteBuilder {


    @Autowired
    private ExceptionHandlerTemplate template;

    @Autowired
    private ExceptionConverter converter;

    /**
     * method will append onExceptionDefinition for given route with provided handled exceptions
     *
     * @param routeBuilder      route for which onExceptionDefinition needs to be defined
     * @param logger            logger of  provided route
     * @param handledExceptions list of handledExceptions
     */

    public void addCallbackExceptionHandler(RouteBuilder routeBuilder, Logger logger,
                                            List<Class<? extends Exception>> handledExceptions) {

        for (Class<? extends Exception> handledException : handledExceptions) {

            routeBuilder.onException(handledException)
                    .handled(true)
                    .setProperty(LOGGER, constant(logger))
                    .setProperty(REQUEST_BODY, body())

                    .toD(LOGGING_ENDPOINT)
                    .bean(exchangeProperty(Exchange.EXCEPTION_CAUGHT))
                    .bean(method(converter, "convert"))
                    .bean(method(template, "handleException"));
        }
        routeBuilder.onException(TransformerNotFoundException.class)
                .handled(true);
    }

    @Override
    public void configure() throws Exception {
        //required by super class
    }
}


