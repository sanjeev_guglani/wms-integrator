package com.delhivery.godam.integrator.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;

/**
 * json converter class to be used across application.
 * @author saurabhsharma
 * @since 24/05/16.
 */
public abstract class JSONConverter<T> implements Converter<T,String> {

    private final ObjectMapper mapper = new ObjectMapper();
    private JavaType javaType;

    public abstract TypeReference<?> getTypeReference();

    protected JSONConverter() {
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        javaType = mapper.getTypeFactory().constructType(getTypeReference());
    }

    public T convertFrom(String jsonString) throws IOException {
        return mapper.readValue(jsonString,javaType);
    }

    public String convertTo(T t) throws IOException {
        StringWriter writer = new StringWriter();
        mapper.writeValue(writer,t);
        return writer.toString();
    }

    public ObjectMapper getMapper() {
        return mapper;
    }
}