package com.delhivery.godam.integrator.notification;

import com.delhivery.godam.integrator.NotificationProcessor;
import com.delhivery.godam.integrator.NotificationsTransformerProvider;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.oms.OrderStatusLoad;
import org.apache.camel.Body;
import org.apache.camel.ExchangeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

/**
 * Created by sachinBhogal on 6/7/17.
 * default implementation class for notification processor
 */
@Service("defaultDsnsNotificationProcessor")
public class DefaultNotificationProcessorImpl implements NotificationProcessor<OrderStatusLoad> {

    @Autowired
    private NotificationsTransformerProvider transformerProvider;

    @Override
    public NotificationsTransformer getNotificationTransformer(String channel, NotificationRequest.NotificationType notificationType) throws TransformerNotFoundException {
        return transformerProvider.getNotificationTransformer(channel, notificationType);
    }

    /**
     * method is used to create default orderShipRequestBody
     *
     * @param payload list of dsns request load wrapped on default order ship load
     * @return dsnsRequestPayload containing orderShipNotificationRequestBody
     */
    public DsnsRequestPayload createOrderShipRequest(@Body DsnsRequestPayload<List<OrderStatusLoad>> payload) {

        DsnsRequestPayload<Map<String, List<OrderStatusLoad>>> orderShipRequest = new DsnsRequestPayload<>();

        orderShipRequest.setRequestId(payload.getRequestId());
        orderShipRequest.setChannelName(payload.getChannelName());
        orderShipRequest.setSubject(payload.getSubject());
        orderShipRequest.setMeta(payload.getMeta());
        orderShipRequest.setTargetDRN(payload.getTargetDRN());
        orderShipRequest.setExtra(payload.getExtra());

        Map<String, List<OrderStatusLoad>> orderShipRequestLoad = new HashMap<>();
        orderShipRequestLoad.put("orderlines", payload.getMessage());
        orderShipRequest.setMessage(orderShipRequestLoad);

        return orderShipRequest;
    }

    public DsnsRequestPayload updateDsnsRequestMetaInfo(@ExchangeProperty(AUTH_TOKEN) String authToken, @Body DsnsRequestPayload payload) {

        payload.getMeta().getHeaders().put(AUTHORIZATION, authToken);
        payload.getMeta().getHeaders().put(CONTENT_TYPE, APPLICATION_JSON);

        return payload;
    }

    /**
     * method is used to override the channel name to default
     *
     * @param notificationRequest notificationRequest from notificationController
     */
    public void setDefaultChannel(@Body NotificationRequest notificationRequest) {

        notificationRequest.setChannel(DEFAULT_CHANNEL_NAME);
    }
}
