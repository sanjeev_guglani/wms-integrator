package com.delhivery.godam.integrator.oms.order.aggregator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.apache.camel.Exchange.EXCEPTION_CAUGHT;
import static org.apache.camel.Exchange.FAILURE_HANDLED;


/**
 * Default aggregator to aggregate the exchange bodies and
 * set the list of bodies as final exchange body
 * Created by Ankit Kumar on 29/5/17.
 */
@Service
public class ListAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        List exchangeBody = oldExchange==null ? new ArrayList() : oldExchange.getIn().getBody(List.class);

        if(newExchange.getProperty(Exchange.EXCEPTION_CAUGHT)!=null) {
            newExchange.removeProperty(EXCEPTION_CAUGHT);
            newExchange.removeProperty(FAILURE_HANDLED);
            newExchange.removeProperty(Exchange.ERRORHANDLER_HANDLED);
        } else {
            List newExchangeBody = newExchange.getIn().getBody(List.class);
            exchangeBody.addAll(newExchangeBody);

        }
        newExchange.getIn().setBody(exchangeBody);
        return newExchange;
    }
}
