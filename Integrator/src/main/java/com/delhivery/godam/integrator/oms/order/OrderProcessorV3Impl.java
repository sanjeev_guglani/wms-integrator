package com.delhivery.godam.integrator.oms.order;

import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.oms.order.OrderCreateTransformer;
import com.delhivery.godam.integrator.oms.OMSTransformerFactoryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Sanjeev Guglani on 2/5/19.
 * This is the default implementation of OrderProcessor API
 */
@Service
public class OrderProcessorV3Impl implements OrderProcessorV3 {

    @Autowired
    private OMSTransformerFactoryProvider omsTransformerFactoryProvider;

    @Override
    public OrderCreateTransformer orderCreateTransformer(String channel) throws TransformerNotFoundException {
        return omsTransformerFactoryProvider
                .getOMSTransformerFactory(channel).getOrderCreateTransformer();
    }
}
