package com.delhivery.godam.integrator.oms.order.create.hilti;

import com.delhivery.godam.integrator.AppPropertiesConfig;
import com.delhivery.godam.integrator.config.GodamConfig;
import com.delhivery.godam.integrator.config.marketplaces.HiltiConfig;
import com.delhivery.godam.integrator.contracts.exceptions.EmptyMessageQueueException;
import com.delhivery.godam.integrator.contracts.exceptions.InvalidUnitOfMeasurementException;
import com.delhivery.godam.integrator.contracts.exceptions.ProductDataException;
import com.delhivery.godam.integrator.contracts.exceptions.WarehouseMappingNotFoundException;
import com.delhivery.godam.integrator.contracts.utils.BigDecimalSerializerStore;
import com.delhivery.godam.integrator.exceptions.handler.CommonExceptionHandler;
import com.delhivery.godam.integrator.oms.order.OrderProcessorV3;
import com.delhivery.godam.integrator.utils.IntegratorRoute;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.HILTI_ORDER_CREATE_ROUTE_ID;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Activity.CREATE;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Entity.ORDERV2;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel.HILTI;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ORDER_V2_ROUTE;
import static com.delhivery.godam.integrator.utils.ExceptionUtil.getCommonExceptionsForExtApi;
import static com.delhivery.godam.integrator.utils.ExceptionUtil.getExceptionListForOrder;

/**
 * The type Hilti order create route.
 */
@Component
@IntegratorRoute(channel = HILTI,entity = ORDERV2,activity = CREATE)
public class HiltiOrderCreateRoute extends RouteBuilder {


    @Autowired
    @Qualifier("orderProcessorV3Impl")
    private OrderProcessorV3 orderProcessorV3;

    @Autowired
    private GodamConfig godamConfig;

    @Autowired
    private HiltiConfig hiltiConfig;

    private JacksonDataFormat formatter;

    @Autowired
    private CommonExceptionHandler commonExceptionHandler;


    @Value("#{'${hilti.integrator.second.version.active.fc}'.split(',')}")
    private Set<String> activeFc;

    /**
     * The constant logger.
     */
    private Logger logger = LoggerFactory.getLogger(HiltiOrderCreateRoute.class);

    /**
     * Init.
     */
    @PostConstruct
    public void init(){
        formatter = new JacksonDataFormat();
        formatter.setObjectMapper(BigDecimalSerializerStore.getBigDecimalSerializerStore().getObjectMapper());
    }

    @Override
    public void configure(){

        List<Class<? extends Exception>> exceptionList = getExceptionListForOrder();
        exceptionList.addAll(getCommonExceptionsForExtApi());
        exceptionList.add(WarehouseMappingNotFoundException.class);
        exceptionList.add(InvalidUnitOfMeasurementException.class);
        exceptionList.add(IllegalArgumentException.class);
        exceptionList.add(EmptyMessageQueueException.class);
        exceptionList.add(ProductDataException.class);

        commonExceptionHandler.addCallbackExceptionHandler(this, logger, exceptionList);


        /*
         *   This route will read new asn request from queue
         *   and navigate it either to order wms1 or order wms-2.0 flow
         */
        from(hiltiConfig.getOrderCreateQueueEndpoint())
                .noAutoStartup()
                .routeId(HILTI_ORDER_CREATE_ROUTE_ID)
                .group(AppPropertiesConfig.RouteGroups.HILTI.name())
                .pipeline(VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE,WMS_ORDER_V2_ROUTE)
        .end();

    }
}
