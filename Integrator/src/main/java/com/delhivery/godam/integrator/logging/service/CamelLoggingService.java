package com.delhivery.godam.integrator.logging.service;

import com.delhivery.godam.integrator.logging.LoggingFormat;
import org.apache.camel.Exchange;

public interface CamelLoggingService {

    LoggingFormat createLogFromExchange(Exchange exchange);
}
