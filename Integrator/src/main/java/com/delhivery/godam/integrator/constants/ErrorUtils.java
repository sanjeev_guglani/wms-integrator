package com.delhivery.godam.integrator.constants;

/**
 * class to holds error codes used in case of any exception and errors
 * @author Dewbrat Kumar on 22/11/17
 */
public class ErrorUtils {

    public interface ErrorCodes{

        String AMZ_INVALID_BUCKET = "AMZ_INVALID_BUCKET";
        String HTTP_OPERATION_FAILED = "HTTP_OPERATION_FAILED";
        String ASN_DATA_INVALID = "ASN_DATA_INVALID";
        String ORDER_DATA_EXCEPTION = "ORDER_DATA_INVALID";
        String NOTIFICATION_DATA_INVALID = "NOTIFICATION_DATA_INVALID";
        String INVALID_WAREHOUSE="WAREHOUSE_INVALID";
        String ITEM_DETAILS_INVALID="ITEM_DETAILS_INVALID";
        String TRANSFER_ORDER_DATA_INVALID = "TRANSFER_ORDER_DATA_INVALID";
        String PRODUCT_RECALL_DATA_INVALID = "PRODUCT_RECALL_DATA_INVALID";
        String FC_DETAIL_DATA_INVALID = "FC_DETAIL_DATA_INVALID";
        String RECALL_INVENTORY_SHORT = "REC-InventoryShort";
        String AMAZON_INVALID_FILE_EXTENSION = "AMZ_INVALID_FILE_EXTENSION";
        String AMZ_INVALID_ACTIVITY= "AMZ_INVALID_ACTIVITY";
    }

}
