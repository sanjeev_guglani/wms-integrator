package com.delhivery.godam.integrator.exceptions.template;

import com.delhivery.godam.integrator.exceptions.processor.ExceptionProcessor;
import com.delhivery.godam.integrator.exceptions.utils.ActionType;
import com.delhivery.godam.integrator.model.Template;
import org.apache.camel.ProducerTemplate;

/**
 * created by sachin bhogal
 * <p>
 * class is used to define contract to handle exception
 */
public abstract class ExceptionHandlerTemplate {


    public void handleException(Template template) {

        ActionType actionType = getActionType(template);

        ExceptionProcessor exceptionProcessor = actionType.getProcessor();

        producerTemplate()
                .sendBody(exceptionProcessor.sendNotification(), exceptionProcessor.transform(template));

    }

    // what action needs to be performed based on action type like send to dsns, or request tracker etc
    public abstract ActionType getActionType(Template template);

    public abstract ProducerTemplate producerTemplate();

}
