package com.delhivery.godam.integrator.oms.order;

import com.delhivery.godam.integrator.contracts.*;
import com.delhivery.godam.integrator.contracts.adaptor.AdapterRequestPayload;
import com.delhivery.godam.integrator.contracts.exceptions.EmptyMessageQueueException;
import com.delhivery.godam.integrator.contracts.exceptions.OrderDataException;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.oms.model.OrderContract;
import com.delhivery.godam.integrator.contracts.oms.model.OrderCreateParameters;
import com.delhivery.godam.integrator.contracts.oms.order.OrderCreateTransformer;
import com.delhivery.godam.integrator.contracts.oms.order.OrderRequest;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.godam.integrator.utils.JSONConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.camel.ExchangeProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.CHANNEL;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.ErrorMsgs.INVALID_ORDER_DATA_PAYLOAD;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.REQUEST_ID;

/**
 * Created by Sanjeev Guglani on 29/4/19.
 * contract for transforming client models to godam OrderV3 model
 * for orderCreate activity
 */
public interface OrderProcessorV3 {

    /**
     * The constant logger.
     */
    Logger logger = LoggerFactory.getLogger(OrderProcessorV3.class);

    /**
     * The constant converter.
     */
    JSONConverter<OrderRequest<AdapterRequestPayload>> converter = new JSONConverter<OrderRequest<AdapterRequestPayload>>() {
        @Override
        public TypeReference<OrderRequest<AdapterRequestPayload>> getTypeReference() {
            return new TypeReference<OrderRequest<AdapterRequestPayload>>() {
            };
        }
    };


    /**
     * Order create transformer order create transformer.
     *
     * @param channel the channel
     * @return the order create transformer
     * @throws TransformerNotFoundException the transformer not found exception
     */
    OrderCreateTransformer orderCreateTransformer(String channel) throws TransformerNotFoundException;


    /**
     * Validate list.
     *
     * @param <C>         the type parameter
     * @param clientOrder the client order
     * @param requestId   the request id
     * @param channel     the channel
     * @return the list
     * @throws TransformerNotFoundException the transformer not found exception
     * @throws OrderDataException           the order data exception
     */
    default <C> List<C> validate(List<C> clientOrder, @ExchangeProperty(REQUEST_ID) String requestId, @ExchangeProperty(CHANNEL) String channel) throws TransformerNotFoundException, OrderDataException, Validator.ValidationException {
        List<TransformerHelper<C>> helpers = orderCreateTransformer(channel.toLowerCase()).validate(clientOrder);

        logErrorsIfAny(helpers, requestId);
        return getValidObjects(helpers, channel, requestId);
    }


    /**
     * method is used to convert string payload to orderRequest entity
     *
     * @param request payload pulled from integrator-fetcher queue
     * @param channel the channel
     * @return orderRequest object
     * @throws OrderDataException when invalid payload received
     */
    default OrderRequest<AdapterRequestPayload> setRequest(String request, @ExchangeProperty(CHANNEL) String channel) throws OrderDataException {
        logger.debug(String.format("incoming order request from queue : {%s}", request));

        try {
            OrderRequest<AdapterRequestPayload> orderRequest = converter.convertFrom(request);
            String requestId = orderRequest.getRequestId();
            orderRequest.setRequestId((requestId == null || requestId.isEmpty()) ? UUID.randomUUID().toString() : requestId);
            return orderRequest;
        } catch (IOException ex) {
            throw new OrderDataException(channel, INVALID_ORDER_DATA_PAYLOAD, request, ex);
        }
    }

    /**
     * Transform to client order list.
     *
     * @param <C>            the type parameter
     * @param inboundRequest the inbound request
     * @param channel        the channel
     * @return the list
     * @throws TransformerNotFoundException the transformer not found exception
     * @throws OrderDataException           the order data exception
     */
    default <C> List<C> transformToClientOrder(OrderRequest<AdapterRequestPayload> inboundRequest, @ExchangeProperty(CHANNEL) String channel) throws TransformerNotFoundException, OrderDataException {
        return orderCreateTransformer(channel).getClientOrders(inboundRequest.getPayload().getPulledObject());
    }

    /**
     * Transform to godam order list.
     *
     * @param <G>         the type parameter
     * @param <C>         the type parameter
     * @param clientOrder the client order
     * @param channel     the channel
     * @return the list
     * @throws TransformerNotFoundException the transformer not found exception
     * @throws OrderDataException           the order data exception
     */
    default <G extends OrderContract,C>   List<G> transformToGodamOrder(List<C> clientOrder,
                                                                        @ExchangeProperty(CHANNEL) String channel) throws TransformerNotFoundException, OrderDataException {
        return orderCreateTransformer(channel.toLowerCase()).toGodamOrder(clientOrder);
    }


    /**
     * Log errors if any.
     *
     * @param <C>       the type parameter
     * @param helpers   the helpers
     * @param requestId the request id
     */
    default <C> void logErrorsIfAny(List<TransformerHelper<C>> helpers, String requestId) {
        List<TransformerHelper<C>> invalidData = helpers.stream().filter(tP -> !tP.isValid()).collect(Collectors.toList());

        invalidData.forEach(helper -> {
            logger.warn(String.format("request_id [%s], client data pre validations check failed. error : %s, data : %s ", requestId, helper.getError(), helper.getData()));
        });
    }


    /**
     * Gets valid objects.
     *
     * @param <C>       the type parameter
     * @param helpers   the helpers
     * @param channel   the channel
     * @param requestId the request id
     * @return the valid objects
     */
    default  <C> List<C> getValidObjects(List<TransformerHelper<C>> helpers, String channel, String requestId) {

        List<C> orders = helpers.stream().filter(TransformerHelper::isValid)
                .map(TransformerHelper::getTransformedObject).collect(Collectors.toList());
        if (orders.isEmpty()) {
            List<TransformerHelper<?>> invalidObjects = TransformerHelper.getInvalidObjects(helpers);
            throw new EmptyMessageQueueException(String.format("request_id : [%s] channel : [%s] Order Validation Failed data : [%s]", requestId, channel, invalidObjects));
        }
        return orders;
    }

    /**
     * Convert to service blob service blob.
     *
     * @param <G>         the type parameter
     * @param godamOrders the godam orders
     * @param channel     the channel
     * @param requestId   the request id
     * @return the service blob
     */
    default <G extends OrderContract> ServiceBlob convertToServiceBlob(List<G> godamOrders, @ExchangeProperty(CHANNEL) String channel, @ExchangeProperty(REQUEST_ID) String requestId){
        OrderCreateParameters orderCreateParameters=new OrderCreateParameters();
        orderCreateParameters.setOrders(godamOrders);
        return ServiceBlob
                .builder()
                .activity(Activity.CREATE)
                .channel(channel)
                .requestId(requestId)
                .serviceName(Entity.OMS)
                .load(AbstractBlob
                        .builder()
                        .orderCreateParameters(orderCreateParameters)
                        .build())
                .build();
    }
}
