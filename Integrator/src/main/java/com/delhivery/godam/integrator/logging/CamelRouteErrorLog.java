package com.delhivery.godam.integrator.logging;

import com.delhivery.godam.integrator.contracts.godam.IntegratorRequest;
import com.delhivery.godam.integrator.contracts.utils.Mapper;
import com.delhivery.godam.integrator.contracts.utils.Utility;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import org.apache.camel.Exchange;
import org.apache.camel.http.common.HttpOperationFailedException;

import javax.validation.constraints.NotNull;
import java.util.Objects;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.REQUEST;

/**
 * Created by pradeep on 15/11/16.
 * class to make all route exceptions to model in one common format. It's main purpose to handle all error logs more efficiently.
 */
public class CamelRouteErrorLog {

    private String requestId;
    private String errorCode;
    private String response;
    private int statusCode;
    private String message;
    private String endpoint;
    private String channel;
    private String failureRouteId;
    private Object payload;
    private String trace;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getStatusCode() {
        return statusCode;
    }

    private void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Object getPayload() {
        return payload;
    }

    private void setPayload(Object request) {
        this.payload = request;
    }

    public String getFailureRouteId() {
        return failureRouteId;
    }

    private void setFailureRouteId(String failureRouteId) {
        this.failureRouteId = failureRouteId;
    }

    public String getTrace() {
        return trace;
    }

    private void setTrace(String trace) {
        this.trace = trace;
    }

    public String getErrorCode() {
        return errorCode;
    }

    private void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * method to create log message on http operation failure.
     *
     * @param exchange camel exchange
     * @param exception http failure exception
     * @return camel error route log pojo
     */
    public static CamelRouteErrorLog getLogMessageOnHttpOperationError(@NotNull Exchange exchange,
                                                                       @NotNull HttpOperationFailedException exception) {
        CamelRouteErrorLog errorLog = new CamelRouteErrorLog();
        errorLog.setEndpoint(exception.getUri());
        errorLog.setErrorCode("INTGTR_HTTP_OPERATION_FAIL");
        errorLog.setStatusCode(exception.getStatusCode());
        errorLog.setMessage(exception.getMessage());
        errorLog.setResponse(exception.getResponseBody());
        errorLog.setFailureRouteId(exchange.getProperty(Exchange.FAILURE_ROUTE_ID, String.class));

        //since we get http request in Exchange body.
        errorLog.setPayload(exchange.getIn().getBody(String.class));
        IntegratorRequest request = exchange.getProperty(REQUEST, IntegratorRequest.class);

        // for logs without intergrator requests
        if (Objects.nonNull(request)) {
            errorLog.setChannel(request.getChannel());
            errorLog.setRequestId(request.getRequestId());
        }
        return errorLog;
    }

    /**
     * method to create log message on unhandled exceptions.
     *
     * @return Camel error route log pojo
     */
    public static CamelRouteErrorLog getErrorLogMessageFromExchange(@NotNull Exchange exchange) {
        CamelRouteErrorLog errorLog = new CamelRouteErrorLog();
        errorLog.setErrorCode("INTGR_UNEXP_ROUTE_ERR");
        Exception exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
        errorLog.setEndpoint(exchange.getProperty(Exchange.FAILURE_ENDPOINT, String.class));
        errorLog.setMessage(exception.getMessage());
        errorLog.setTrace(Utility.getTraceToLog(exception));
        errorLog.setFailureRouteId(exchange.getProperty(Exchange.FAILURE_ROUTE_ID, String.class));
        IntegratorRequest request = exchange.getProperty(REQUEST, IntegratorRequest.class);

        // for logs without intergrator requests
        if (Objects.nonNull(request)) {
            errorLog.setPayload(request.getPayload());
            errorLog.setChannel(request.getChannel());
            errorLog.setRequestId(request.getRequestId());
        }
        return errorLog;
    }

    /**
     * method to create log message on validation failed custion exceptions.
     *
     * @param exchange processing camel exchange
     * @param exception exception occurred
     * @return camel error route log pojo
     */
    public static CamelRouteErrorLog getLogMessageOnValidationFailure(@NotNull Exchange exchange,
                                                                      @NotNull Validator.ValidationException exception) {
        CamelRouteErrorLog logMessage = getErrorLogMessageFromExchange(exchange);
        logMessage.setErrorCode("INTGR_VALIDATION_FAIL");
        logMessage.setMessage(exception.getErrorListAsString());
        return logMessage;
    }

    @Override
    public String toString() {
        return Mapper.toString(this);
    }

}
