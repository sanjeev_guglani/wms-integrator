package com.delhivery.godam.integrator.inbound.asn;


import com.delhivery.godam.integrator.AppPropertiesConfig;
import com.delhivery.godam.integrator.config.GodamConfig;
import com.delhivery.godam.integrator.utils.LogTransitionMessage;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ASN_V2_ROUTE;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ASN_V2_ROUTE_ID;
import static com.delhivery.godam.integrator.utils.CamelUtils.SimpleExpressions.INFO_LOG_ENDPOINT;

/**
 *  Created By Sanjeev Guglani
 * The type Godam asn create route.
 */

@Component
public class GodamASNRoute extends RouteBuilder {

    @Autowired
    private GodamConfig godamConfig;

    @Override
    public void configure() throws Exception {

                from(WMS_ASN_V2_ROUTE)
                    .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())
                    .routeId(WMS_ASN_V2_ROUTE_ID)
                    .setHeader(CONTENT_TYPE, constant(APPLICATION_JSON))
                    .setHeader(USER_ID,simple(godamConfig.getIntegratorUuid()))
                    .setHeader(REQUEST_ID_V2,exchangeProperty(REQUEST_ID))

                    .marshal().json(JsonLibrary.Jackson)
                    .setProperty(LOGGING_MESSAGE, constant(LogTransitionMessage.REQUEST_TO_GODAM.getMessage()))
                    .toD(INFO_LOG_ENDPOINT)
                    .to(godamConfig.getApiWMSV2AsnCreate())

                    .setBody(bodyAs(String.class))
                    .setProperty(LOGGING_MESSAGE, constant(LogTransitionMessage.RESPONSE_GODAM.getMessage()))
                    .toD(INFO_LOG_ENDPOINT)
                .end();
    }
}
