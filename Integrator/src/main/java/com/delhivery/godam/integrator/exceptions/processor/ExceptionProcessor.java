package com.delhivery.godam.integrator.exceptions.processor;

import com.delhivery.godam.integrator.model.Template;

/**
 * contract for transforming template to action type i.e send to dsns or request tracker
 *
 * @param <T> transfomed object
 */
public interface ExceptionProcessor<T> {

    T transform(Template emailTemplate);

    String sendNotification();

}
