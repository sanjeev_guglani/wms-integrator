package com.delhivery.godam.integrator.exceptions;

import com.delhivery.godam.integrator.contracts.exceptions.DataException;
import com.delhivery.godam.integrator.contracts.exceptions.ErrorMessage;

/**
 * class to wrap all errors in incoming notifications requests formats.
 * Created by pradeepverma on 1/5/17.
 */
public class NotificationsDataException extends DataException {

    private String client;
    private String data;
    private String type;

    public NotificationsDataException(String client, String type, String message, String data)
    {
        super(message);
        this.client = client;
        this.type = type;
        this.data = data;
    }

    public NotificationsDataException(String client, String type, String message, String data, Exception e) {
        super(message, e);
        this.client = client;
        this.type = type;
        this.data = data;
    }

    public NotificationsDataException(String client, ErrorMessage errorMessage, String activity, Object data, String referenceNo) {
        super(errorMessage.getMessage(client, activity));
        this.client = client;
        this.activity = activity;
        this.errorMessage = errorMessage;
        this.referenceNo = referenceNo;
    }

    public String getType() {
        return this.type;
    }

    public String getRawData() {
        return this.data;
    }

    @Override
    public String getMessage() {
        return String.format("client [%s], type :: [%s], error [%s] , data :: %s",
                client, type, super.getMessage(), data);
    }
}
