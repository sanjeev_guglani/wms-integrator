package com.delhivery.godam.integrator.utils;

import java.io.IOException;

/**
 * @author saurabhsharma
 * @since 30/06/16
 */
public interface Converter<OUT,IN> {

    OUT convertFrom(IN in) throws IOException;
    IN convertTo(OUT out) throws IOException;

}
