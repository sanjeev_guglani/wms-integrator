package com.delhivery.godam.integrator;

import com.delhivery.godam.db.config.DbConfiguration;
import org.apache.cxf.spring.boot.autoconfigure.CxfAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = CxfAutoConfiguration.class)
@EnableTransactionManagement
@EnableAsync
@Import(DbConfiguration.class)
public class IntegratorApp {

    public static void main(String[] args) {
        SpringApplication.run(IntegratorApp.class, args);
    }

}