package com.delhivery.godam.integrator.notification;


import com.delhivery.godam.integrator.contracts.godam.IntegratorRequest;
import com.delhivery.godam.integrator.contracts.godam.PayloadWrapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.http.HttpHeaders;

/**
 * Notificaiton request extends Integrator request to add some notificaiton flow
 * specific information to request object
 * <p>
 * T : List<OMSNotificaitonLoad></> etc
 * <p>
 * Created by rahulbeniwal on 4/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationRequest<T> extends IntegratorRequest<PayloadWrapper<T>> {
    // contains module, entity and operation attrs to determine type of notification request
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class NotificationType {

        @JsonProperty("module")
        private String module;

        @JsonProperty("entity")
        private String entity;

        @JsonProperty("operation")
        private String operation;

        public String getModule() {
            return module;
        }

        public void setModule(String module) {
            this.module = module;
        }

        public String getEntity() {
            return entity;
        }

        public void setEntity(String entity) {
            this.entity = entity;
        }

        public String getOperation() {
            return operation;
        }

        public void setOperation(String operation) {
            this.operation = operation;
        }

        @Override
        public boolean equals(Object object) {
            if (object == null || object.getClass() != this.getClass()) {
                return false;
            }
            NotificationType notificationType = (NotificationType) object;
            return this.module.equals(notificationType.module) &&
                    this.entity.equals(notificationType.entity) &&
                    this.operation.equals(notificationType.operation);
        }

        //no-arg constructor
        public NotificationType() {
        }


        public NotificationType(String module, String entity, String operation) {
            this.entity = entity;
            this.module = module;
            this.operation = operation;
        }

        @Override
        public String toString() {
            return String.format("[%s-%s-%s]", this.getEntity().toUpperCase(), this.getEntity().toUpperCase(), this.getOperation().toUpperCase());
        }

        @JsonIgnore
        public String getNotificationTypeName() {
            return String.format("%s-%s-%s", this.getModule().toLowerCase(), this.getEntity().toLowerCase(), this.getOperation().toLowerCase());
        }
    }

    @JsonProperty("notification_type")
    private NotificationType notificationType;

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public NotificationRequest setNotificationType(String module, String entity, String operation) {
        setNotificationType(new NotificationType(module, entity, operation));
        return this;
    }

    /**
     * static method to create NotificationRequest
     *
     * @param requestId      unique request id of request
     * @param headers        http headers for request
     * @param payloadWrapper pay load for notification request
     * @return NotificationRequest
     */

    public static NotificationRequest<String> createNotificationRequest(
            String requestId, HttpHeaders headers,
            PayloadWrapper<String> payloadWrapper) throws JsonProcessingException {

        // set all parameters in Notification request
        NotificationRequest<String> notificationRequest = new NotificationRequest<>();
        notificationRequest.setRequestId(requestId);
        notificationRequest.setHeaders(headers);
        notificationRequest.setChannel(payloadWrapper.getChannel());

        // set payload to string as unable to convert it automatically in controller args
        payloadWrapper.setPayload(new ObjectMapper().writeValueAsString(payloadWrapper.getPayload()));
        notificationRequest.setPayload(payloadWrapper);
        return notificationRequest;
    }

    /**
     * method used to create notification request from route
     * instead of controller
     * @param requestId request id used to process request
     * @param payloadWrapper payload used to create notification
     * @return returns notification request
     * @throws JsonProcessingException it can throw exception while writing payload as string
     */
    public static NotificationRequest<String> createNotificationRequest(
            String requestId, PayloadWrapper<String> payloadWrapper) throws JsonProcessingException {

        // set all parameters in Notification request
        NotificationRequest<String> notificationRequest = new NotificationRequest<>();
        notificationRequest.setRequestId(requestId);
        notificationRequest.setChannel(payloadWrapper.getChannel());

        // set payload to string as unable to convert it automatically in controller args
        payloadWrapper.setPayload(new ObjectMapper().writeValueAsString(payloadWrapper.getPayload()));
        notificationRequest.setPayload(payloadWrapper);
        return notificationRequest;
    }

    /**
     * static method to create NotificationRequest
     *
     * @param requestId      unique request id of request
     * @param payloadWrapper pay load for notification request
     * @return NotificationRequest
     */

    public static NotificationRequest<String> createOMSNotificationRequest(
            String requestId,
            PayloadWrapper<String> payloadWrapper) throws JsonProcessingException {

        // set all parameters in Notification request
        NotificationRequest<String> notificationRequest = new NotificationRequest<>();
        notificationRequest.setRequestId(requestId);
        notificationRequest.setChannel(payloadWrapper.getChannel());

        // set payload to string as unable to convert it automatically in controller args
        payloadWrapper.setPayload(new ObjectMapper().writeValueAsString(payloadWrapper.getPayload()));
        notificationRequest.setPayload(payloadWrapper);
        return notificationRequest;
    }

    /**
     * will return notification name for which the request belongs (entity + operation)
     *
     * @return notificationName
     */
    public String getNotificationName() {
        //regex to split alpha numeric string
        String regex = "[^a-zA-Z0-9]+";
        String[] entities = getNotificationType().getEntity().split(regex);
        String[] operations = getNotificationType().getOperation().split(regex);

        return String.format("%s%s",
                getCaptalizeString(entities),
                getCaptalizeString(operations));
    }

    private String getCaptalizeString(String[] array) {
        StringBuilder result = new StringBuilder();
        for (String str : array) {
            result.append(WordUtils.capitalize(str));
        }
        return result.toString();
    }

    //no-arg constructor
    public NotificationRequest() {
    }

    //copy constructor
    public NotificationRequest(NotificationRequest request) {
        this.notificationType = request.getNotificationType();
        this.setRequestId(request.getRequestId());
        this.setChannel(request.getChannel());
        this.setPayload((PayloadWrapper) request.getPayload());
        this.setHeaders(request.getHeaders());
    }

}
