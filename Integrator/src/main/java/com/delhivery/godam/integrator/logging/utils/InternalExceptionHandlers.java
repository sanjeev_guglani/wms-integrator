package com.delhivery.godam.integrator.logging.utils;

import org.apache.camel.http.common.HttpOperationFailedException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;

/**
 * created by sachin bhogal
 * <p>
 * class contains method required to log exception's data
 */
@Service
public class InternalExceptionHandlers {

    /*
        for httpOperationFailedException
     */
    public Map<String, Object> getErrorLog(HttpOperationFailedException exception) {
        Map<String, Object> errors = new HashMap<>();

        errors.put(STATUS_CODE, exception.getStatusCode());
        errors.put(MESSAGE, exception.getMessage());
        errors.put(RESPONSE_BODY, exception.getResponseBody());
        errors.put(REQUEST_URI, exception.getUri());
        errors.put(RESPONSE_HEADERS, exception.getResponseHeaders());

        return errors;
    }


    /*
        for blanket exception
     */
    public Map<String, Object> getErrorLog(Exception exception) {

        Map<String, Object> errors = new HashMap<>();

        //errors.put("error_code", CustomExceptionHandlers.getErrorCode(exception));
        errors.put(MESSAGE, exception.getMessage());
        errors.put(EXCEPTION_CAUSE , exception.getCause());

        return errors;
    }


}
