package com.delhivery.godam.integrator.oms.order.create;


import com.delhivery.godam.integrator.AppPropertiesConfig;
import com.delhivery.godam.integrator.config.GodamConfig;
import com.delhivery.godam.integrator.contracts.oms.order.OrderRequest;
import com.delhivery.godam.integrator.oms.order.OrderProcessorV3;
import com.delhivery.godam.integrator.utils.LogTransitionMessage;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.*;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Activity.CREATE;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.Entity.ORDER;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ORDER_V2_ROUTE;
import static com.delhivery.godam.integrator.inbound.asn.ASNConstants.WMS_ORDER_V2_ROUTE_ID;
import static com.delhivery.godam.integrator.utils.CamelUtils.SimpleExpressions.INFO_LOG_ENDPOINT;

/**
 *  Created By Sanjeev Guglani
 * The type Godam Order create route.
 */

@Component
public class GodamOrderRoute extends RouteBuilder {

    @Autowired
    private GodamConfig godamConfig;

    @Autowired
    @Qualifier("orderProcessorV3Impl")
    private OrderProcessorV3 orderProcessorV3;

    @Override
    public void configure() throws Exception {



        /*
         * route will parse incoming request body into OrderRequest object
         * generate request_id for incoming request and set in OrderRequest object
         * parse client's order data(string) into list of objects of that client
         * then convert client objects into Order of WMS-2.0 object one at a time and
         * aggregate WMS-2.0 objects
         */
        from(VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE)
                .noAutoStartup()
                .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())
                .routeId(VALIDATE_AND_TRANSFORM_WMS_V2_ROUTE_ID)
                .errorHandler(noErrorHandler())
                .bean(orderProcessorV3, "setRequest")
                .setBody(bodyAs(OrderRequest.class))

                .setProperty(REQUEST, body())
                .setProperty(REQUEST_ID, simple("${body.getRequestId}"))
                .setProperty(CHANNEL, simple("${body.getHeaders['channel']}"))
                .setProperty(CLIENT_ORDERS ,simple("${body.payload.pulledObject}"))
                .setProperty(ACTIVITY, simple(ORDER.getActivity(CREATE)))
                .setProperty(LOGGING_MESSAGE, constant(LogTransitionMessage.REQUEST_FROM_CLIENT.getMessage()))
                .toD(INFO_LOG_ENDPOINT)

                .bean(orderProcessorV3, "transformToClientOrder")
                .bean(orderProcessorV3,"validate")
                .bean(orderProcessorV3, "transformToGodamOrder")
        .end();




        /*
         *   Generic Route For Godam  V2 Order Create
         */
        from(WMS_ORDER_V2_ROUTE)
                .group(AppPropertiesConfig.RouteGroups.DEFAULT.name())
                .routeId(WMS_ORDER_V2_ROUTE_ID)
                .setHeader(CONTENT_TYPE, constant(APPLICATION_JSON))
                .setHeader(REQUEST_ID_V2,exchangeProperty(REQUEST_ID))

                .bean(orderProcessorV3,"convertToServiceBlob")
                .marshal().json(JsonLibrary.Jackson)

                .setProperty(LOGGING_MESSAGE, constant(LogTransitionMessage.REQUEST_TO_GODAM.getMessage()))
                .toD(INFO_LOG_ENDPOINT)
                .to(godamConfig.getApiWMSV2OrderCreate())

                .setBody(bodyAs(String.class))
                .setProperty(LOGGING_MESSAGE, constant(LogTransitionMessage.RESPONSE_GODAM.getMessage()))

                .toD(INFO_LOG_ENDPOINT)
         .end();
    }
}
