package com.delhivery.godam.integrator.inbound.asn.hilti;

import com.delhivery.godam.integrator.config.marketplaces.HiltiConfig;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.inbound.ASNCreateTransformer;
import com.delhivery.godam.integrator.contracts.inbound.model.ASN;
import com.delhivery.godam.integrator.inbound.InboundTransformerFactoryProvider;
import com.delhivery.godam.integrator.inbound.asn.ASNV2Processor;
import com.delhivery.transformer.hilti.model.DeliveryDespatchAdviceNotification;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

/**
 * Hilti ASNV2 Processor
 * Created By Sanjeev Guglani
 */
@Service
@Slf4j
public class HiltiAsnV2Processor implements ASNV2Processor<DeliveryDespatchAdviceNotification, ASN> {


    @Autowired
    private HiltiConfig hiltiConfig;


    @Autowired
    private InboundTransformerFactoryProvider inboundTransformerFactoryProvider;

    @Override
    public ASNCreateTransformer<DeliveryDespatchAdviceNotification, ASN> asnCreateTransformer(String channel) throws TransformerNotFoundException {
        return inboundTransformerFactoryProvider.getInboundTransformerFactory(channel).getASNV2Transformer();
    }

    /**
     * method is used to update client key and fc number
     *
     * @param transformedAsn asn
     * @return updated asn
     */
    public ASN updateWMSClientDetails(ASN transformedAsn) {
        transformedAsn.setClientUuid(hiltiConfig.getClientKeyV2());
        return transformedAsn;
    }





}
