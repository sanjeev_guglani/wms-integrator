<if(headers.channel)>Channel: <headers.channel>,<\n>
<endif>
<if(headers.activity)>Activity: <headers.activity>,<\n>
<endif>
<if(headers.request_id)>Tracking Id: <headers.request_id>,<\n>
<endif>
<if(headers.error)>Error Code: <headers.error>,<\n>
<endif>
<if(headers.statusCode)>Status Code: <headers.statusCode>,<\n>
<endif>
<if(headers.redirectionLocation)>Redirect URL: <headers.redirectionLocation>,<\n>
<endif>
<if(headers.uri)>URI: <headers.uri>,<\n>
<endif>
<if(headers.statusText)>Status Text: <headers.statusText>,<\n>
<endif>
<if(headers.responseBody)>Response Body: <headers.responseBody>,<\n>
<endif>
<if(headers.responseHeaders)>Response Headers: <headers.responseHeaders>,<\n>
<endif>
<if(headers.message)>Message: <headers.message>,<\n>
<endif>
<if(body)>Request Body: <body>
<endif>