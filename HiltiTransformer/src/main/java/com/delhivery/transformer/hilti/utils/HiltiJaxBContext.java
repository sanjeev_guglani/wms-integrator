package com.delhivery.transformer.hilti.utils;

import org.springframework.util.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *  Created By Sanjeev Guglani
 * The type Hilti jax b context.
 */
public class HiltiJaxBContext {

    private static final String CONTEXT_PATH = "com.delhivery.transformer.hilti.model";

    private static Unmarshaller unmarshaller;

    private static Marshaller marshaller;

    static {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CONTEXT_PATH);
            unmarshaller = jaxbContext.createUnmarshaller();
            marshaller = jaxbContext.createMarshaller();
        } catch (JAXBException ex) {
            throw new RuntimeException(String.format("Unable to create JaxBContext: HILTI error:[%s]", ex.getMessage()));
        }
    }

    /**
     * return singleton instance of unmarshaller
     *
     * @return the unmarshaller
     */
    public static Unmarshaller getUnmarshaller() {
        Assert.notNull(unmarshaller, "JaxB Unmarshaller is not initialized for hilti");
        return unmarshaller;
    }

    /**
     * return singleton instance of marsheller
     *
     * @return the marsheller
     */
    public static Marshaller getMarsheller() {
        Assert.notNull(marshaller, "JaxB marsheller is not initialized for hilti");
        return marshaller;
    }
}
