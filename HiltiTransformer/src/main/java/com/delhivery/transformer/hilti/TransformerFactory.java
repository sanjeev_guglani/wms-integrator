package com.delhivery.transformer.hilti;

import com.delhivery.godam.integrator.contracts.InboundTransformerFactory;
import com.delhivery.godam.integrator.contracts.OMSTransformerFactory;
import com.delhivery.godam.integrator.contracts.commons.annotation.GodamTransformer;
import com.delhivery.godam.integrator.contracts.exceptions.TransformerNotFoundException;
import com.delhivery.godam.integrator.contracts.inbound.ASNCreateTransformer;
import com.delhivery.godam.integrator.contracts.oms.order.OrderCreateTransformer;
import com.delhivery.transformer.hilti.transfomers.HiltiASNCreateTransformer;
import com.delhivery.transformer.hilti.transfomers.HiltiOrderCreateTransformer;

import static com.delhivery.transformer.hilti.ClientConstants.HILTI_CHANNEL;

/**
 * Created by sachin bhogal on 11/09/17.
 * All Diffrent Transformed for differect tasks such as Pack , create etc. are initialized here.
 */
@GodamTransformer(channelName = HILTI_CHANNEL)
public class TransformerFactory implements InboundTransformerFactory, OMSTransformerFactory {


    @Override
    public ASNCreateTransformer getASNV2Transformer() {
        return new HiltiASNCreateTransformer();
    }

    @Override
    public OrderCreateTransformer getOrderCreateTransformer() throws TransformerNotFoundException {
        return new HiltiOrderCreateTransformer();
    }


}
