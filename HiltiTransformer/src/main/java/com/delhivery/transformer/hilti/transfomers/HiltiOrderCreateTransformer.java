package com.delhivery.transformer.hilti.transfomers;

import com.delhivery.godam.db.entity.B2BStore;
import com.delhivery.godam.db.utils.ClientDetailsUtils;
import com.delhivery.godam.db.utils.WarehouseUtils;
import com.delhivery.godam.integrator.contracts.exceptions.OrderDataException;
import com.delhivery.godam.integrator.contracts.oms.order.OrderCreateTransformer;
import com.delhivery.godam.integrator.contracts.oms.model.*;
import com.delhivery.godam.integrator.contracts.utils.GodamUtils;
import com.delhivery.godam.integrator.contracts.utils.SoapUtils;
import com.delhivery.godam.integrator.contracts.utils.Utility;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.transformer.hilti.HiltiConstants;
import com.delhivery.transformer.hilti.model.OutboundDeliveryExecutionRequest;
import com.delhivery.transformer.hilti.utils.HiltiJaxBContext;
import com.delhivery.transformer.hilti.validations.CustomerOrderGroup;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.groups.Default;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.COUNTRY_INDIA;
import static com.delhivery.godam.integrator.contracts.commons.constants.IntegratorConstants.DEFAULT_WORKFLOW;
import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel.HILTI;

/**
 * The type Hilti order create transformer.
 * This Transformer will convert hilti request to WMS-2.0 Order Request
 */
public class HiltiOrderCreateTransformer implements OrderCreateTransformer<OutboundDeliveryExecutionRequest, Order> {

    private static final Logger logger = LoggerFactory.getLogger(HiltiOrderCreateTransformer.class);

    /*
     * This method will validate client order
     */
    @Override
    public void validate(OutboundDeliveryExecutionRequest order) throws  Validator.ValidationException {
        Validator.validate(order, Default.class);
        if (HiltiConstants.CUSTOMER_ORDER_TYPE_CODE.equals(order.getOutboundDeliveryExecution().getTypeCode())) {
            Validator.validate(order, CustomerOrderGroup.class);
        }
    }

    /*
     * This method will convert hilti order create to Godam Request
     */
    @Override
    public List<Order>  toGodamOrder(List<OutboundDeliveryExecutionRequest> hiltiOrders) {
        return hiltiOrders
                .stream()
                .map(hiltiOrder->{
                    Order order = getOrder(hiltiOrder);
                    order.setOrderNumber(hiltiOrder.getOutboundDeliveryExecution().getID());
                    order.setOrderDate(Utility.getGodamFormatZonedDateTime(
                            hiltiOrder.getMessageHeader().getCreationDateTime().toString()).toString());
                    order.setOrderType(OrderType.FWD.getOrderType());
                    order.setChannel(HILTI.getSlugName());
                    return order;
                }).collect(Collectors.toList());
    }

    /*
     *  Get Order from Hilti Order
     */
    private Order getOrder(OutboundDeliveryExecutionRequest hiltiOrder){
        if (HiltiConstants.CUSTOMER_ORDER_TYPE_CODE.equals(hiltiOrder.getOutboundDeliveryExecution().getTypeCode())) {
            return getCustomerOrder(hiltiOrder);
        } else {
            return getStockOrder(hiltiOrder);
        }
    }


    /*
     * Get Shipment Details for Customer
     */
    private List<Shipment> getShippmentsForCustomer(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution outboundDeliveryExecution){

        List<Shipment> shipments = new ArrayList<>();
        outboundDeliveryExecution
                .getItems()
                .stream()
                .collect(Collectors.groupingBy(item->item.getID()))
                .forEach((k,v)->{
                    Shipment shipment = getShipmentDetails(k,v,outboundDeliveryExecution);
                    if (HiltiConstants.SHIPPING_INSTRUCTION_FIELD.equalsIgnoreCase(outboundDeliveryExecution.getShippingInstruction())) {
                        shipment.setChildCourier(HiltiConstants.SHIPPING_INSTRUCTION_VALUE);
                    }
                    shipments.add(shipment);
                });
        return shipments;
    }

    /*
     * Method to get fcname from warehouseId
     */
    private String getFcName(String warehouseId) {
        return WarehouseUtils.getFulfillmentCenterName(GodamUtils.MasterChannel.HILTI, warehouseId);
    }


    /*
     * Get Common OrderLines for Customer and Stock Order
     */
    private List<OrderLine> getOrderLines(List<OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.Item> items){

        return items
                .stream()
                .filter(item -> !isMotherItem(item))
                .map(HiltiOrderCreateTransformer::getOrderLine)
                .collect(Collectors.toList());
    }

    /*
     * Get OrderLine from hilti request
     */

    private static OrderLine getOrderLine(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.Item item){
        OrderLine orderLine= new OrderLine();
        orderLine.setProductSku(item.getProduct().getInternalID());
        orderLine.setNumber(item.getProduct().getInternalID());
        orderLine.setQuantity(new Float(item.getDeliveryQuantity().getValue()).intValue());
        orderLine.setClientId(ClientDetailsUtils.getClientUuid(GodamUtils.MasterChannel.HILTI));
        return orderLine;
    }

    /*
     * Get Stock Orders
     */
    private Order getStockOrder(OutboundDeliveryExecutionRequest hiltiOrder) {
        Order order = new Order();
        Consignee consignee = getConsigneeForStockOrder(hiltiOrder.getOutboundDeliveryExecution());
        order.setConsignee(consignee);
        order.setShipments(getShippmentsForStockOrder(hiltiOrder.getOutboundDeliveryExecution()));
        return order;
    }

    /*
     * Get Shipments from Stock Orders
     */
    private List<Shipment> getShippmentsForStockOrder(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution outboundDeliveryExecution) {
        List<Shipment> shipments = new ArrayList<>();

        outboundDeliveryExecution
                .getItems()
                .stream()
                .collect(Collectors.groupingBy(item -> item.getID()))
                .forEach((k, v) -> {
                    Shipment shipment=getShipmentDetails(k,v,outboundDeliveryExecution);
                    shipment.setChildCourier(HiltiConstants.STOCK_ORDER_COURIER_NAME);
                    shipments.add(shipment);
                });
        return shipments;
    }

    /*
     * get common Shipment Details for Stock and Customer Order
     */
    private Shipment getShipmentDetails(String key,List<OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.Item> items, OutboundDeliveryExecutionRequest.OutboundDeliveryExecution outboundDeliveryExecution){
        Shipment shipment = new Shipment();
        shipment.setOrderLines(getOrderLines(items));
        shipment.setFc(getFcName(outboundDeliveryExecution.getShipFromLocation().getInternalID()));
        shipment.setNumber(key);
        shipment.setWorkflow(DEFAULT_WORKFLOW);
        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber(StringUtils.EMPTY);
        invoice.setPaymentMode(Shipment.PaymentMode.COD.name());
        shipment.setInvoice(invoice);
        return shipment;
    }

    /*
     * Get Shipments from Customer Orders
     */
    private Order getCustomerOrder(OutboundDeliveryExecutionRequest hiltiOrder) {
        Order order = new Order();
        Consignee consignee = getConsigneeDetailsForCustomerOrder(hiltiOrder.getOutboundDeliveryExecution());
        OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.BuyerParty buyerParty = hiltiOrder.
                getOutboundDeliveryExecution().getBuyerParty();

        consignee.setPrimaryPhoneNumber(String.valueOf(
                buyerParty.getAddress().getCommunication().getTelephone().getNumber().getSubscriberID()));

        order.setConsignee(consignee);
        order.setChannel(HILTI.getSlugName());
        order.setShipments(getShippmentsForCustomer(hiltiOrder.getOutboundDeliveryExecution()));
        return order;
    }

    /*
     * Get Client Orders
     */

    public List<OutboundDeliveryExecutionRequest> getClientOrders(String orders) throws OrderDataException {
        OutboundDeliveryExecutionRequest outboundDeliveryExecutionRequest;
        String rawData = StringUtils.remove(orders, "\\");
        try {
            SOAPMessage soapMessage = SoapUtils.stringToSoapMessage(rawData);
            String rawOrders = SoapUtils.soapBodyToString(soapMessage);
            Unmarshaller unmarshaller = HiltiJaxBContext.getUnmarshaller();
            StringReader sr = new StringReader(rawOrders);
            outboundDeliveryExecutionRequest = (OutboundDeliveryExecutionRequest) unmarshaller.unmarshal(sr);

            return Collections.singletonList(outboundDeliveryExecutionRequest);
        } catch (JAXBException ex) {
            throw new OrderDataException(HILTI.getSlugName(), "Unable to create unmarshaller from jaxBContext: [%s]", ex.getMessage());
        } catch (SOAPException | ClassCastException | TransformerException | IOException ex) {
            throw new OrderDataException(HILTI.getSlugName(), "Unable to parse order data for hilti data: [%s]", orders);
        }
    }

    /**
     * get common mapping of consigneeV2
     *
     * @param outboundDeliveryExecution hilti's order
     * @return consigneeV2
     */
    private static Consignee getConsigneeDetailsForCustomerOrder(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution outboundDeliveryExecution) {

        Consignee consignee = new Consignee();
        OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.ProductRecipientParty productRecipientParty = outboundDeliveryExecution.getProductRecipientParty();
        consignee.setName(productRecipientParty.getAddress().getDisplayName().getValue());
        consignee.setAddressLine1(getAddress(productRecipientParty.getAddress().getPhysicalAddress().getStreetPrefixName()));
        consignee.setCity(productRecipientParty.getAddress().getPhysicalAddress().getCityName());
        consignee.setState(productRecipientParty.getAddress().getPhysicalAddress().getRegionName());
        consignee.setCountry(StringUtils.isBlank(productRecipientParty.getAddress().getPhysicalAddress().getCountryName().getValue())
                ? COUNTRY_INDIA : productRecipientParty.getAddress().getPhysicalAddress().getCountryName().getValue());
        consignee.setPinCode((long) productRecipientParty.getAddress().getPhysicalAddress().getStreetPostalCode());
        return consignee;

    }

    /**
     * Gets address.
     *
     * @param address the address
     * @return the address
     */
    private static String getAddress(List<String> address) {
        return address.stream().collect(Collectors.joining());
    }


    /**
     * method is used to check is item is motherItem
     *
     * @param item OutboundDeliveryExecution
     * @return true/false
     */
    private static boolean isMotherItem(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution.Item item) {
        String motherItem = item.getNonDeliveryRelevantIndicator();
        return (Objects.nonNull(motherItem) && HiltiConstants.MOTHER_ITEM.equalsIgnoreCase(motherItem));

    }


    /**
     * Gets consignee v2 for stock order.
     *
     * @param outboundDeliveryExecution the outbound delivery execution
     * @return the consignee v 2 for stock order
     */
    public static Consignee getConsigneeForStockOrder(OutboundDeliveryExecutionRequest.OutboundDeliveryExecution outboundDeliveryExecution) {

        String storeId = outboundDeliveryExecution.getShipToLocation().getInternalID();
        B2BStore stockOrderStoreDetails = WarehouseUtils.getB2BStoreDetails(HILTI,storeId);

        if(Objects.isNull(stockOrderStoreDetails)){
            throw new IllegalArgumentException(String.format("hilti store details cannot be empty for storeId: %s",storeId));
        }

        Consignee consignee = new Consignee();
        consignee.setName(stockOrderStoreDetails.getName());
        consignee.setState(stockOrderStoreDetails.getState());
        consignee.setAddressLine1(stockOrderStoreDetails.getAddress1());
        consignee.setPinCode(stockOrderStoreDetails.getPincode());
        consignee.setCountry(StringUtils.isBlank(stockOrderStoreDetails.getCountry()) ? COUNTRY_INDIA : stockOrderStoreDetails.getCountry());
        consignee.setCity(stockOrderStoreDetails.getCity());
        consignee.setPrimaryPhoneNumber(stockOrderStoreDetails.getPhone1());
        consignee.setSecondaryPhoneNumber(stockOrderStoreDetails.getPhone2());
        return consignee;

    }
}