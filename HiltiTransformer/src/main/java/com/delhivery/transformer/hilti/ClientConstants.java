package com.delhivery.transformer.hilti;

public class ClientConstants {

    public static final String HILTI_CHANNEL = "hilti";
    public static final String TRANSFORMED_ASN = "transformedAsn";

    public static final String HILTI_ASN_V2_CREATE_ROUTE = "direct:hiltiAsnV2Route";
    public static final String HILTI_ASN_V2_CREATE_ROUTE_ID = "hiltiAsnV2Route";

    public static final String HILTI_ASN_V1_CREATE_ROUTE = "direct:hiltiAsnV1Route";
    public static final String HILTI_ASN_V1_CREATE_ROUTE_ID = "hiltiAsnV1Route";
}
