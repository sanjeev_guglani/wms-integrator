package com.delhivery.transformer.hilti;

/**
 * Created by Sanjeev Guglani on 3/5/19.
 */

public class HiltiConstants {

    public static final String CUSTOMER_ORDER_TYPE_CODE = "1580";
    public static final String SHIPPING_INSTRUCTION_FIELD = "BD-AIR";
    public static final String SHIPPING_INSTRUCTION_VALUE = "Bluedart Courier";
    public static final String MOTHER_ITEM = "true";
    public static final String STOCK_ORDER_COURIER_NAME = "Rivigo";
}
