package com.delhivery.transformer.hilti.transfomers;


import com.delhivery.godam.db.utils.WarehouseUtils;
import com.delhivery.godam.integrator.contracts.exceptions.ASNDataException;
import com.delhivery.godam.integrator.contracts.inbound.ASNCreateTransformer;
import com.delhivery.godam.integrator.contracts.inbound.model.ASN;
import com.delhivery.godam.integrator.contracts.inbound.model.ASNProduct;
import com.delhivery.godam.integrator.contracts.utils.SoapUtils;
import com.delhivery.godam.integrator.contracts.utils.Utility;
import com.delhivery.godam.integrator.contracts.utils.Validator;
import com.delhivery.transformer.hilti.model.DeliveryDespatchAdviceNotification;
import com.delhivery.transformer.hilti.utils.HiltiJaxBContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.groups.Default;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.delhivery.godam.integrator.contracts.utils.GodamUtils.MasterChannel.HILTI;

/**
 * Created by Sanjeev Guglani on 28/3/19.
 * Transformer to convert ASNCreate Request to WMS-2.0 ASN
 */

public class HiltiASNCreateTransformer implements ASNCreateTransformer<DeliveryDespatchAdviceNotification, ASN> {

    private static final Logger logger = LoggerFactory.getLogger(HiltiASNCreateTransformer.class);


    @Override
    public List<DeliveryDespatchAdviceNotification> getClientASNs(String asnRawData) throws ASNDataException {
        DeliveryDespatchAdviceNotification despatchAdviceNotification;
        String rawData = StringUtils.remove(asnRawData, "\\");
        try {
            SOAPMessage soapMessage = SoapUtils.stringToSoapMessage(rawData);
            String soapBody = SoapUtils.soapBodyToString(soapMessage);
            Unmarshaller unmarshaller = HiltiJaxBContext.getUnmarshaller();
            StringReader sr = new StringReader(soapBody);
            despatchAdviceNotification = (DeliveryDespatchAdviceNotification) unmarshaller.unmarshal(sr);

        } catch (JAXBException ex) {
            throw new ASNDataException(HILTI.getSlugName(),String.format("Unable to create unmarshaller from jaxBContext: [%s]", ex.getMessage()), ex.getMessage());
        } catch (ClassCastException | SOAPException | TransformerException | IOException ex) {
            throw new ASNDataException(HILTI.getSlugName(), String.format("Unable to parse asn data for hilti data: [%s]", asnRawData), asnRawData);
        }

        return Collections.singletonList(despatchAdviceNotification);
    }

    @Override
    public void validate(DeliveryDespatchAdviceNotification asn) throws ASNDataException {
        try {
            Validator.validate(asn, Default.class);

            Optional<Float> productQuantity = asn.getDeliveryDespatchAdvice().getItem()
                    .stream().map(i -> i.getDeliveryQuantity().getValue()).reduce((x, y) -> x + y);
            productQuantity.filter(f -> f.intValue() != 0).orElseThrow(() -> new ASNDataException(HILTI.getSlugName(), "Product Quantity Cannot be Zero", asn.toString()));

        } catch (Validator.ValidationException ex) {
            throw new ASNDataException(HILTI.getSlugName(), ex.getErrorListAsString(), asn.toString());
        }
    }

    @Override
    public List<ASN> toGodamASN(DeliveryDespatchAdviceNotification clientAsn) {
        List<ASN> asns = new ArrayList<>();
        ASN asn = ASN.builder()
                .sourceNumber(clientAsn.getDeliveryDespatchAdvice().getID())
                .fulfillmentCenterUuid(getFcUuid(clientAsn.getDeliveryDespatchAdvice().getShipToLocation().getInternalID()))
                .modeOfTransportation(ASN.ModeOfTransportation.DF)
                .asnType(ASN.AsnType.FWD)
                .expectedArrivalDate(
                        Utility.getGodamFormatZonedDateTime(
                                clientAsn.getDeliveryDespatchAdvice().getDateTimePeriod().
                                        getArrivalDateTimePeriod().getStartDateTime().getValue().toString()))
                .products(getProducts(clientAsn))
                .build();
        asns.add(asn);
        return asns;
    }


    /*
     * Method to getProducts from DespatchAdviceNotification
     */
    private List<ASNProduct> getProducts(DeliveryDespatchAdviceNotification clientAsn){
        List<ASNProduct> productDetails = new ArrayList<>();
        for (DeliveryDespatchAdviceNotification.DeliveryDespatchAdvice.Item item : clientAsn.getDeliveryDespatchAdvice().getItem()) {
            ASNProduct asnProductV2 = new ASNProduct();
            asnProductV2.setSku(item.getProduct().getInternalID());
            asnProductV2.setName(item.getProduct().getInternalID());
            asnProductV2.setQuantity(new Float(item.getDeliveryQuantity().getValue()).intValue());
            productDetails.add(asnProductV2);
        }
        return productDetails;
    }

    /*
     * Method to get FC UUid based on warehouse id
     */
    private String getFcUuid(String warehouseId) {
        return WarehouseUtils.getFulfillmentCenterUuid(HILTI, warehouseId);
    }

}
